<?php


namespace PiZone\CatalogBundle\Event;

use FOS\UserBundle\Model\UserInterface;
use PiZone\CatalogBundle\Entity\Order;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PiZoneOrderEvent extends Event
{
    /**
     * @var null|Request
     */
    protected $request;

    /**
     * @var UserInterface
     */
    protected $user;

    protected $order;

    /**
     * @var Response
     */
    private $response;


    /**
     * UserEvent constructor.
     *
     * @param UserInterface $user
     * @param Order $order
     * @param Request|null  $request
     */
    public function __construct(UserInterface $user, Order $order, Request $request = null)
    {
        $this->order = $order;
        $this->user = $user;
        $this->request = $request;
    }

    /**
     * @return UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getOrder(){
        return $this->order;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Response $response
     */
    public function setResponse(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @return Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
}
