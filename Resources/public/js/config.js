
function StateConfigPZCatalog($stateProvider, $ocLazyLoadProvider, IdleProvider) {

    // Configure Idle settings
    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider
        .state('catalog', {
            abstract: true,
            url: "",
            templateUrl:  "/bundles/pizoneadmin/tmpl/common/_admin_content.html"
        })
        .state('catalog.category_new', GetStateConfig('category', 'new'))
        .state('catalog.category_edit', GetStateConfig('category', 'edit'))
        .state('catalog.category_list', GetStateConfig('category', 'list'))
        .state('catalog.catalog_new', GetStateConfig('catalog', 'new'))
        .state('catalog.catalog_edit', GetStateConfig('catalog', 'edit'))
        .state('catalog.catalog_list',  GetStateConfig('catalog', 'list'))
        .state('catalog.content_new', GetStateConfig('catalog_content', 'new'))
        .state('catalog.content_edit', GetStateConfig('catalog_content', 'edit'))
        .state('catalog.content_list', GetStateConfig('catalog_content', 'list'))
        .state('catalog.group_parameters_new', GetStateConfig('group_parameters', 'new'))
        .state('catalog.group_parameters_edit', GetStateConfig('group_parameters', 'edit'))
        .state('catalog.group_parameters_list', GetStateConfig('group_parameters', 'list'))
        .state('catalog.tag_new', GetStateConfig('tag', 'new'))
        .state('catalog.tag_edit', GetStateConfig('tag', 'edit'))
        .state('catalog.tag_list', GetStateConfig('tag', 'list'))
        .state('catalog.project_new', GetStateConfig('project', 'new'))
        .state('catalog.project_edit', GetStateConfig('project', 'edit'))
        .state('catalog.project_list', GetStateConfig('project', 'list'))
        .state('user.catalog_user_new', GetStateConfig('catalog_user', 'new'))
        .state('user.catalog_user_edit', GetStateConfig('catalog_user', 'edit'))
        .state('user.catalog_user_list', GetStateConfig('catalog_user', 'list'))
        .state('shop', {
            abstract: true,
            url: "",
            templateUrl:  "/bundles/pizoneadmin/tmpl/common/_admin_content.html"
        })
        .state('shop.stock_new', GetStateConfig('stock', 'new'))
        .state('shop.stock_edit', GetStateConfig('stock', 'edit'))
        .state('shop.stock_list', GetStateConfig('stock', 'list'))
        .state('shop.order_new', GetStateConfig('order', 'new'))
        .state('shop.order_edit', GetStateConfig('order', 'edit'))
        .state('shop.order_list', GetStateConfig('order', 'list'))
    ;

    var collections = new LazyLoadCollection();

    function GetStateConfig(name, action){
        var url = '',
            tmpl = '',
            path = "/bundles/pizonecatalog/tmpl/";
        if(action == 'list'){
            url = "/" + name + "/:pageId?sort&order_by&perPage";
            tmpl = path + name + '/index.html';
        }
        else if (action == 'new'){
            url = "/" + name + "/new";
            tmpl = path + name + '/new.html';
        }
        else if(action == 'edit'){
            url = "/" + name + "/edit/:id";
            tmpl = path + name + '/edit.html';
        }
        return {
            url: url,
                templateUrl: tmpl,
                resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    if(action == 'list')
                        return $ocLazyLoad.load(collections.GetList());
                    else
                        return $ocLazyLoad.load(collections.GetEdit());
                }
            }
        };
    }
}
StateConfigPZCatalog.$inject = ['$stateProvider', '$ocLazyLoadProvider', 'IdleProvider'];

angular
    .module('PiZone.Catalog')
    .config(StateConfigPZCatalog);
