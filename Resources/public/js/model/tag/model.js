function AdminCatalogTag($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/catalog_tag';
    self.statelink.edit = 'catalog.tag_edit';

    angular.extend(this, self);
}