function AdminCatalogTagForm($scope) {
    var self = this;
    self.GetFields = GetFields;

    function GetFields(data){
        $scope.breadcrumbs.param = data.title.value;

        var requiredMess = 'TAG.MESSAGE.REQUIRED.';

        $scope.fields =
            [
                [
                    {size: 0, icon: '', field: data._token},
                    {size: 6, icon: 'fa fa-eye', field: data.is_active}
                ],
                [
                    {size: 6, icon: 'fa fa-cube', field: data.title, assert: [
                        {key: 'notNull', message: requiredMess  + 'TITLE'}
                    ]}
                ]
            ];
    }
}