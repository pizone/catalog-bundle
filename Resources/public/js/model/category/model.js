function AdminCatalogCategory($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/category';
    self.statelink.edit = 'catalog.category_edit';

    angular.extend(this, self);
}