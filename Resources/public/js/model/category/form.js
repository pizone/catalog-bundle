function AdminCatalogCategoryForm($scope, $timeout) {
    var self = this;
    self.GetTabs = GetTabs;

    function GetTabs(data){
        $scope.breadcrumbs.param = data.title.value;
        var requiredMess = 'CATEGORY.MESSAGE.REQUIRED.',
            regexpMess = 'CATEGORY.MESSAGE.REGEXP.';

        data.image.remove = data.delete_image;

        data.groups.template = FieldDispatcher.GetLayout('multiselect');
        data.groups.type = 'multiselect';

        $scope.tabs = [
            {
                title: 'CATEGORY.TAB.GENERAL.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 0, icon: '', field: data._token},

                        {size: 6, icon: 'fa fa-cubes', field: data.parent},
                        {size: 6, icon: 'fa fa-eye', field: data.is_active}
                    ],
                    [
                        {size: 6, icon: 'fa fa-cube', field: data.title, assert: [
                            {key: 'notNull', message: requiredMess  + 'TITLE'}
                        ]},
                        {size: 6, icon: 'fa fa-codepen', field: data.alias, assert: [
                            {key: 'notNull', message: requiredMess  + 'ALIAS'}
                        ]},
                    ]
                ]
            },
            {
                title: 'CATEGORY.TAB.CONTENT.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 2, icon: 'fa fa-file', field: data.image}
                    ],
                    [
                        {size: 12, icon: 'fa fa-file', field: data.anons}
                    ],
                    [
                        {size: 12, icon: 'fa fa-edit', field: data.show_editor_anons}
                    ]
                ]
            },
            {
                title: 'CATEGORY.TAB.GROUP.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 12, icon: 'fa fa-file', field: data.groups}
                    ]
                ]
            }
        ];
        Watch();
    }

    function SetLayout(type){
        $timeout(function() {
            $scope.tabs[1].groups[1][0].field.template = FieldDispatcher.GetLayout(type);
        });
    }

    function Watch(){
        $scope.$watch('tabs[1].groups[2][0].field.checked', function(val){
            if(val)
                SetLayout('editor');
            else
                SetLayout('textarea');
        }, true);
    }
}