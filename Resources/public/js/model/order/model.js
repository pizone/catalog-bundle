function AdminCatalogOrder($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/order_stock';
    self.statelink.edit = 'shop.order_edit';

    angular.extend(this, self);
}