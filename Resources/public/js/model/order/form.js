function AdminCatalogOrderForm($scope, $timeout) {
    var self = this;
    $scope.ClickAddField = ClickAddField;
    $scope.ClickRemoveField = ClickRemoveField;
    $scope.Befor.Submit = BeforSubmit;
    self.GetTabs = GetTabs;

    function GetTabs(data){

        var requiredMess = 'ORDER.MESSAGE.REQUIRED.',
            regexpMess = 'ORDER.MESSAGE.REGEXP.';

        data.status.template = FieldDispatcher.GetLayout('simple_choice');

        $scope.collectionPrototype = data.content.prototype;
        $scope.tabs = [
            {
                title: 'ORDER.TAB.GENERAL.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 0, icon: '', field: data._token},
                        {size: 6, icon: 'fa fa-eye', field: data.is_active},
                        {size: 6, icon: 'fa fa-eye', field: data.is_confirm}
                    ],
                    [
                        {size: 6, icon: 'fa fa-user', field: data.user, assert: [
                            {key: 'notNull', message: requiredMess + 'USER'}
                        ]},
                        {size: 6, icon: 'fa fa-cube', field: data.status, assert: [
                            {key: 'notNull', message: requiredMess + 'STATUS'}
                        ]}
                    ],
                    [
                        {size: 6, icon: 'fa fa-cube', field: data.itog, assert: [
                            {key: 'notNull', message: requiredMess + 'ITOG'}
                        ]},
                        {size: 6, icon: 'fa fa-cube', field: data.discount, assert: [
                            {key: 'int', message: regexpMess + 'DISCOUNT'}
                        ]}
                    ],
                    [
                        {size: 6, icon: 'fa fa-cube', field: data.stock}
                    ],
                    [
                        {size: 12, icon: 'fa fa-comment', field: data.comment}
                    ]
                ]
            },
            {
                title: 'ORDER.TAB.GOODS.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 12, icon: 'fa fa-cubes', field: GetCollectionForm(data.content)}
                    ]
                ]
            }
        ];

        $scope.prepareToAutocomplete($scope.tabs[0].groups[1][0].field, 'admin/catalog_user');
    }

    function pad (str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }

    function GetCollectionForm(codes){
        var collection = {}, one = {};
        var requiredMess = 'ORDER.MESSAGE.REQUIRED.',
            regExpMess = 'ORDER.MESSAGE.REGEXP';
        if(codes.form.length === 0) {
            one = GetNewCollectionForm();
            codes.form.push(one);
        }
        else{
            $.each(codes.form, function(i){
                collection[pad($scope.collectionIndex, 3)] =  [
                    [
                        {size: 0, icon: '', field: codes.form[i].form.id},
                        {size: 7, icon: 'fa fa-cube', field: codes.form[i].form.content, assert: [
                            {key: 'notNull', message: requiredMess + 'GOODS'}
                        ]},
                        {size: 3, icon: 'fa fa-codepen', field: codes.form[i].form.quantity, assert: [
                            {key: 'notNull', message: requiredMess + 'QUANTITY'},
                            {key: 'int', message: regExpMess + 'QUANTITY'}
                        ]},
                        {size: 7, icon: 'fa fa-codepen', field: codes.form[i].form.price, assert: [
                            {key: 'notNull', message: requiredMess + 'PRICE'},
                            {key: 'float', message: regExpMess + 'PRICE'}
                        ]},
                        {size: 3, icon: 'fa fa-codepen', field: codes.form[i].form.discount, assert: [
                            {key: 'int', message: regExpMess + 'DISCOUNT'}
                        ]},
                        {size: 10, icon: 'fa fa-comment', field: codes.form[i].form.comment}
                    ]
                ];
                $scope.collectionIndex = $scope.collectionIndex + 1;
            });
            codes.form = collection;
        }

        if(Object.keys(codes.form).length > 1)
            $scope.showRemoveCollectionButton = true;
        return codes;
    }

    function GetNewCollectionForm() {
        var requiredMess = 'ORDER.MESSAGE.REQUIRED.',
            regExpMess = 'ORDER.MESSAGE.REGEXP';
        var form = angular.copy($scope.collectionPrototype);

        $.each(form, function (i) {
            $.each(form[i], function (j) {
                if (typeof(form[i][j]) == 'string')
                    form[i][j] = form[i][j].replace('__name__', $scope.collectionIndex);
            });
        });
        var one = {};

        one[pad($scope.collectionIndex, 3)] =  [
            {size: 7, icon: 'fa fa-cube', field: form.content, assert: [
                {key: 'notNull', message: requiredMess + 'CONTENT'}
            ]},
            {size: 3, icon: 'fa fa-codepen', field: form.quantity, assert: [
                {key: 'notNull', message: requiredMess + 'QUANTITY'},
                {key: 'int', message: regExpMess + 'QUANTITY'}
            ]},
            {size: 7, icon: 'fa fa-codepen', field: form.price, assert: [
                {key: 'notNull', message: requiredMess + 'PRICE'},
                {key: 'float', message: regExpMess + 'PRICE'}
            ]},
            {size: 3, icon: 'fa fa-codepen', field: form.discount, assert: [
                {key: 'int', message: regExpMess + 'DISCOUNT'}
            ]},
            {size: 10, icon: 'fa fa-eye', field: form.comment}
        ];
        $scope.collectionIndex = $scope.collectionIndex + 1;

        return one;
    }

    function ClickAddField(){
        $timeout(function(){
            var form = GetNewCollectionForm(),
                collection = $scope.tabs[1].groups[0][0].field.form;

            collection[pad($scope.collectionIndex-1, 3)] = form;

            if(Object.keys($scope.tabs[1].groups[0][0].field.form).length > 1)
                $scope.showRemoveCollectionButton = true;
        });
    }

    function ClickRemoveField(key){
        $timeout(function() {
            var collection =  $scope.tabs[1].groups[0][0].field.form,
                form = {};
            delete collection[key];

            $.each(collection, function(i, one){
                if(one)
                    form[i] = one;
            });
            $scope.tabs[1].groups[0][0].field.form= form;

            var keys = Object.keys( $scope.tabs[1].groups[0][0].field.form);
            if(Object.keys(keys).length == 1){
                var res =  $scope.tabs[1].groups[0][0].field.form[keys[0]];
                $.each(res, function(i){
                    if(res[i][res[i].length-1].field.value === '')
                        $scope.showRemoveCollectionButton = false;
                });
            }
            if(Object.keys(keys).length === 0){
                ClickAddField();
                $scope.showRemoveCollectionButton = false;
            }
        });
    }

    function BeforSubmit(){
        var keys = Object.keys( $scope.tabs[1].groups[0][0].field.form);
        if(Object.keys(keys).length == 1){
            var res =  $scope.tabs[1].groups[0][0].field.form[keys[0]];
            $.each(res, function(i){
                if(res[i][res[i].length-1].field.value === '') {
                    $('#' + res[i][res[i].length-1].field.id).attr('disabled', 'disabled');
                }
            });
        }
    }
}