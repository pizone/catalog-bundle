function AdminGroupParameters($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/group_parameters';
    self.statelink.edit = 'catalog.group_parameters_edit';

    angular.extend(this, self);
}