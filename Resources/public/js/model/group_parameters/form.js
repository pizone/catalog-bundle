function AdminGroupParametersForm($scope, $timeout) {
    var self = this;
    self.GetTabs = GetTabs;
    $scope.ClickAddField = ClickAddField;
    $scope.ClickRemoveField = ClickRemoveField;
    $scope.Befor.Submit = BeforSubmit;
    var requiredMess = 'GPARAM.MESSAGE.REQUIRED.',
        uniqueMess = 'GPARAM.MESSAGE.UNIQUE.';

    function GetTabs(data){

        $scope.breadcrumbs.param = data.title.value;

        $scope.collectionPrototype = data.parameter.prototype;

        $scope.tabs = [
            {
                title: 'GPARAM.TAB.GENERAL.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 0, icon: '', field: data._token},
                        {size: 8, icon: 'fa fa-eye', field: data.is_active}
                    ],
                    [
                        {size: 6, icon: 'fa fa-cube', field: data.title, assert: [
                            {key: 'notNull', message: requiredMess + 'TITLE'}
                        ]},
                        {size: 6, icon: 'fa fa-codepen', field: data.alias, assert: [
                            {key: 'notNull', message: requiredMess + 'ALIAS'}
                        ]}
                    ],
                    [
                        {size: 12, icon: 'fa fa-comment', field: data.description}
                    ]
                ]
            },
            {
                title: 'GPARAM.TAB.PARAMS.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 12, icon: 'fa fa-cubes', field: GetCollectionForm(data.parameter)}
                    ]
                ]
            }
        ];
    }

    function getCodes(){
        return $scope.tabs[1].groups[0][0].field.form;
    }

    function pad (str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }

    function GetCollectionForm(codes){
        var collection = {}, one = {};
        if(codes.form.length === 0) {
            one = GetNewCollectionForm();
            codes.form.push(one);
        }
        else{
            $.each(codes.form, function(i){
                collection[pad($scope.collectionIndex, 3)] =  [
                    [
                        {size: 0, icon: '', field: codes.form[i].form.id},
                        {size: 4, icon: 'fa fa-cube', field: codes.form[i].form.title, assert: [
                            {key: 'notNull', message: requiredMess + 'PTITLE'}
                        ]},
                        {size: 4, icon: 'fa fa-codepen', field: codes.form[i].form.alias, assert: [
                            {key: 'notNull', message: requiredMess + 'ALIAS'},
                            {key: 'unique', message: uniqueMess + 'ALIAS', field: 'alias', collection: getCodes}
                        ]},
                        {size: 2, icon: 'fa fa-eye', field: codes.form[i].form.is_active}
                    ]
                ];
                $scope.collectionIndex = $scope.collectionIndex + 1;
            });
            codes.form = collection;
        }

        if(Object.keys(codes.form).length > 1)
            $scope.showRemoveCollectionButton = true;
        return codes;
    }

    function GetNewCollectionForm() {
        var form = angular.copy($scope.collectionPrototype);

        $.each(form, function (i) {
            $.each(form[i], function (j) {
                if (typeof(form[i][j]) == 'string')
                    form[i][j] = form[i][j].replace('__name__', $scope.collectionIndex);
            });
        });
        var one = {};

        one[pad($scope.collectionIndex, 3)] =  [
            {size: 4, icon: 'fa fa-cube', field: form.title, assert: [
                {key: 'notNull', message: requiredMess + 'PTITLE'}
            ]},
            {size: 4, icon: 'fa fa-codepen', field: form.alias, assert: [
                {key: 'notNull', message: requiredMess + 'ALIAS'},
                {key: 'unique', message: uniqueMess + 'ALIAS', field: 'alias', collection: getCodes}
            ]},
            {size: 2, icon: 'fa fa-eye', field: form.is_active}
        ];
        $scope.collectionIndex = $scope.collectionIndex + 1;

        return one;
    }

    function ClickAddField(){
        $timeout(function(){
            var form = GetNewCollectionForm(),
                collection = $scope.tabs[1].groups[0][0].field.form;

            collection[pad($scope.collectionIndex-1, 3)] = form;

            if(Object.keys($scope.tabs[1].groups[0][0].field.form).length > 1)
                $scope.showRemoveCollectionButton = true;
        });
    }

    function ClickRemoveField(key){
        $timeout(function() {
            var collection =  $scope.tabs[1].groups[0][0].field.form,
                form = {};
            delete collection[key];

            $.each(collection, function(i, one){
                if(one)
                    form[i] = one;
            });
            $scope.tabs[1].groups[0][0].field.form= form;

            var keys = Object.keys( $scope.tabs[1].groups[0][0].field.form);
            if(Object.keys(keys).length == 1){
                var res =  $scope.tabs[1].groups[0][0].field.form[keys[0]];
                $.each(res, function(i){
                    if(res[i][res[i].length-1].field.value === '')
                        $scope.showRemoveCollectionButton = false;
                });
            }
            if(Object.keys(keys).length === 0){
                ClickAddField();
                $scope.showRemoveCollectionButton = false;
            }
        });
    }

    function BeforSubmit(){
        var keys = Object.keys( $scope.tabs[1].groups[0][0].field.form);
        if(Object.keys(keys).length == 1){
            var res =  $scope.tabs[1].groups[0][0].field.form[keys[0]];
            $.each(res, function(i){
                if(res[i][res[i].length-1].field.value === '') {
                    $('#' + res[i][res[i].length-1].field.id).attr('disabled', 'disabled');
                }
            });
        }
    }
}