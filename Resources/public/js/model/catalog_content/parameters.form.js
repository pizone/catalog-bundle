function AdminParametersForm($scope, $timeout, $http) {
    var self = this;
    self.GetFields = GetFields;

    function GetFields(data){
        var requiredMess = 'CCONTENT.MESSAGE.REQUIRED.';
        $scope.fields =
            [
                [
                    {size: 0, icon: '', field: data.content},
                    {size: 12, icon: 'fa fa-cubes', field: data.category, assert:[
                        {key: 'notNull', message: requiredMess  + 'CATEGORY'}
                    ]}
                ],
                []
            ];

        WatchCategory();

        if(data.group_parameters)
            GroupParameters(data.group_parameters.form);
    }

    function GroupParameters(data){
        $scope.fields[1] = [];
        for(var key in data){
            data[key].template = FieldDispatcher.GetLayout('multiselect');
            data[key].type = 'multiselect';
            $scope.fields[1].push({size: 6, icon: 'fa fa-cog', field: data[key]});
        }
    }


    var init = false;
    function WatchCategory(){
        $scope.$watch('fields[0][1].field.model', function(val){
            if(init){
                var container = $('.block_categories:eq(1)');
                var loader = new Loader(container);
                loader.Create();

                $http({
                    url: PiZoneConfig.apiPrefix + '/admin/catalog_content/get_parameters/' + val.value,
                    method: 'GET',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).then(
                    function(message){
                        message = JSON.parse(message.data);

                        if(message.fields.group_parameters)
                            GroupParameters(message.fields.group_parameters.form);
                        else
                            GroupParameters([]);
                        loader.Delete();
                    },
                    function(message){
                        if (message.status == 400) {
                            message = JSON.parse(message.data);
                            $timeout(function() {
                                $scope.Callback.Error(message);
                            });
                        }
                        loader.Delete();
                    });
            }
            init = true;
        });
    }
}