function AdminCatalogContent($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/catalog_content';
    self.statelink.edit = 'catalog.content_edit';

    angular.extend(this, self);
}