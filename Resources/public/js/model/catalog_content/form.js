function AdminCatalogContentForm($scope, $timeout, $http, $q) {
    var self = this;
    self.GetTabs = GetTabs;

    function GetTabs(data){

        $scope.breadcrumbs.param = data.title.value;
        var requiredMess = 'CCONTENT.MESSAGE.REQUIRED.',
            regexpMess = 'CCONTENT.MESSAGE.REGEXP.';

        data.image.remove = data.delete_image;
        data.big_image.remove = data.delete_big_image;

        data.related.template = FieldDispatcher.GetLayout('multiselect');
        data.related.type = 'multiselect';

        data.string_tags.template = FieldDispatcher.GetLayout('tags_autocomplete');
        data.string_tags.type = 'tags_autocomplete';

        $scope.tabs = [
            {
                title: 'CCONTENT.TAB.GENERAL.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 0, icon: '', field: data._token},
                        {size: 3, icon: 'fa fa-eye', field: data.is_active},
                    ],
                    [
                        {size: 6, icon: 'fa fa-cube', field: data.title, assert: [
                            {key: 'notNull', message: requiredMess  + 'TITLE'}
                        ]},
                        {size: 6, icon: 'fa fa-codepen', field: data.alias, assert: [
                            {key: 'notNull', message: requiredMess  + 'ALIAS'}
                        ]}
                    ],
                    [
                        {size: 4, icon: 'fa fa-exclamation', field: data.is_new},
                        {size: 4, icon: 'fa fa-star', field: data.is_hit},
                        {size: 4, icon: 'fa fa-archive', field: data.is_old}
                    ],
                    [
                        {size: 12, icon: 'fa fa-cubes', field: data.string_tags, loadItems: getHashTagList}
                    ]
                ]
            },
            {
                title: 'CCONTENT.TAB.ANONS.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 2, icon: 'fa fa-file', field: data.image},
                    ],
                    [
                        {size: 12, icon: 'fa fa-file', field: data.anons},
                        {size: 12, icon: 'fa fa-edit', field: data.show_editor_anons}
                    ]
                ]
            },
            {
                title: 'CCONTENT.TAB.CONTENT.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 2, icon: 'fa fa-file', field: data.big_image},
                    ],
                    [
                        {size: 12, icon: 'fa fa-file', field: data.content},
                        {size: 12, icon: 'fa fa-edit', field: data.show_editor_content}
                    ]
                ]
            },
            {
                title: 'CCONTENT.TAB.MORE.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                ]
            },
            {
                title: 'CCONTENT.TAB.CATEGORY.TITLE',
                description: '',
                form: true,
                valid: true,
                id: 'block_categories',
                groups: [
                    [
                        {size: 4, icon: 'fa fa-cubes', field: data.category}
                    ],
                    []
                ]
            },
            {
                title: 'CCONTENT.TAB.RELATED.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 12, icon: 'fa fa-cubes', field: data.related}
                    ]
                ]
            },
            {
                title: 'CCONTENT.TAB.SALE.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 3, icon: 'fa fa-money', field: data.sale.form.purchase_price, assert: [
                            {key: 'float', message: regexpMess + 'FLOAT'}
                        ]},
                        {size: 3, icon: 'fa fa-money', field: data.sale.form.retail_price, assert: [
                            {key: 'float', message: regexpMess + 'FLOAT'}
                        ]},
                        {size: 3, icon: 'fa fa-scissors', field: data.sale.form.discount},
                        {size: 3, icon: 'fa fa-info', field: data.sale.form.VAT}
                    ],
                    [
                        {size: 6, icon: 'fa fa-barcode', field: data.sale.form.barcode}
                    ],
                    [
                        {size: 3, icon: 'fa fa-arrows-h', field: data.sale.form.width, assert: [
                            {key: 'float', message: regexpMess + 'FLOAT'}
                        ]},
                        {size: 3, icon: 'fa fa-arrows-v', field: data.sale.form.height, assert: [
                            {key: 'float', message: regexpMess + 'FLOAT'}
                        ]},
                        {size: 3, icon: 'fa fa-expand', field: data.sale.form.length, assert: [
                            {key: 'float', message: regexpMess + 'FLOAT'}
                        ]},
                        {size: 3, icon: 'fa fa-tachometer', field: data.sale.form.weight, assert: [
                            {key: 'float', message: regexpMess + 'FLOAT'}
                        ]}
                    ],
                    [
                        {size: 12, icon: 'fa fa-cubes', field:  GetCollectionForm(data.sale.form.stocks)}
                    ]
                ]
            },
            {
                title: 'CONTENT.TAB.META.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 6, icon: 'fa fa-cube', field: data.meta.form.meta_title}
                    ],
                    [
                        {size: 6, icon: 'fa fa-key', field: data.meta.form.meta_keywords}
                    ],
                    [
                        {size: 6, icon: 'fa fa-comment', field: data.meta.form.meta_description}
                    ],
                    [
                        {size: 6, icon: 'fa fa-code', field: data.meta.form.more_scripts}
                    ]
                ]
            }
        ];

        Watch(1, 1);
        Watch(2, 1);
        WatchCategory();

        if(data.more)
            More(data.more.form);

        if(data.group_parameters)
            GroupParameters(data.group_parameters.form);
    }

    function More(data){
        var i = 0;
        for(var key in data){
            var obj = {size: 12, icon: 'fa fa-tag', field: data[key]};
            if(data[key].required)
                obj.assert = [{key: 'notNull', message: 'COMMON.MESSAGE.REQUIRED.NOT_NULL'}];
            $scope.tabs[3].groups[i] = [obj];
            i++;
        }
    }

    function GroupParameters(data){
        $scope.tabs[4].groups[1] = [];
        for(var key in data){
            data[key].template = FieldDispatcher.GetLayout('multiselect');
            data[key].type = 'multiselect';
            $scope.tabs[4].groups[1].push({size: 4, icon: 'fa fa-cog', field: data[key]});
        }
    }

    function SetLayout(tabsId, id, type){
        $timeout(function() {
            $scope.tabs[tabsId].groups[id][0].field.template = FieldDispatcher.GetLayout(type);
        });
    }

    function Watch(tabsId, id){
        $scope.$watch('tabs[' + tabsId + '].groups[' + id + '][1].field.checked', function(val){
            if(val)
                SetLayout(tabsId, id, 'editor');
            else
                SetLayout(tabsId, id, 'textarea');
        }, true);
    }

    var init = false;
    function WatchCategory(){
        $scope.$watch('tabs[4].groups[0][0].field.model', function(val){
            if(init){
                var container = $('.block_categories:eq(1)');
                var loader = new Loader(container);
                loader.Create();

                $http({
                    url: PiZoneConfig.apiPrefix + '/admin/catalog_content/get_parameters/' + val.value,
                    method: 'GET',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).then(
                    function(message){
                        message = JSON.parse(message.data);

                        if(message.fields.group_parameters)
                            GroupParameters(message.fields.group_parameters.form);
                        else
                            GroupParameters([]);
                        loader.Delete();
                    },
                    function(message){
                        if (message.status == 400) {
                            message = JSON.parse(message.data);
                            $timeout(function() {
                                $scope.Callback.Error(message);
                            });
                        }
                        loader.Delete();
                    });
            }
            init = true;
        });
    }

    function getHashTagList(query){
        if (query){
            var defer = $q.defer();

            $http.get(PiZoneConfig.apiPrefix + '/admin/catalog_tag/find/' + encodeURIComponent(query)).then(
                function (response){
                    var data = JSON.parse(response.data);
                    return defer.resolve(data);
                },
                function (error){
                    console.log(error);
                }
            );
            return defer.promise;
        }
        else
            return [];
    }

    function pad (str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
    }

    function GetCollectionForm(codes){
        var collection = {}, mess = 'MESSAGE.CCONTENT.REGEXP.';

        $.each(codes.form, function(i){
            collection[pad($scope.collectionIndex, 3)] =  [
                [
                    {size: 6, icon: 'fa fa-cube', field: codes.form[i].form.quantity, assert: [
                        {key: 'int', message: mess + 'INT'}
                    ]},
                    {size: 6, icon: 'fa fa-codepen', field: codes.form[i].form.reserved, assert: [
                        {key: 'int', message: mess + 'INT'}
                    ]}
                ]
            ];
            $scope.collectionIndex = $scope.collectionIndex + 1;
        });
        codes.form = collection;
        codes.hideButton = true;

        return codes;
    }
}