function AdminCatalogUserForm($scope) {
    var self = this;

    self.GetTabs = GetTabs;

    function GetTabs(data){
        $scope.breadcrumbs.param = data.username.value;

        var requiredMess = 'CUSER.MESSAGE.REQUIRED.',
            regexpMess = 'CUSER.MESSAGE.REGEXP.';

        data.plainPassword.form.first.type = 'password';
        data.plainPassword.form.second.type = 'password';

        $scope.tabs = [
            {
                title: 'CUSER.TAB.GENERAL.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 0, icon: '', field: data._token},
                        {size: 6, icon: 'fa fa-power-aff', field: data.enabled}
                    ],
                    [
                        {size: 6, icon: 'fa fa-user', field: data.username, assert: [
                            {key: 'notNull', message: requiredMess  + 'USERNAME'}
                        ]},
                        {size: 6, icon: 'fa fa-user', field: data.fullName}
                    ],
                    [
                        {size: 6, icon: 'fa fa-user', field: data.company},
                        {size: 6, icon: 'fa fa-user', field: data.positionHeld}
                    ],
                    [
                        {size: 12, icon: 'fa fa-envelope', field: data.address}
                    ],
                    [
                        {size: 4, icon: 'fa fa-envelope', field: data.email, assert: [
                            {key: 'notNull', message: requiredMess  + 'EMAIL'},
                            {key: 'email', message: regexpMess  + 'EMAIL'}
                        ]},
                        {size: 4, icon: 'fa fa-envelope', field: data.phone},
                        {size: 4, icon: 'fa fa-envelope', field: data.messenger}
                    ]
                ]
            },
            {
                title: 'CUSER.TAB.SECURITY.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 6, icon: 'fa fa-lock', field: data.plainPassword.form.first, assert: [
                            {key: 'repeat', message: 'CUSER.MESSAGE.REPEAT.PASSWORD', compare: data.plainPassword.form.second}
                        ]},
                        {size: 6, icon: 'fa fa-lock', field: data.plainPassword.form.second}
                    ],
                    [
                        {size: 12, icon: 'fa fa-users', field: data.roles}
                    ]
                ]
            }
        ];
    }
}