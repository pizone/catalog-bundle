function AdminCatalogUser($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/catalog_user';
    self.statelink.edit = 'user.catalog_user_edit';

    angular.extend(this, self);
}