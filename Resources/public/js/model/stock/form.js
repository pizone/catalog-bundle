function AdminCatalogStockForm($scope) {
    var self = this;
    self.GetFields = GetFields;

    function GetFields(data){
        $scope.breadcrumbs.param = data.title.title;

        var requiredMess = 'STOCK.MESSAGE.REQUIRED.';

        $scope.fields =
            [
                [
                    {size: 0, icon: '', field: data._token},
                    {size: 6, icon: 'fa fa-eye', field: data.is_active}
                ],
                [
                    {size: 6, icon: 'fa fa-cube', field: data.title, assert: [
                        {key: 'notNull', message: requiredMess  + 'TITLE'}
                    ]}
                ],
                [
                    {size: 12, icon: 'fa fa-map-marker', field: data.address, assert: [
                        {key: 'notNull', message: requiredMess  + 'ADDRESS'}
                    ]}
                ],
                [
                    {size: 6, icon: 'fa fa-globe', field: data.lat},
                    {size: 6, icon: 'fa fa-globe', field: data.lng}
                ]
            ];
    }
}