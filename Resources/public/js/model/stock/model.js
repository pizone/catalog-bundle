function AdminCatalogStock($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/catalog_stock';
    self.statelink.edit = 'catalog.stock_edit';

    angular.extend(this, self);
}