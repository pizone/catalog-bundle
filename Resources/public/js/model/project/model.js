function AdminCatalogProject($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/catalog_project';
    self.statelink.edit = 'catalog.project_edit';

    angular.extend(this, self);
}