function AdminCatalogForm($scope, $timeout) {
    var self = this;
    self.GetTabs = GetTabs;

    function GetTabs(data){

        $scope.breadcrumbs.param = data.title.value;
        var requiredMess = 'CATALOG.MESSAGE.REQUIRED.',
            regexpMess = 'CATALOG.MESSAGE.REGEXP.';

        $scope.tabs = [
            {
                title: 'CATALOG.TAB.GENERAL.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 0, icon: '', field: data._token},
                        {size: 8, icon: 'fa fa-eye', field: data.is_active}
                    ],
                    [
                        {size: 12, icon: 'fa fa-cubes', field: data.parent}
                    ],
                    [
                        {size: 6, icon: 'fa fa-cube', field: data.title, assert: [
                            {key: 'notNull', message: requiredMess  + 'TITLE'}
                        ]},
                        {size: 6, icon: 'fa fa-codepen', field: data.alias, assert: [
                            {key: 'notNull', message: requiredMess  + 'ALIAS'}
                        ]}
                    ],
                    [
                        {size: 12, icon: 'fa fa-file', field: data.content}
                    ]
                ]
            },
            {
                title: 'CATALOG.TAB.META.TITLE',
                description: '',
                form: true,
                valid: true,
                groups: [
                    [
                        {size: 6, icon: 'fa fa-cube', field: data.meta.form.meta_title},
                        {size: 6, icon: 'fa fa-key', field: data.meta.form.meta_keywords}
                    ],
                    [
                        {size: 6, icon: 'fa fa-comment', field: data.meta.form.meta_description},
                        {size: 6, icon: 'fa fa-code', field: data.meta.form.more_scripts}
                    ],
                    [
                        {size: 6, icon: 'fa fa-cube', field: data.meta.form.in_menu},
                        {size: 6, icon: 'fa fa-cube', field: data.meta.form.in_breadcrumbs}
                    ],
                    [
                        {size: 6, icon: 'fa fa-cube', field: data.meta.form.in_site_map},
                        {size: 6, icon: 'fa fa-cube', field: data.meta.form.in_robots}
                    ]
                ]
            }
        ];
    }
}