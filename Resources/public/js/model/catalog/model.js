function AdminCatalog($scope, $http, $state, i, data, SweetAlert, $filter){
    var self = new Model($scope, $http, $state, i, data, SweetAlert, $filter);
    self.prefix = 'admin/catalog';
    self.statelink.edit = 'catalog.catalog_edit';

    angular.extend(this, self);
}