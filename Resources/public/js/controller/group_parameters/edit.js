function AdminGroupParametersEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/group_parameters';
    $scope.statelink.list = 'catalog.group_parameters_list';
    $scope.statelink.new = 'catalog.group_parameters_new';
    $scope.breadcrumbs.title = 'GPARAM.EDIT';
    $scope.breadcrumbs.path = [{title: 'GPARAM.LIST', url: 'catalog.group_parameters_list'}];
    $scope.activeTab = {
        0: $stateParams.tabId === 0 ? true : false,
        1: $stateParams.tabId == 1 ? true : false,
        2: $stateParams.tabId == 2 ? true : false,
        3: $stateParams.tabId == 3 ? true : false
    };
    $scope.collectionPrototype = null;
    $scope.collectionIndex = 0;
    $scope.showRemoveCollectionButton = false;

    var Form = new AdminGroupParametersForm($scope, $timeout);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminGroupParametersEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Catalog').controller('AdminGroupParametersEditCtrl', AdminGroupParametersEditCtrl);