function AdminGroupParametersListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/group_parameters';
    $scope.statelink.add = 'catalog.group_parameters_new';
    $scope.statelink.edit = 'catalog.group_parameters_edit';
    $scope.statelink.page = 'catalog.group_parameters_list';
    $scope.breadcrumbs.title = 'GPARAM.LIST';
    $scope.model = AdminGroupParameters;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'GPARAM.FIELD.TITLE', name: 'title'},
        {title: 'GPARAM.FIELD.ALIAS', name: 'alias'},
        {title: 'GPARAM.FIELD.DESCRIPTION', name: 'description'},
        {title: 'GPARAM.FIELD.PARAMS.TITLE'},
        {title: 'GPARAM.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 4, 6, 7]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;
        data.is_active.template = FieldDispatcher.GetLayout('simple_choice');

        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-cube', field: data.title, show: data.title.value ? true : false},
            {icon: 'fa fa-codepen', field: data.title, show: data.alias.value ? true : false},
            {icon: 'fa fa-comment', field: data.description, show: data.description.value ? true : false},
            {icon: 'fa fa-eye', field: data.is_active, show: data.is_active.value !== null ? true: false},
        ];
    }

    $scope.Init();
}
AdminGroupParametersListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Catalog').controller('AdminGroupParametersListCtrl', AdminGroupParametersListCtrl);