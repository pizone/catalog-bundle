function AdminGroupParametersNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/group_parameters';
    $scope.statelink.list = 'catalog.group_parameters_list';
    $scope.statelink.new = 'catalog.group_parameters_new';
    $scope.statelink.edit = 'catalog.group_parameters_edit';
    $scope.breadcrumbs.title = 'GPARAM.NEW';
    $scope.breadcrumbs.path = [{title: 'GPARAM.LIST', url: 'catalog.group_parameters_list'}];
    $scope.collectionPrototype = null;
    $scope.collectionIndex = 0;
    $scope.showRemoveCollectionButton = false;

    var Form = new AdminGroupParametersForm($scope, $timeout);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminGroupParametersNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Catalog').controller('AdminGroupParametersNewCtrl', AdminGroupParametersNewCtrl);