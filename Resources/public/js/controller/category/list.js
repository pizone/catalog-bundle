function AdminCatalogCategoryListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/category';
    $scope.statelink.add = 'catalog.category_new';
    $scope.statelink.edit = 'catalog.category_edit';
    $scope.statelink.page = 'catalog.category_list';
    $scope.breadcrumbs.title = 'CATEGORY.LIST';
    $scope.model = AdminCatalogCategory;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'CATEGORY.FIELD.TITLE', name: 'title'},
        {title: 'CATEGORY.FIELD.ALIAS', name: 'alias'},
        {title: 'CATEGORY.FIELD.IMAGE', name: 'image'},
        {title: 'CATEGORY.FIELD.ANONS', name: 'anons'},
        {title: 'CATEGORY.FIELD.GROUPS'},
        {title: 'CATEGORY.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 6, 7, 8]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;
        data.is_active.template = FieldDispatcher.GetLayout('simple_choice');

        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-cubes', field: data.parent, show: data.parent.value ? true : false},
            {icon: 'fa fa-cube', field: data.title, show: data.title.value ? true : false},
            {icon: 'fa fa-codepen', field: data.alias, show: data.alias.value ? true : false},
            {icon: 'fa fa-file', field: data.anons, show: data.anons.value ? true: false},
            {icon: 'fa fa-eye', field: data.is_active, show: data.is_active.value >= 0 ? true: false},
        ];
    }

    $scope.Init();
}
AdminCatalogCategoryListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Catalog').controller('AdminCatalogCategoryListCtrl', AdminCatalogCategoryListCtrl);