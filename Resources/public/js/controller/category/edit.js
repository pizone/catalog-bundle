function AdminCatalogCategoryEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/category';
    $scope.statelink.list = 'catalog.category_list';
    $scope.statelink.new = 'catalog.category_new';
    $scope.breadcrumbs.title = 'CATEGORY.EDIT';
    $scope.breadcrumbs.path = [{title: 'CATEGORY.LIST', url: 'catalog.category_list'}];
    $scope.activeTab = {
        0: $stateParams.tabId === 0 ? true : false,
        1: $stateParams.tabId == 1 ? true : false,
        2: $stateParams.tabId == 2 ? true : false,
        3: $stateParams.tabId == 3 ? true : false
    };

    var Form = new AdminCatalogCategoryForm($scope, $timeout);

    $scope.Get.Fields = Form.GetTabs;

    $scope.Init();
}
AdminCatalogCategoryEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Catalog').controller('AdminCatalogCategoryEditCtrl', AdminCatalogCategoryEditCtrl);