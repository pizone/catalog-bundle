function AdminCatalogCategoryNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/category';
    $scope.statelink.list = 'catalog.category_list';
    $scope.statelink.new = 'catalog.category_new';
    $scope.statelink.edit = 'catalog.category_edit';
    $scope.breadcrumbs.title = 'CATEGORY.NEW';
    $scope.breadcrumbs.path = [{title: 'CATEGORY.LIST', url: 'catalog.category_list'}];

    var Form = new AdminCatalogCategoryForm($scope, $timeout);

    $scope.Get.Fields = Form.GetTabs;

    $scope.Init();
}
AdminCatalogCategoryNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Catalog').controller('AdminCatalogCategoryNewCtrl', AdminCatalogCategoryNewCtrl);