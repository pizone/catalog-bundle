function AdminCatalogNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/catalog';
    $scope.statelink.list = 'catalog.catalog_list';
    $scope.statelink.new = 'catalog.catalog_new';
    $scope.statelink.edit = 'catalog.catalog_edit';
    $scope.breadcrumbs.title = 'CATALOG.NEW';
    $scope.breadcrumbs.path = [{title: 'CATALOG.LIST', url: 'catalog.catalog_list'}];

    var Form = new AdminCatalogForm($scope, $timeout);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminCatalogNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Catalog').controller('AdminCatalogNewCtrl', AdminCatalogNewCtrl);