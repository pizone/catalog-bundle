function AdminCatalogListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/catalog';
    $scope.statelink.add = 'catalog.catalog_new';
    $scope.statelink.edit = 'catalog.catalog_edit';
    $scope.statelink.page = 'catalog.catalog_list';
    $scope.breadcrumbs.title = 'CATALOG.LIST';
    $scope.model = AdminCatalog;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'CATALOG.FIELD.TITLE', name: 'title'},
        {title: 'CATALOG.FIELD.ALIAS', name: 'alias'},
        {title: 'CATALOG.FIELD.CONTENT', name: 'content'},
        {title: 'CATALOG.FIELD.CREATED_AT', name: 'created_at'},
        {title: 'CATALOG.FIELD.LEVEL', name: 'level'},
        {title: 'CATALOG.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 4, 7, 8]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;
        data.is_active.template = FieldDispatcher.GetLayout('simple_choice');

        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-institution', field: data.project, show: data.project.value ? true : false},
            {icon: 'fa fa-cubes', field: data.parent, show: data.parent.value ? true : false},
            {icon: 'fa fa-sort-amount-asc', field: data.level, show: data.level.value ? true : false},
            {icon: 'fa fa-cube', field: data.title, show: data.title.value ? true : false},
            {icon: 'fa fa-codepen', field: data.alias, show: data.alias.value ? true : false},
            {icon: 'fa fa-eye', field: data.is_active, show: data.is_active.value >= 0 ? true: false},
        ];
    }

    $scope.Init();
}
AdminCatalogListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Catalog').controller('AdminCatalogListCtrl', AdminCatalogListCtrl);