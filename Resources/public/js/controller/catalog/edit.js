function AdminCatalogEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/catalog';
    $scope.statelink.list = 'catalog.catalog_list';
    $scope.statelink.new = 'catalog.catalog_new';
    $scope.breadcrumbs.title = 'CATALOG.EDIT';
    $scope.breadcrumbs.path = [{title: 'CATALOG.LIST', url: 'catalog.catalog_list'}];
    $scope.activeTab = {
        0: $stateParams.tabId === 0 ? true : false,
        1: $stateParams.tabId == 1 ? true : false,
        2: $stateParams.tabId == 2 ? true : false,
        3: $stateParams.tabId == 3 ? true : false
    };

    var Form = new AdminCatalogForm($scope, $timeout);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminCatalogEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Catalog').controller('AdminCatalogEditCtrl', AdminCatalogEditCtrl);