function AdminCatalogTagNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/catalog_tag';
    $scope.statelink.list = 'catalog.tag_list';
    $scope.statelink.new = 'catalog.tag_new';
    $scope.statelink.edit = 'catalog.tag_edit';
    $scope.breadcrumbs.title = 'TAG.NEW';
    $scope.breadcrumbs.path = [{title: 'TAG.LIST', url: 'catalog.tag_list'}];

    var Form = new AdminCatalogTagForm($scope, $timeout);

    $scope.Get.Fields = Form.GetFields;

    $scope.Init();
}
AdminCatalogTagNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Catalog').controller('AdminCatalogTagNewCtrl', AdminCatalogTagNewCtrl);