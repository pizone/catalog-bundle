function AdminCatalogTagListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/catalog_tag';
    $scope.statelink.add = 'catalog.tag_new';
    $scope.statelink.edit = 'catalog.tag_edit';
    $scope.statelink.page = 'catalog.tag_list';
    $scope.breadcrumbs.title = 'TAG.LIST';
    $scope.model = AdminCatalogTag;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'TAG.FIELD.TITLE', name: 'title'},
        {title: 'TAG.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 4]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;
        data.is_active.template = FieldDispatcher.GetLayout('simple_choice');

        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-institution', field: data.project, show: data.project.value ? true : false},
            {icon: 'fa fa-cube', field: data.title, show: data.title.value ? true : false},
            {icon: 'fa fa-eye', field: data.is_active, show: data.is_active.value === null ? false: true}
        ];
    }

    $scope.Init();
}
AdminCatalogTagListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Catalog').controller('AdminCatalogTagListCtrl', AdminCatalogTagListCtrl);