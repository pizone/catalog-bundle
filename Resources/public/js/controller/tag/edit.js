function AdminCatalogTagEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/catalog_tag';
    $scope.statelink.list = 'catalog.tag_list';
    $scope.statelink.new = 'catalog.tag_new';
    $scope.breadcrumbs.title = 'TAG.EDIT';
    $scope.breadcrumbs.path = [{title: 'TAG.LIST', url: 'catalog.tag_list'}];

    var Form = new AdminCatalogTagForm($scope, $timeout);

    $scope.Get.Fields = Form.GetFields;

    $scope.Init();
}
AdminCatalogTagEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Catalog').controller('AdminCatalogTagEditCtrl', AdminCatalogTagEditCtrl);