function AdminCatalogStockNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/catalog_stock';
    $scope.statelink.list = 'catalog.stock_list';
    $scope.statelink.new = 'catalog.stock_new';
    $scope.statelink.edit = 'catalog.stock_edit';
    $scope.breadcrumbs.title = 'STOCK.NEW';
    $scope.breadcrumbs.path = [{title: 'STOCK.LIST', url: 'catalog.stock_list'}];

    var Form = new AdminCatalogStockForm($scope, $timeout);

    $scope.Get.Fields = Form.GetFields;

    $scope.Init();
}
AdminCatalogStockNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Catalog').controller('AdminCatalogStockNewCtrl', AdminCatalogStockNewCtrl);