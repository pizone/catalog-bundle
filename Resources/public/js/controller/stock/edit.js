function AdminCatalogStockEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/catalog_stock';
    $scope.statelink.list = 'catalog.stock_list';
    $scope.statelink.new = 'catalog.stock_new';
    $scope.breadcrumbs.title = 'STOCK.EDIT';
    $scope.breadcrumbs.path = [{title: 'STOCK.LIST', url: 'catalog.stock_list'}];

    var Form = new AdminCatalogStockForm($scope, $timeout);

    $scope.Get.Fields = Form.GetFields;

    $scope.Init();
}
AdminCatalogStockEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Catalog').controller('AdminCatalogStockEditCtrl', AdminCatalogStockEditCtrl);