function AdminCatalogStockListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/catalog_stock';
    $scope.statelink.add = 'catalog.stock_new';
    $scope.statelink.edit = 'catalog.stock_edit';
    $scope.statelink.page = 'catalog.stock_list';
    $scope.breadcrumbs.title = 'STOCK.LIST';
    $scope.model = AdminCatalogStock;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'TAG.FIELD.TITLE', name: 'title'},
        {title: 'TAG.FIELD.ADDRESS', name: 'address'},
        {title: 'TAG.FIELD.LAT', name: 'lat'},
        {title: 'TAG.FIELD.LNG', name: 'lng'},
        {title: 'TAG.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 6, 7]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;
        data.is_active.template = FieldDispatcher.GetLayout('simple_choice');

        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-cube', field: data.title, show: data.title.value ? true : false},
            {icon: 'fa fa-map-marker', field: data.address, show: data.address.value ? true : false},
            {icon: 'fa fa-globe', field: data.lat, show: data.lat.value ? true : false},
            {icon: 'fa fa-globe', field: data.lng, show: data.lng.value ? true : false},
            {icon: 'fa fa-eye', field: data.is_active, show: data.is_active.value === null ? false: true}
        ];
    }

    $scope.Init();
}
AdminCatalogStockListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Catalog').controller('AdminCatalogStockListCtrl', AdminCatalogStockListCtrl);