function AdminCatalogOrderEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/catalog_order';
    $scope.statelink.list = 'shop.order_list';
    $scope.statelink.new = 'shop.order_new';
    $scope.breadcrumbs.title = 'ORDER.EDIT';
    $scope.breadcrumbs.path = [{title: 'ORDER.LIST', url: 'shop.order_list'}];
    $scope.activeTab = {
        0: $stateParams.tabId === 0 ? true : false,
        1: $stateParams.tabId == 1 ? true : false,
        2: $stateParams.tabId == 2 ? true : false,
        3: $stateParams.tabId == 3 ? true : false
    };
    $scope.collectionPrototype = null;
    $scope.collectionIndex = 0;
    $scope.showRemoveCollectionButton = false;

    $scope.breadcrumbs.param = $scope.id;

    var Form = new AdminCatalogOrderForm($scope, $timeout);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminCatalogOrderEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Catalog').controller('AdminCatalogOrderEditCtrl', AdminCatalogOrderEditCtrl);