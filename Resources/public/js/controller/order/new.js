function AdminCatalogOrderNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/catalog_order';
    $scope.statelink.list = 'shop.order_list';
    $scope.statelink.new = 'shop.order_new';
    $scope.statelink.edit = 'shop.order_edit';
    $scope.breadcrumbs.title = 'ORDER.NEW';
    $scope.breadcrumbs.path = [{title: 'ORDER.LIST', url: 'shop.order_list'}];
    $scope.collectionPrototype = null;
    $scope.collectionIndex = 0;
    $scope.showRemoveCollectionButton = false;

    var Form = new AdminCatalogOrderForm($scope, $timeout);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminCatalogOrderNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Catalog').controller('AdminCatalogOrderNewCtrl', AdminCatalogOrderNewCtrl);