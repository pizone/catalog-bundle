function AdminCatalogOrderListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/catalog_order';
    $scope.statelink.add = 'shop.order_new';
    $scope.statelink.edit = 'shop.order_edit';
    $scope.statelink.page = 'shop.order_list';
    $scope.breadcrumbs.title = 'ORDER.LIST';
    $scope.model = AdminCatalogOrder;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'ORDER.FIELD.GOODS'},
        {title: 'ORDER.FIELD.USER.TITLE', name: 'user.username'},
        {title: 'ORDER.FIELD.ITOG', name: 'itog'},
        {title: 'ORDER.FIELD.DISCOUNT', name: 'discount'},
        {title: 'ORDER.FIELD.STATUS', name: 'status'},
        {title: 'ORDER.FIELD.STOCK', name: 'stock'},
        {title: 'ORDER.FIELD.CREATED_AT', name: 'created_at'},
        {title: 'ORDER.FIELD.UPDATED_AT', name: 'updated_at'},
        {title: 'ORDER.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'ORDER.FIELD.IS_ORDER.LABEL', name: 'is_order'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 6, 8, 12]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;
        data.is_active.template = FieldDispatcher.GetLayout('simple_choice');

        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-institution', field: data.project, show: data.project.value ? true : false},
            // {icon: 'fa fa-cube', field: data.title, show: data.title.value ? true : false},
            // {icon: 'fa fa-map-marker', field: data.address, show: data.address.value ? true : false},
            // {icon: 'fa fa-globe', field: data.lat, show: data.lat.value ? true : false},
            // {icon: 'fa fa-globe', field: data.lng, show: data.lng.value ? true : false},
            {icon: 'fa fa-eye', field: data.is_active, show: data.is_active.value === null ? false: true},
            {icon: 'fa fa-eye', field: data.is_confirm, show: data.is_confirm.value === null ? false: true}
        ];
    }

    $scope.Init();
}
AdminCatalogOrderListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Catalog').controller('AdminCatalogOrderListCtrl', AdminCatalogOrderListCtrl);