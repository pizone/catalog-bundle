function AdminCatalogContentListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore, $modal){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/catalog_content';
    $scope.statelink.add = 'catalog.content_new';
    $scope.statelink.edit = 'catalog.content_edit';
    $scope.statelink.page = 'catalog.content_list';
    $scope.breadcrumbs.title = 'CCONTENT.LIST';
    $scope.model = AdminCatalogContent;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'CCONTENT.FIELD.IMAGE', name: 'image'},
        {title: 'CCONTENT.FIELD.TITLE', name: 'title'},
        {title: 'CCONTENT.FIELD.ARTICLE'},
        {title: 'CCONTENT.FIELD.ALIAS', name: 'alias'},
        {title: 'CCONTENT.FIELD.ANONS', name: 'anons'},
        {title: 'CCONTENT.FIELD.CATALOG'},
        {title: 'CCONTENT.FIELD.CATEGORY', name: 'category.title'},
        {title: 'CCONTENT.FIELD.PARAMETERS'},
        {title: 'CCONTENT.FIELD.CREATED_AT', name: 'created_at'},
        {title: 'CCONTENT.FIELD.UPDATED_AT', name: 'updated_at'},
        {title: 'CCONTENT.FIELD.IS_NEW.LABEL', name: 'is_new'},
        {title: 'CCONTENT.FIELD.IS_HIT.LABEL', name: 'is_hit'},
        {title: 'CCONTENT.FIELD.IS_OLD.LABEL', name: 'is_old'},
        {title: 'CCONTENT.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 3, 4, 8, 9, 15, 16]
    };

    $scope.Callback.GetData = CreateFilters;

    $scope.Click.Batch.SetParameters = SetParameters;

    function CreateFilters() {
        var data = $scope.filters;
        data.is_active.template = FieldDispatcher.GetLayout('simple_choice');
        data.is_new.template = FieldDispatcher.GetLayout('simple_choice');
        data.is_hit.template = FieldDispatcher.GetLayout('simple_choice');
        data.is_old.template = FieldDispatcher.GetLayout('simple_choice');

        data.created_at.type = 'date_range';
        data.created_at.template = FieldDispatcher.GetLayout('date_range');

        data.updated_at.type = 'date_range';
        data.updated_at.template = FieldDispatcher.GetLayout('date_range');

        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-institution', field: data.project, show: data.project.value ? true : false},
            {icon: 'fa fa-cube', field: data.title, show: data.title.value ? true : false},
            {icon: 'fa fa-code', field: data.article_field, show: data.article_field.value ? true : false},
            {icon: 'fa fa-codepen', field: data.alias, show: data.alias.value ? true : false},
            {icon: 'fa fa-folder-open-o', field: data.catalog, show: data.catalog.value ? true : false},
            {icon: 'fa fa-folder-open-o', field: data.category, show: data.category.value ? true : false},
            {icon: 'fa fa-file', field: data.anons, show: data.anons.value ? true: false},
            {icon: 'fa fa-lock', field: data.updated_at, show: $scope.ShowDateFilter(data.updated_at.form)},
            {icon: 'fa fa-lock', field: data.created_at, show: $scope.ShowDateFilter(data.created_at.form)},
            {icon: 'fa fa-eye', field: data.is_active, show: data.is_active.value !== null ? true: false},
            {icon: 'fa fa-exclamation', field: data.is_new, show: data.is_new.value !== null ? true: false},
            {icon: 'fa fa-star', field: data.is_hit, show: data.is_hit.value !== null ? true: false},
            {icon: 'fa fa-archive', field: data.is_old, show: data.is_old.value !== null ? true: false},
        ];
    }

    function SetParameters(){
        $modal.open({
            templateUrl: '/bundles/pizonecatalog/tmpl/catalog_content/_parameters.html',
            size: 'lg',
            controller: AdminModalParametersCtrl,
            resolve: {
                selectedId: function(){
                    var content = $scope.Get.Selected(),
                        ids = [];

                    for(var i=0; i <= content.length-1; i++){
                        ids.push(content[i].id);
                    }
                    return ids.join(',');
                },
                callback: function() {
                    return $scope.Get.Layout;
                }
            }
        });
    }

    $scope.Init();
}
AdminCatalogContentListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore', '$modal'];
angular.module('PiZone.Catalog').controller('AdminCatalogContentListCtrl', AdminCatalogContentListCtrl);