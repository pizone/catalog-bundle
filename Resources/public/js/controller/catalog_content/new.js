function AdminCatalogContentNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter, $q){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/catalog_content';
    $scope.statelink.list = 'catalog.content_list';
    $scope.statelink.new = 'catalog.content_new';
    $scope.statelink.edit = 'catalog.content_edit';
    $scope.breadcrumbs.title = 'CCONTENT.NEW';
    $scope.breadcrumbs.path = [{title: 'CCONTENT.LIST', url: 'catalog.content_list'}];
    $scope.collectionIndex = 0;
    $scope.showRemoveCollectionButton = false;

    var Form = new AdminCatalogContentForm($scope, $timeout, $http, $q);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminCatalogContentNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter', '$q'];
angular.module('PiZone.Catalog').controller('AdminCatalogContentNewCtrl', AdminCatalogContentNewCtrl);