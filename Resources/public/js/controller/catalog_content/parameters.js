function AdminModalParametersCtrl($scope, $http, $state, $timeout, SweetAlert, $filter, $modalInstance, selectedId, callback){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/catalog_content';
    $scope.formId = 'parametersFormId';
    $scope.fields = {};

    var Form = new AdminParametersForm($scope, $timeout, $http);

    $scope.Get.Fields = Form.GetFields;
    $scope.Callback.Success = CallbackSubmitSuccess;
    $scope.Click.Cancel = ClickCancel;
    $scope.Click.Submit = Submit;
    $scope.GetBaseQuery = GetBaseQuery;
    $scope.Callback.Init = CallbackInit;

    function CallbackInit(){
        $scope.fields[0][0].field.value = selectedId;
    }

    function GetBaseQuery(){
        return PiZoneConfig.apiPrefix + '/' + $scope.prefix + '/categories';
    }

    function Submit(){
        if($scope.IsValid()) {
            var container = $('#' + $scope.formId);
            var loader = new Loader(container);
            $scope.Befor.Submit();
            if (loader.ready === false)
                loader.Create();

            $http({
                url: PiZoneConfig.apiPrefix + '/' + $scope.prefix + '/save_categories',
                method: 'POST',
                data: $scope.Get.Values(),
                headers: {'Content-Type': undefined}
            }).then(
                function(message){
                    message = JSON.parse(message.data);
                    loader.Delete();
                    if (message.result == 'ok')
                        $scope.Callback.Success(message);
                    else
                        $scope.Callback.Error(message);
                },
                function(message){
                    loader.Delete();
                    $modalInstance.close();
                    if (message.status == 400) {
                        message = JSON.parse(message.data);
                        $timeout(function() {
                            $scope.Callback.Error(message);
                        });
                    }
                    else
                        $scope.ViewNotify('submit', 'error');
                });
        }
    }

    function CallbackSubmitSuccess(message){
        $modalInstance.close();
        callback();
        $scope.ViewNotify('submit', 'success');
    }

    function ClickCancel(){
        $modalInstance.close();
    }

    $scope.Init();
}
AdminModalParametersCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter', '$modalInstance', 'selectedId', 'callback'];
angular.module('PiZone.Catalog').controller('AdminModalParametersCtrl', AdminModalParametersCtrl);