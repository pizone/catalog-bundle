function AdminCatalogContentEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter, $q){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/catalog_content';
    $scope.statelink.list = 'catalog.content_list';
    $scope.statelink.new = 'catalog.content_new';
    $scope.breadcrumbs.title = 'CCONTENT.EDIT';
    $scope.breadcrumbs.path = [{title: 'CCONTENT.LIST', url: 'catalog.content_list'}];
    $scope.activeTab = {
        0: $stateParams.tabId === 0 ? true : false,
        1: $stateParams.tabId == 1 ? true : false,
        2: $stateParams.tabId == 2 ? true : false,
        3: $stateParams.tabId == 3 ? true : false
    };
    $scope.collectionIndex = 0;
    $scope.showRemoveCollectionButton = false;

    var Form = new AdminCatalogContentForm($scope, $timeout, $http, $q);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminCatalogContentEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter', '$q'];
angular.module('PiZone.Catalog').controller('AdminCatalogContentEditCtrl', AdminCatalogContentEditCtrl);