function AdminCatalogProjectNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/catalog_project';
    $scope.statelink.list = 'catalog.project_list';
    $scope.statelink.new = 'catalog.project_new';
    $scope.statelink.edit = 'catalog.project_edit';
    $scope.breadcrumbs.title = 'STOCK.NEW';
    $scope.breadcrumbs.path = [{title: 'PROJECT.LIST', url: 'catalog.project_list'}];
    $scope.collectionPrototype = null;
    $scope.collectionIndex = 0;
    $scope.showRemoveCollectionButton = false;

    var Form = new AdminCatalogProjectForm($scope, $timeout);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminCatalogProjectNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Catalog').controller('AdminCatalogProjectNewCtrl', AdminCatalogProjectNewCtrl);