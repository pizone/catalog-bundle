function AdminCatalogProjectListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/catalog_project';
    $scope.statelink.add = 'catalog.project_new';
    $scope.statelink.edit = 'catalog.project_edit';
    $scope.statelink.page = 'catalog.project_list';
    $scope.breadcrumbs.title = 'PROJECT.LIST';
    $scope.model = AdminCatalogProject;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'PROJECT.FIELD.TITLE', name: 'title'},
        {title: 'PROJECT.FIELD.ALIAS', name: 'alias'},
        {title: 'PROJECT.FIELD.DESCRIPTION', name: 'description'},
        {title: 'PROJECT.FIELD.IS_ACTIVE.LABEL', name: 'is_active'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 6]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;
        data.is_active.template = FieldDispatcher.GetLayout('simple_choice');

        $scope._token.filter = data._token;

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-cube', field: data.title, show: data.title.value ? true : false},
            {icon: 'fa fa-cube', field: data.alias, show: data.alias.value ? true : false},
            {icon: 'fa fa-comment', field: data.description, show: data.description.value ? true : false},
            {icon: 'fa fa-eye', field: data.is_active, show: data.is_active.value === null ? false: true}
        ];
    }

    $scope.Init();
}
AdminCatalogProjectListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Catalog').controller('AdminCatalogProjectListCtrl', AdminCatalogProjectListCtrl);