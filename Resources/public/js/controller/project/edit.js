function AdminCatalogProjectEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/catalog_project';
    $scope.statelink.list = 'catalog.project_list';
    $scope.statelink.new = 'catalog.project_new';
    $scope.breadcrumbs.title = 'PROJECT.EDIT';
    $scope.breadcrumbs.path = [{title: 'PROJECT.LIST', url: 'catalog.project_list'}];
    $scope.activeTab = {
        0: $stateParams.tabId === 0 ? true : false,
        1: $stateParams.tabId == 1 ? true : false,
        2: $stateParams.tabId == 2 ? true : false,
        3: $stateParams.tabId == 3 ? true : false
    };
    $scope.collectionPrototype = null;
    $scope.collectionIndex = 0;
    $scope.showRemoveCollectionButton = false;

    var Form = new AdminCatalogProjectForm($scope, $timeout);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminCatalogProjectEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Catalog').controller('AdminCatalogProjectEditCtrl', AdminCatalogProjectEditCtrl);