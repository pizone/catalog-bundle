function AdminCatalogUserEditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter){
    angular.extend($scope, new EditCtrl($scope, $http, $state, $stateParams, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/catalog_user';
    $scope.statelink.list = 'user.catalog_user_list';
    $scope.statelink.new = 'user.catalog_user_new';
    $scope.breadcrumbs.title = 'CUSER.EDIT';
    $scope.breadcrumbs.path = [{title: 'CUSER.LIST', url: 'user.catalog_user_list'}];

    var Form = new AdminCatalogUserForm($scope);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminCatalogUserEditCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Catalog').controller('AdminCatalogUserEditCtrl', AdminCatalogUserEditCtrl);