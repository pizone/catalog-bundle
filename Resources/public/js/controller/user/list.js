function AdminCatalogUserListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore){
    angular.extend($scope, new ListCtrl($scope, $http, $state, $stateParams, SweetAlert, $timeout, $filter, $cookieStore));
    $scope.prefix = 'admin/catalog_user';
    $scope.statelink.add = 'user.catalog_user_new';
    $scope.statelink.edit = 'user.catalog_user_edit';
    $scope.statelink.page = 'user.catalog_user_list';
    $scope.breadcrumbs.title = 'USER.LIST';
    $scope.model = AdminCatalogUser;
    $scope.columns = [
        {show: true},
        {title: 'ID', name: 'id'},
        {title: 'USER.FIELD.USERNAME', name: 'user.username'},
        {title: 'USER.FIELD.CONTENT'},
        {title: 'USER.FIELD.COMPANY', name: 'company'},
        {title: 'USER.FIELD.POSITION_HELD', name: 'positionHeld'},
        {title: 'USER.FIELD.ADDRESS', name: 'address'},
        {title: 'USER.FIELD.EMAIL', name: 'email'},
        {title: 'USER.FIELD.PHONE', name: 'phone'},
        {title: 'USER.FIELD.MESSENGER', name: 'messenger'},
        {title: 'USER.FIELD.REGISTRATION_AT', name: 'registration_at'},
        {title: 'USER.FIELD.LAST_LOGIN', name: 'last_login'},
        {title: 'USER.FIELD.ROLES'},
        {title: 'USER.FIELD.ENABLED.LABEL', name: 'enabled'},
        {title: 'COMMON.ACTIONS'}
    ];
    $scope.showDefaultColumns = {
        all: [0, 1, 2, 3, 7, 8, 10, 13, 14]
    };

    $scope.Callback.GetData = CreateFilters;

    function CreateFilters() {
        var data = $scope.filters;

        data.enabled.template = FieldDispatcher.GetLayout('simple_choice');
        $scope._token.filter = data._token;
        data.registration_at.type = 'date_range';
        data.registration_at.template = FieldDispatcher.GetLayout('date_range');
        data.last_login.type = 'date_range';
        data.last_login.template = FieldDispatcher.GetLayout('date_range');

        $scope.filters = [
            {icon: 'fa fa-cubes', field: data.all, show: true},
            {icon: 'fa fa-user', field: data.username, show: data.username.value ? true : false},
            {icon: 'fa fa-envelope', field: data.email, show: data.email.value ? true : false},
            {icon: 'fa fa-clock', field: data.registration_at, show: $scope.ShowDateFilter(data.registration_at.form)},
            {icon: 'fa fa-clock', field: data.last_login, show: $scope.ShowDateFilter(data.last_login.form)},
            {icon: 'fa fa-eye', field: data.enabled, show: data.enabled.value >= 0 ? true: false},
        ];
    }

    $scope.Init();
}
AdminCatalogUserListCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', 'SweetAlert', '$timeout', '$filter', '$cookieStore'];
angular.module('PiZone.Catalog').controller('AdminCatalogUserListCtrl', AdminCatalogUserListCtrl);