function AdminCatalogUserNewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter){
    angular.extend($scope, new NewCtrl($scope, $http, $state, $timeout, SweetAlert, $filter));
    $scope.prefix = 'admin/catalog_user';
    $scope.statelink.list = 'user.catalog_user_list';
    $scope.statelink.new = 'user.catalog_user_new';
    $scope.statelink.edit = 'user.catalog_user_edit';
    $scope.breadcrumbs.title = 'CUSER.NEW';
    $scope.breadcrumbs.path = [{title: 'CUSER.LIST', url: 'user.catalog_user_list'}];

    var Form = new AdminCatalogUserForm($scope);

    $scope.Get.Tabs = Form.GetTabs;

    $scope.Init();
}
AdminCatalogUserNewCtrl.$inject = ['$scope', '$http', '$state', '$timeout', 'SweetAlert', '$filter'];
angular.module('PiZone.Catalog').controller('AdminCatalogUserNewCtrl', AdminCatalogUserNewCtrl);