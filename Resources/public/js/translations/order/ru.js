var ruCatalogOrderPZTranslates = {
    LIST: 'Заказы',
    NEW: 'Новый заказ',
    EDIT: 'Редактировать заказ - "{{param}}"',
    TAB: {
        GENERAL: {
            TITLE: 'Основные данные'
        },
        GOODS: {
            TITLE: 'Товары в заказе'
        }
    },
    FIELD: {
        USER: {
            TITLE: 'Пользователь',
            USERNAME: 'Логин',
            FULL_NAME: 'Ф.И.О.',
            COMPANY: 'Компания',
            POSITION_HELD: 'Должность',
            ADDRESS: 'Адресс',
            REGISTRATION_AT: 'Дата регистрации',
            EMAIL: 'Email',
            PHONE: 'Телефон',
            MESSENGER: 'Скайп',
            LOCKED: 'Заблокирован'
        },
        CONTENT: {
            TITLE: 'Название',
            ARTICLE: 'Артикль',
            QUANTITY: 'Количество',
            PRICE: 'Цена',
            DISCOUNT: 'Скидка',
            CATEGORY: 'Категория',
            CATALOG: 'Каталог'
        },
        GOODS: 'Продукция',
        QUANTITY: 'Количество',
        STATUS: {
            LABEL: 'Статус заказа',
            INIT: 'Новый',
            PROCESSING: 'В обработке',
            PAYED: 'Оплачен',
            DELIVERY: 'Доставлен',
            CLOSED: 'Закрыт'
        },
        ITOG: 'Итого',
        DISCOUNT: 'Скидка',
        COMMENT: 'Комментарий',
        STOCK: 'Склад',
        CREATED_AT: 'Дата заказа',
        UPDATED_AT: 'Дата обновления',
        ADDRESS: 'Адрес',
        IS_ACTIVE: {
            LABEL: 'Активен?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        IS_CONFIRM: {
            LABEL: 'Подтвержден?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Пожалуйста введите название склада.',
            ADDRESS: 'Пожалуйста введите адрес.'
        }
    }
};