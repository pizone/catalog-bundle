var enCatalogOrderPZTranslates = {
    LIST: 'Orders',
    NEW: 'New order',
    EDIT: 'Edit order - "{{param}}"',
    TAB: {
        GENERAL: {
            TITLE: 'Основные данные'
        },
        GOODS: {
            TITLE: 'Товары в заказе'
        }
    },
    FIELD: {
        USER: {
            TITLE: 'User',
            USERNAME: 'Login',
            FULL_NAME: 'Full name',
            COMPANY: 'Company',
            POSITION_HELD: 'Position held',
            ADDRESS: 'Address',
            REGISTRATION_AT: 'Registration date',
            EMAIL: 'Email',
            PHONE: 'Phone',
            MESSENGER: 'Skype',
            LOCKED: 'Locked'
        },
        CONTENT: {
            TITLE: 'Название',
            ARTICLE: 'Артикль',
            QUANTITY: 'Количество',
            PRICE: 'Цена',
            DISCOUNT: 'Скидка',
            CATEGORY: 'Категория',
            CATALOG: 'Каталог'
        },
        GOODS: 'Продукция',
        QUANTITY: 'Количество',
        STATUS: {
            LABEL: 'Статус заказа',
            INIT: 'Новый',
            PROCESSING: 'В обработке',
            PAYED: 'Оплачен',
            DELIVERY: 'Доставлен',
            CLOSED: 'Закрыт'
        },
        ITOG: 'Итого',
        DISCOUNT: 'Скидка',
        COMMENT: 'Комментарий',
        STOCK: 'Склад',
        CREATED_AT: 'Дата заказа',
        UPDATED_AT: 'Дата обновления',
        ADDRESS: 'Адрес',
        IS_ACTIVE: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        IS_CONFIRM: {
            LABEL: 'Is confirm?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Please enter title.',
            ADDRESS: 'Please enter stock address.'
        }
    }
};