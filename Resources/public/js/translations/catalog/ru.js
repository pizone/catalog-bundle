var ruCatalogPZTranslates = {
    LIST: 'Каталог',
    NEW: 'Новая раздел каталога',
    EDIT: 'Редактировать раздел каталога - "{{param}}"',
    TAB:{
        GENERAL: {
            TITLE: 'Основные данные'
        },
        META: {
            TITLE: 'SEO'
        }
    },
    FIELD: {
        TITLE: 'Заголовок',
        ALIAS: 'Псевдоним',
        PARENT: 'Родительский раздел',
        CONTENT: 'Связанный контент',
        IN_MENU: 'Отображать в меню',
        IN_BREADCRUMBS: 'Отображать в хлебных крошках',
        IN_SITE_MAP: 'Добавить в карту сайта',
        IN_ROBOTS: 'Обрабатывать роботом',
        CREATED_AT: 'Дата создания',
        LEVEL: 'Уровень',
        PROJECT: 'Проект',
        IS_ACTIVE: {
            LABEL: 'Активен?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        META_TITLE: 'Meta title',
        META_KEYWORDS: 'Meta keywords',
        META_DESCRIPTION: 'Meta description',
        META_SCRIPTS: 'Дополнительные скрипты',
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Введите название страницы.',
            ALIAS: 'Введите псевдоним страницы.'
        }
    }
};