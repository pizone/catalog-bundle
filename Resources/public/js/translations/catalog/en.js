var enCatalogPZTranslates = {
    LIST: 'Catalog',
    NEW: 'New item',
    EDIT: 'Edit item - "{{param}}"',
    TAB:{
        GENERAL: {
            TITLE: 'Main data'
        },
        META: {
            TITLE: 'SEO'
        }
    },
    FIELD: {
        TITLE: 'Title',
        ALIAS: 'Alias',
        PARENT: 'Parent section',
        CONTENT: 'Related content',
        IN_MENU: 'Show menu',
        IN_BREADCRUMBS: 'Show in breadcrumbs',
        IN_SITE_MAP: 'Show in site map',
        IN_ROBOTS: 'Handle robot',
        CREATED_AT: 'Date of creation',
        LEVEL: 'Level',
        PROJECT: 'Project',
        IS_ACTIVE: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        META_TITLE: 'Meta title',
        META_KEYWORDS: 'Meta keywords',
        META_DESCRIPTION: 'Meta description',
        META_SCRIPTS: 'More scripts',
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Please enter title.',
            ALIAS: 'Please enter alias.'
        }
    }
};