var enCatalogStockPZTranslates = {
    LIST: 'Stocks',
    NEW: 'New stock',
    EDIT: 'Edit stock - "{{param}}"',
    FIELD: {
        TITLE: 'Title',
        ADDRESS: 'Address',
        LAT: 'Latitude',
        LNG: 'Longitude',
        IS_ACTIVE: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Please enter title.',
            ADDRESS: 'Please enter stock address.'
        }
    }
};