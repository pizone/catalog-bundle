var ruCatalogStockPZTranslates = {
    LIST: 'Склады',
    NEW: 'Новый склад',
    EDIT: 'Редактировать склад - "{{param}}"',
    FIELD: {
        TITLE: 'Название',
        ADDRESS: 'Адрес',
        LAT: 'Широта',
        LNG: 'Долгота',
        IS_ACTIVE: {
            LABEL: 'Активен?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Пожалуйста введите название склада.',
            ADDRESS: 'Пожалуйста введите адрес.'
        }
    }
};