var enCatalogCategoryPZTranslates = {
    LIST: 'Categories',
    NEW: 'New category',
    EDIT: 'Edit category - "{{param}}"',
    TAB:{
        GENERAL: {
            TITLE: 'Main data'
        },
        CONTENT: {
            TITLE: 'Content'
        },
        GROUP: {
            TITLE: 'Parameters group'
        }
    },
    FIELD: {
        TITLE: 'Category name',
        ALIAS: 'Alias',
        ANONS: 'Anons',
        PARENT: 'Parent category',
        IMAGE: 'Image',
        EDITOR_CONTENT: 'Use the visual editor',
        GROUPS: 'Parameters group',
        IS_ACTIVE: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Please enter category name.',
            ALIAS: 'Please enter alias.'
        }
    }
};