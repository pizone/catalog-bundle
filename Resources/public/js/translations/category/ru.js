var ruCatalogCategoryPZTranslates = {
    LIST: 'Категории',
    NEW: 'Новая категория',
    EDIT: 'Редактировать категорию - "{{param}}"',
    TAB:{
        GENERAL: {
            TITLE: 'Основные данные'
        },
        CONTENT: {
            TITLE: 'Контент'
        },
        GROUP: {
            TITLE: 'Группы параметров'
        }
    },
    FIELD: {
        TITLE: 'Название категории',
        ALIAS: 'Псевдоним',
        ANONS: 'Анонс',
        PARENT: 'Родительская категория',
        IMAGE: 'Изображение',
        EDITOR_CONTENT: 'Использовать визуальный редактор',
        GROUPS: 'Группы параметров',
        IS_ACTIVE: {
            LABEL: 'Активна?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любая'
        },
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Пожалуйста введите название категории.',
            ALIAS: 'Пожалуйста введите псевдоним.'
        }
    }
};