var ruCatalogGroupParametersPZTranslates = {
    LIST: 'Группы параметров',
    NEW: 'Новая группа параметров',
    EDIT: 'Редактировать группу параметров - "{{param}}"',
    TAB:{
        GENERAL: {
            TITLE: 'Основное'
        },
        PARAMS: {
            TITLE: 'Параметры'
        }
    },
    FIELD: {
        TITLE: 'Название группы',
        ALIAS: 'Псевдоним',
        DESCRIPTION: 'Описание',
        PARAMS: {
            TITLE: 'Название параметра',
            ALIAS: 'Псевдоним',
            IS_ACTIVE: 'Активен ?'
        },
        IS_ACTIVE: {
            LABEL: 'Активна ?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любая'
        },
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Пожалуйста введите название группы.',
            PTITLE: 'Пожалуйста введите название параметра.',
            ALIAS: 'Пожалуйста введите псевдоним параметра.'
        },
        UNIQUE: {
            ALIAS: 'У параметра должен быть уникальный псевдоним'
        }
    }
};