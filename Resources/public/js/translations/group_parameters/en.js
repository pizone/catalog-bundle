var enCatalogGroupParametersPZTranslates = {
    LIST: 'Group parameters',
    NEW: 'New group parameters',
    EDIT: 'Edit group parameters - "{{param}}"',
    TAB:{
        GENERAL: {
            TITLE: 'Main data'
        },
        PARAMS: {
            TITLE: 'Parameters'
        }
    },
    FIELD: {
        TITLE: 'Group name',
        ALIAS: 'Alias',
        DESCRIPTION: 'Description',
        PARAMS: {
            TITLE: 'Parameter name',
            ALIAS: 'Alias',
            IS_ACTIVE: 'Is active ?'
        },
        IS_ACTIVE: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Please enter group name.',
            PTITLE: 'Please enter parameter name.',
            ALIAS: 'Please enter alias.'
        },
        UNIQUE: {
            ALIAS: 'In parameter must have a unique alias'
        }
    }
};