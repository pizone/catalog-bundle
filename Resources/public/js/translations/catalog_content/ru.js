var ruCatalogContentPZTranslates = {
    LIST: 'Контент',
    NEW: 'Новая контент',
    EDIT: 'Редактировать контент - "{{param}}"',
    SET_PARAMETERS: 'Установить параметры',
    TAB:{
        GENERAL: {
            TITLE: 'Основные данные'
        },
        ANONS: {
            TITLE: 'Анонс'
        },
        CONTENT: {
            TITLE: 'Контент'
        },
        MORE: {
            TITLE: 'Дополнительные поля'
        },
        CATEGORY: {
            TITLE: 'Параметры'
        },
        RELATED: {
            TITLE: 'Связанные товары'
        },
        SALE: {
            TITLE: 'Продажи'
        },
        META: {
            TITLE: 'SEO'
        }
    },
    FIELD: {
        TITLE: 'Заголовок',
        ALIAS: 'Псевдоним',
        ANONS: 'Анонс',
        PARENT: 'Родительская категория',
        IMAGE: 'Изображение анонса',
        BIG_IMAGE: 'Изображение',
        EDITOR_CONTENT: 'Использовать визуальный редактор',
        CATEGORY: 'Категория',
        CATALOG: 'Каталог',
        PARAMETERS: 'Параметры',
        ARTICLE: 'Артикль',
        CONTENT: 'Контент',
        PURCHASE_PRICE: 'Закупочная',
        RETAIL_PRICE: 'Розничная',
        DISCOUNT: 'Скидка (в проценах)',
        VAT: 'Включает НДС ?',
        BARCODE: 'Штрихкод',
        WIDTH: 'Ширина',
        HEIGHT: 'Высота',
        LENGTH: 'Длина',
        WEIGHT: 'Масса',
        CREATED_AT: 'Дата создания',
        UPDATED_AT: 'Дата обновления',
        META_TITLE: 'Meta title',
        META_KEYWORDS: 'Meta keywords',
        META_DESCRIPTION: 'Meta description',
        META_SCRIPTS: 'Дополнительные скрипты',
        HASH_TAG: 'Хеш тег',
        STOCKS: 'Склады',
        QUANTITY: 'Всего на складе',
        RESERVED: 'Зарезервировано',
        IS_ACTIVE: {
            LABEL: 'Активна?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любая'
        },
        IS_NEW: {
            LABEL: 'Новый?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        IS_HIT: {
            LABEL: 'Хит?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        IS_OLD: {
            LABEL: 'Устаревший?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Пожалуйста введите заголовок.',
            ALIAS: 'Пожалуйста введите псевдоним.',
            CATEGORY: 'Пожалуйста выберите категорию.',
            CONTENT: 'Не выбрано ни одного контента.'
        },
        REGEXP: {
            INT: 'Поле должно содержать цифровое значение.',
            FLOAT: 'Поле должно содержать цифровое значение.'
        }
    }
};