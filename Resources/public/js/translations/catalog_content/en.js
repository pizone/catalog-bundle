var enCatalogContentPZTranslates = {
    LIST: 'Content list',
    NEW: 'New content',
    EDIT: 'Edit content - "{{param}}"',
    SET_PARAMETERS: 'Set parameters',
    TAB:{
        GENERAL: {
            TITLE: 'Main data'
        },
        ANONS: {
            TITLE: 'Anons'
        },
        CONTENT: {
            TITLE: 'Content'
        },
        MORE: {
            TITLE: 'More fields'
        },
        CATEGORY: {
            TITLE: 'Parameters'
        },
        RELATED: {
            TITLE: 'Related goods'
        },
        SALE: {
            TITLE: 'Sale'
        },
        META: {
            TITLE: 'SEO'
        }
    },
    FIELD: {
        TITLE: 'Title',
        ALIAS: 'Alias',
        ANONS: 'Anons',
        IMAGE: 'Anons image',
        BIG_IMAGE: 'Image',
        EDITOR_CONTENT: 'Use the visual editor',
        CATEGORY: 'Category',
        CATALOG: 'Catalog',
        PARAMETERS: 'Parameters',
        ARTICLE: 'Article',
        CONTENT: 'Content',
        PURCHASE_PRICE: 'Purchase price',
        RETAIL_PRICE: 'Retail price',
        DISCOUNT: 'Discount (percent)',
        VAT: 'VAT',
        BARCODE: 'Barcode',
        WIDTH: 'Width',
        HEIGHT: 'Height',
        LENGTH: 'Length',
        WEIGHT: 'Weight',
        CREATED_AT: 'Created at',
        UPDATED_AT: 'Updated at',
        META_TITLE: 'Meta title',
        META_KEYWORDS: 'Meta keywords',
        META_DESCRIPTION: 'Meta description',
        META_SCRIPTS: 'More scripts',
        HASH_TAG: 'Hash tag',
        STOCKS: 'Stocks',
        QUANTITY: 'Total in stock',
        RESERVED: 'Reserved',
        IS_ACTIVE: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        IS_NEW: {
            LABEL: 'Is new?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        IS_HIT: {
            LABEL: 'Is hit?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        IS_OLD: {
            LABEL: 'Is old?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Please enter content name.',
            ALIAS: 'Please enter alias.',
            CATEGORY: 'Please select category.',
            CONTENT: 'content not selected.'
        },
        REGEXP: {
            INT: 'The field must contain a numeric value.',
            FLOAT: 'The field must contain a numeric value.'
        }
    }
};