var ruCatalogUserPZTranslates = {
    LIST: 'Пользователи',
    ADMIN_LIST: 'Администраторы',
    BUYER_LIST: 'Покупатели',
    NEW: 'Новый пользователь',
    EDIT: 'Редактировать пользователя - "{{param}}"',
    TAB: {
        GENERAL:{
            TITLE: 'Основное'
        },
        SECURITY:{
            TITLE: 'Доступ'
        }
    },
    FIELD: {
        USERNAME: 'Логин',
        FULL_NAME: 'Ф.И.О.',
        EMAIL: 'Email',
        PHONE: 'Телефон',
        COMPANY: 'Компания',
        POSITION_HELD: 'Должность',
        MESSENGER: 'Скайп',
        ADDRESS: 'Адрес',
        ENABLED: {
            LABEL: 'Активен?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        ROLES: 'Роли',
        REGISTRATION_AT: 'Дата регистрации',
        LAST_LOGIN: 'Дата последнего входа',
        PASSWORD: 'Пароль',
        PASSWORD_CONFIRMATION: 'Подтвердите пароль',
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            USERNAME: 'Введите имя пользователя.',
            EMAIL: 'Введите email.',
            PASSWORD: 'Введите пароль.'
        },
        REGEXP: {
            EMAIL: 'Строка не является адресом email'
        },
        REPEAT: {
            PASSWORD: 'Пароли не совпадают.'
        }
    }
};