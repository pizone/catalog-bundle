var enCatalogUserPZTranslates = {
    LIST: 'Users',
    ADMIN_LIST: 'Management',
    BUYER_LIST: 'Buyers',
    NEW: 'New user',
    EDIT: 'Edit user - "{{param}}"',
    TAB: {
        GENERAL:{
            TITLE: 'Main'
        },
        SECURITY:{
            TITLE: 'Security'
        }
    },
    FIELD: {
        USERNAME: 'Username',
        FULL_NAME: 'Full name',
        EMAIL: 'Email',
        PHONE: 'Phone',
        COMPANY: 'Company',
        POSITION_HELD: 'Position held',
        MESSENGER: 'Messenger',
        ADDRESS: 'Address',
        ENABLED: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        ROLES: 'Roles',
        REGISTRATION_AT: 'Registration date',
        LAST_LOGIN: 'Last login date',
        PASSWORD: 'Password',
        PASSWORD_CONFIRMATION: 'Password confirmation',
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            USERNAME: 'Please enter username.',
            EMAIL: 'Please enter email.',
            PASSWORD: 'Please enter password'
        },
        REGEXP: {
            EMAIL: 'No valid email'
        },
        REPEAT: {
            PASSWORD: 'Passwords do not match.'
        }
    }
};