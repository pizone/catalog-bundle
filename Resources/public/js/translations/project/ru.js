var ruCatalogProjectPZTranslates = {
    LIST: 'Проекты',
    NEW: 'Новый проект',
    EDIT: 'Редактировать проект - "{{param}}"',
    TAB:{
        GENERAL: {
            TITLE: 'Основное'
        },
        FIELDS: {
            TITLE: 'Дополнительные поля'
        }
    },
    FIELD: {
        TITLE: 'Название',
        ALIAS: 'Псевдоним',
        DESCRIPTION: 'Описание',
        FIELD: 'Тип поля',
        FIELD_PLACEHOLDER: 'Выберите тип поля',
        DEFAULT_VALUE: 'Значение по умолчанию',
        SORT: 'Сортировка',
        IS_ACTIVE: {
            LABEL: 'Активен?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            FIELD: 'Выберите тип поля',
            TITLE: 'Пожалуйста введите название.',
            ALIAS: 'Пожалуйста введите прсевдоним.'
        },
        UNIQUE: {
            ALIAS: 'У поля должен быть уникальный псевдоним'
        }
    }
};