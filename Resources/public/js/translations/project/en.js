var enCatalogProjectPZTranslates = {
    LIST: 'Projects',
    NEW: 'New project',
    EDIT: 'Edit project - "{{param}}"',
    FIELD: {
        TITLE: 'Title',
        ALIAS: 'Alias',
        DESCRIPTION: 'Description',
        IS_ACTIVE: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Please enter title.',
            ALIAS: 'Please enter alias.'
        }
    }
};