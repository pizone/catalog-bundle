var ruCatalogTagPZTranslates = {
    LIST: 'Хеш теги',
    NEW: 'Новый хеш тег',
    EDIT: 'Редактировать хеш тег - "{{param}}"',
    FIELD: {
        TITLE: 'Хеш тег',
        PROJECT: 'Проект',
        IS_ACTIVE: {
            LABEL: 'Активен?',
            NO: 'Нет',
            YES: 'Да',
            ANY: 'Любой'
        },
        ALL: 'Общий поиск'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Пожалуйста введите хеш тег.'
        }
    }
};