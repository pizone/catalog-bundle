var enCatalogTagPZTranslates = {
    LIST: 'Hash tags',
    NEW: 'New hash tags',
    EDIT: 'Edit hash tag - "{{param}}"',
    FIELD: {
        TITLE: 'Hash tag',
        PROJECT: 'Project',
        IS_ACTIVE: {
            LABEL: 'Is active?',
            NO: 'No',
            YES: 'Yes',
            ANY: 'Any'
        },
        ALL: 'Common search'
    },
    MESSAGE: {
        REQUIRED: {
            TITLE: 'Please enter hash tag.',
        }
    }
};