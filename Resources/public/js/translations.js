
function CatalogTranslationConfig($translateProvider) {
    $translateProvider
        .translations('en', {
            CATEGORY: enCatalogCategoryPZTranslates,
            CCONTENT: enCatalogContentPZTranslates,
            CATALOG: enCatalogPZTranslates,
            GPARAM: enCatalogGroupParametersPZTranslates,
            TAG: enCatalogTagPZTranslates,
            STOCK: enCatalogStockPZTranslates,
            CUSER: enCatalogUserPZTranslates,
            ORDER: enCatalogOrderPZTranslates,
            PROJECT: enCatalogProjectPZTranslates
        })
        .translations('ru', {
            CATEGORY: ruCatalogCategoryPZTranslates,
            CCONTENT: ruCatalogContentPZTranslates,
            CATALOG: ruCatalogPZTranslates,
            GPARAM: ruCatalogGroupParametersPZTranslates,
            TAG: ruCatalogTagPZTranslates,
            STOCK: ruCatalogStockPZTranslates,
            CUSER: ruCatalogUserPZTranslates,
            ORDER: ruCatalogOrderPZTranslates,
            PROJECT: ruCatalogProjectPZTranslates
        });
}
CatalogTranslationConfig.$inject = ['$translateProvider'];

angular
    .module('PiZone.Catalog')
    .config(CatalogTranslationConfig);
