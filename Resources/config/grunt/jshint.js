module.exports = {
    options: {
        reporter: require('jshint-stylish')
    },
    main: [
        'web/bundles/pizonecatalog/js/**/*.js'
    ]
};