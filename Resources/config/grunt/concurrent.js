module.exports = {
    // Опции
    options: {
        limit: 3
    },

    // Задачи разработки
    devFirstCatalog: [
        'jshint'
    ],
    devSecondCatalog: [
        'cssmin',
        'uglify'
    ],


    // Производственные задачи
    prodFirstCatalog: [
        'jshint'
    ],
    prodSecondCatalog: [
        'cssmin',
        'uglify'
    ]
};