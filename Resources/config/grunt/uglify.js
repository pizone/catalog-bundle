module.exports = {
    my_target: {
        files: {
            'web/assetic/js/catalog.min.js': [
                'web/bundles/pizonecatalog/js/app.js',
                'web/bundles/pizonecatalog/js/translations/**/*.js',
                'web/bundles/pizonecatalog/js/translations.js',
                // 'web/bundles/pizonecatalog/js/config.js',
                'web/bundles/pizonecatalog/js/controller/**/*.js',
                'web/bundles/pizonecatalog/js/model/**/*.js'
            ]
        }
    }
};