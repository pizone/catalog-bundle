module.exports = {
    options: {
        shorthandCompacting: false,
        roundingPrecision: -1
    },
    target: {
        files: {
            'web/assetic/css/pz.catalog.style.min.css': [
                'web/bundles/pizonecatalog/css/**/*.css'
            ]
        }
    }
}