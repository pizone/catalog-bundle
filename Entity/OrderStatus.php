<?php
namespace PiZone\CatalogBundle\Entity;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class OrderStatus extends AbstractEnumType
{
    const INIT   = 'INIT';
    const PROCESSING = 'PROCESSING';
    const PAYED  = 'PAYED';
    const DELIVERY  = 'DELIVERY';
    const CLOSED  = 'CLOSED';

    protected static $choices = [
        self::INIT    => 'ORDER.FIELD.STATUS.INIT',
        self::PROCESSING => 'ORDER.FIELD.STATUS.PROCESSING',
        self::PAYED  => 'ORDER.FIELD.STATUS.PAYED',
        self::DELIVERY  => 'ORDER.FIELD.STATUS.DELIVERY',
        self::CLOSED  => 'ORDER.FIELD.STATUS.CLOSED'
    ];
}