<?php

namespace PiZone\CatalogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="category_group_parameters")
 * @ORM\Entity(repositoryClass="PiZone\CatalogBundle\Entity\Repository\GroupParametersRepository")
 * @UniqueEntity(fields="alias", message="Sorry, this alias is already in use.", groups={"PZCatalogGroupParameters"})
 */
class GroupParameters
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank(message="Please enter title.", groups={"PZCatalogGroupParameters"})
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     * @Assert\NotBlank(message="Please enter alias.", groups={"PZCatalogGroupParameters"})
     * @Assert\Regex(
     *       pattern="/^[a-z,A-Z,\_,\-,0-9]+$/",
     *       message="Alias can contain only letters, numbers and symbols '_' , '-'.",
     *       groups={"PZCatalogGroupParameters"}
     * )
     */
    protected $alias;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_active = true;

    /**
     * @ORM\OneToMany(targetEntity="Parameter", mappedBy="group", cascade={"remove", "persist"})
     */
    protected $parameter;

    /**
     * @ORM\ManyToMany(targetEntity="PiZone\CatalogBundle\Entity\Category", mappedBy="groups")
     */
    protected $category;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->parameter = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->getTitle() .  ($this->getDescription() ? ' (' . $this->getDescription() .')' : '');
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return GroupParameters
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return GroupParameters
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Add parameter
     *
     * @param \PiZone\CatalogBundle\Entity\Parameter $parameter
     * @return GroupParameters
     */
    public function addParameter(\PiZone\CatalogBundle\Entity\Parameter $parameter)
    {
        $parameter->setGroup($this);
        $this->parameter[] = $parameter;

        return $this;
    }

    /**
     * Remove parameter
     *
     * @param \PiZone\CatalogBundle\Entity\Parameter $parameter
     */
    public function removeParameter(\PiZone\CatalogBundle\Entity\Parameter $parameter)
    {
        $this->parameter->removeElement($parameter);
    }

    /**
     * Get parameter
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getParameter()
    {
        return $this->parameter;
    }

    /**
     * Add category
     *
     * @param \PiZone\CatalogBundle\Entity\Category $category
     * @return GroupParameters
     */
    public function addCategory(\PiZone\CatalogBundle\Entity\Category $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \PiZone\CatalogBundle\Entity\Category $category
     */
    public function removeCategory(\PiZone\CatalogBundle\Entity\Category $category)
    {
        $this->category->removeElement($category);
    }

    /**
     * Get category
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return GroupParameters
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return GroupParameters
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }
}
