<?php

namespace PiZone\CatalogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="order_content")
 * @ORM\Entity(repositoryClass="PiZone\CatalogBundle\Entity\Repository\ContentOrderRepository")
 */
class ContentOrder
{

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="PiZone\CatalogBundle\Entity\Content", inversedBy="order")
     * @ORM\JoinColumn(name="content_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\NotBlank(message="Please select content", groups={"PZContentOrder"})
     */
    protected $content;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="PiZone\CatalogBundle\Entity\Order", inversedBy="content")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $order;

    /**
     * @Assert\NotBlank(message="Please enter quantity.", groups={"PZContentOrder"})
     * @Assert\Type(type="integer", message="The value {{ value }} is not a valid {{ type }}.", groups={"PZContentOrder"})
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $quantity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $comment;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     * @Assert\NotBlank(message="Please enter price.", groups={"PZContentOrder"})
     * @Assert\Type(type="real", message="The value {{ value }} is not a valid {{ type }}.", groups={"PZContentOrder"})
     */
    protected $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type(type="integer", message="The value {{ value }} is not a valid {{ type }}.", groups={"PZContentOrder"})
     */
    protected $discount;


    /**
     * Set quantity
     *
     * @param string $quantity
     *
     * @return ContentOrder
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return ContentOrder
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set content
     *
     * @param \PiZone\CatalogBundle\Entity\Content $content
     *
     * @return ContentOrder
     */
    public function setContent(\PiZone\CatalogBundle\Entity\Content $content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return \PiZone\CatalogBundle\Entity\Content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set order
     *
     * @param \PiZone\CatalogBundle\Entity\Order $order
     *
     * @return ContentOrder
     */
    public function setOrder(\PiZone\CatalogBundle\Entity\Order $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \PiZone\CatalogBundle\Entity\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return ContentOrder
     */
    public function setPrice($price)
    {
        $this->price = $this->getContent()->getSale() ? $this->getContent()->getSale()->getRetailPrice() : 0;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        if($this->price)
            return $this->price;
        else
            return $this->price = $this->getContent()->getSale() ? $this->getContent()->getSale()->getRetailPrice() : 0;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     *
     * @return ContentOrder
     */
    public function setDiscount($discount)
    {
        if($discount)
            $this->discount = $discount;
        else
            $this->price = $this->getContent()->getSale() ? $this->getContent()->getSale()->getDiscount() : 0;

        return $this;
    }

    /**
     * Get discount
     *
     * @return integer
     */
    public function getDiscount()
    {
        if($this->discount)
            return $this->discount;
        else
            return $this->price = $this->getContent()->getSale() ? $this->getContent()->getSale()->getDiscount() : 0;
    }

    public function getSellPrice(){
        if($this->getDiscount())
            return $this->getPrice() - $this->getPrice()/100*$this->getDiscount();

        return $this->getPrice();
    }
}
