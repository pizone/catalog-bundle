<?php

namespace PiZone\CatalogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="PiZone\CatalogBundle\Entity\Repository\ProjectRepository")
 * @UniqueEntity(fields="alias", message="Sorry, this alias is already in use.", groups={"PZProject"})
 * @ORM\HasLifecycleCallbacks
 */
class Project {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank(message="Please enter title.", groups={"PZProject"})
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $title;
    
    /**
     * @ORM\Column(type="string", length=100, unique=true)
     * @Assert\NotBlank(message="Please enter alias.", groups={"PZProject"})
     * @Assert\Regex( 
     *       pattern="/^[a-z,A-Z,\_,\-,0-9]+$/",
     *       message="Alias can contain only letters, numbers and symbols '_' , '-'.", 
     *       groups={"Project"}
     * )
     */
    protected $alias;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;
    /**
     * @Assert\File(maxSize="6000000")
     */
    protected $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $image_path;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $image_origin_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $title_image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $alt_image;
    
    protected $delete_image = false;
    
    protected $temp_image;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_active = true;

//    /**
//     * @ORM\ManyToMany(targetEntity="PiZone\UserBundle\Entity\User", mappedBy="projects")
//     */
//    protected $users;

    
    /**
    * @ORM\OneToMany(
     *   targetEntity="PiZone\CatalogBundle\Entity\Content",
     *   mappedBy="project", 
     *   cascade={"persist", "remove"})
    */
    protected $contents;
    
    /**
    * @ORM\OneToMany(
     *   targetEntity="\PiZone\CatalogBundle\Entity\ProjectField", 
     *   mappedBy="project", 
     *   cascade={"persist", "remove"})
    */
    protected $fields;

    /**
     * @ORM\OneToMany(targetEntity="PiZone\CatalogBundle\Entity\CatalogProject", mappedBy="project", cascade={"remove", "persist"})
     */
    protected $join_catalog;

    /**
     * @ORM\OneToMany(targetEntity="PiZone\CatalogBundle\Entity\Order", mappedBy="project", cascade={"remove", "persist"})
     */
    protected $join_order;

    /**
     * @ORM\OneToMany(targetEntity="PiZone\CatalogBundle\Entity\Tag", mappedBy="project", cascade={"persist", "remove"})
     */
    protected $tags;
    
    public function getAbsolutePath()
    {
        return null === $this->image_path ? null : $this->getUploadRootDir().'/'.$this->image_path;
    }
    
    public function getWebPath() {
        return null === $this->image_path ? '/images/default_project.jpg' : '/' . $this->getUploadDir() . '/' . $this->image_path;
    }

    protected function getUploadRootDir() {
        global $kernel;

        if ('AppCache' == get_class($kernel)) {
            $kernel = $kernel->getKernel();
        }

        return $kernel->getContainer()->getParameter('pi_zone_catalog.uploads.images.absolute_path');
    }


    protected function getUploadDir() {
        global $kernel;

        if ('AppCache' == get_class($kernel)) {
            $kernel = $kernel->getKernel();
        }

        return $kernel->getContainer()->getParameter('pi_zone_catalog.uploads.images.web_path');
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload() {
        if (null !== $this->getImage()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->image_path = $filename . '.' . $this->getImage()->guessExtension();
            $this->image_origin_name = $this->getImage()->getClientOriginalName();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
        if (null === $this->image) {
            return;
        }

        if (null !== $this->getImage()) {
            $this->getImage()->move($this->getUploadRootDir(), $this->image_path);
            if (isset($this->temp_image)) {
                // delete the old image
                if(file_exists($this->getUploadRootDir() . '/' . $this->temp_image))
                    unlink($this->getUploadRootDir() . '/' . $this->temp_image);
                // clear the temp image path
                $this->temp_image = null;
            }
            $this->image = null;
        }
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
        if ($image = $this->getAbsolutePath()) {
            if(file_exists($image))
                unlink($image);
        }
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Project
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return Project
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    
        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return Project
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;
    
        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->is_active;
    }
    
    public function __toString() {
        return $this->title;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contents = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fields = new \Doctrine\Common\Collections\ArrayCollection();
    }

    
    /**
     * Set image_path
     *
     * @param string $image_path
     * @return Project
     */
    public function setImagePath($image_path)
    {
        $this->image_path = $image_path;
    
        return $this;
    }

    /**
     * Get image_path
     *
     * @return string 
     */
    public function getImagePath()
    {
        return $this->image_path;
    }
    
    /**
     * Sets file.
     *
     * @param UploadedFile $image
     */
    public function setImage(UploadedFile $image = null) {
        $this->image = $image;
        // check if we have an old image path
        if (isset($this->image_path)) {
            // store the old name to delete after the update
            $this->temp_image = $this->image_path;
            $this->image_path = null;
        } else {
            $this->image_path = 'initial';
        }
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage() {
        return $this->image;
    }
    
    public function getDeleteImage(){
        return $this->delete_image;
    }

    public function setDeleteImage($delete) {
        if ($delete && null === $this->getImage()) {
            if ($file = $this->getAbsolutePath()) {
                $this->setImagePath(null);
                if (file_exists($file))
                    unlink($file);
            }
        }
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add contents
     *
     * @param \PiZone\CatalogBundle\Entity\Content $contents
     * @return Project
     */
    public function addContent(\PiZone\CatalogBundle\Entity\Content $contents)
    {
        $this->contents[] = $contents;
    
        return $this;
    }

    /**
     * Remove contents
     *
     * @param \PiZone\CatalogBundle\Entity\Content $contents
     */
    public function removeContent(\PiZone\CatalogBundle\Entity\Content $contents)
    {
        $this->contents->removeElement($contents);
    }

    /**
     * Get contents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContents()
    {
        return $this->contents;
    }

    /**
     * Add fields
     *
     * @param \PiZone\CatalogBundle\Entity\ProjectField $fields
     * @return Project
     */
    public function addField(\PiZone\CatalogBundle\Entity\ProjectField $fields)
    {
        $this->fields[] = $fields;
        $fields->setProject($this);
        return $this;
    }

    /**
     * Remove fields
     *
     * @param \PiZone\CatalogBundle\Entity\ProjectField $fields
     */
    public function removeField(\PiZone\CatalogBundle\Entity\ProjectField $fields)
    {
        $this->fields->removeElement($fields);
    }

    /**
     * Get fields
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Add joinCatalog
     *
     * @param \PiZone\CatalogBundle\Entity\CatalogProject $joinCatalog
     *
     * @return Project
     */
    public function addJoinCatalog(\PiZone\CatalogBundle\Entity\CatalogProject $joinCatalog)
    {
        $this->join_catalog[] = $joinCatalog;

        return $this;
    }

    /**
     * Remove joinCatalog
     *
     * @param \PiZone\CatalogBundle\Entity\CatalogProject $joinCatalog
     */
    public function removeJoinCatalog(\PiZone\CatalogBundle\Entity\CatalogProject $joinCatalog)
    {
        $this->join_catalog->removeElement($joinCatalog);
    }

    /**
     * Get joinCatalog
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJoinCatalog()
    {
        return $this->join_catalog;
    }

    /**
     * Add tag
     *
     * @param \PiZone\CatalogBundle\Entity\Tag $tag
     *
     * @return Project
     */
    public function addTag(\PiZone\CatalogBundle\Entity\Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \PiZone\CatalogBundle\Entity\Tag $tag
     */
    public function removeTag(\PiZone\CatalogBundle\Entity\Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Add joinOrder
     *
     * @param \PiZone\CatalogBundle\Entity\Order $joinOrder
     *
     * @return Project
     */
    public function addJoinOrder(\PiZone\CatalogBundle\Entity\Order $joinOrder)
    {
        $this->join_order[] = $joinOrder;

        return $this;
    }

    /**
     * Remove joinOrder
     *
     * @param \PiZone\CatalogBundle\Entity\Order $joinOrder
     */
    public function removeJoinOrder(\PiZone\CatalogBundle\Entity\Order $joinOrder)
    {
        $this->join_order->removeElement($joinOrder);
    }

    /**
     * Get joinOrder
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJoinOrder()
    {
        return $this->join_order;
    }
}
