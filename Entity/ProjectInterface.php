<?php

namespace PiZone\CatalogBundle\Entity;


interface ProjectInterface{

    function getId();

    function setTitle($title);

    function getTitle();

    function addUser(\PiZone\UserBundle\Entity\User $users);

    function removeUser(\PiZone\UserBundle\Entity\User $users);

    function getUsers();
}