<?php

namespace PiZone\CatalogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\ExecutionContextInterface;
use PiZone\CatalogBundle\Entity\OrderStatus;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Table(name="wf_order")
 * @ORM\Entity(repositoryClass="PiZone\CatalogBundle\Entity\Repository\OrderRepository")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="PiZone\CatalogBundle\Entity\User", inversedBy="order")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\NotBlank(message="Please select user.", groups={"PZOrder"})
     */
    protected $user;

    /**
     * @ORM\Column(type="integer")
     */
    protected $project_id;

    /**
     * @ORM\ManyToOne(targetEntity="PiZone\CatalogBundle\Entity\Project", inversedBy="join_order")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $project;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $discount;

    /**
     * @ORM\Column(type="float")
     */
    protected $itog;

    /**
     * @ORM\OneToMany(targetEntity="PiZone\CatalogBundle\Entity\ContentOrder", mappedBy="order", cascade={"remove", "persist"})
     * @Assert\NotBlank(message="Please select content.", groups={"PZOrder"})
     */
    protected $content;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_active = true;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_confirm = false;

    /**
     * @ORM\Column(name="status", type="OrderStatus", nullable=false, options={"default"="INIT"})
     * @DoctrineAssert\Enum(entity="PiZone\CatalogBundle\Entity\OrderStatus")
     * @Assert\NotBlank(message="Please enter status.", groups={"PZOrder"})
     */
    protected $status;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    protected $comment;

    /**
     * @ORM\ManyToOne(targetEntity="PiZone\CatalogBundle\Entity\Stock", inversedBy="orders")
     * @ORM\JoinColumn(name="stock_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $stock;

    /**
     * Random string sent to the user email address in order to verify it.
     *
     * @var string
     *
     * @ORM\Column(name="confirmation_token", type="string", length=180, unique=true, nullable=true)
     */
    protected $confirmationToken;

    /**
     * @var datetime $created_at
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updated_at;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->content = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getFullName(){
        if($this->getUser())
            return $this->getUser()->getFullName();
        return '';
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set project_id
     *
     * @param integer $projectId
     * @return Order
     */
    public function setProjectId($projectId)
    {
        $this->project_id = $projectId;
    
        return $this;
    }

    /**
     * Get project_id
     *
     * @return integer 
     */
    public function getProjectId()
    {
        return $this->project_id;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     * @return Order
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    
        return $this;
    }

    /**
     * Get discount
     *
     * @return integer 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set itog
     *
     * @param float $itog
     * @return Order
     */
    public function setItog($itog)
    {
        $this->itog = $itog;
    
        return $this;
    }

    /**
     * Get itog
     *
     * @return float 
     */
    public function getItog()
    {
        return $this->itog;
    }

    /**
     * Set is_active
     *
     * @param boolean $isActive
     * @return Order
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;
    
        return $this;
    }

    /**
     * Get is_active
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Order
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $created_at
     * @return Order
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    
        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updated_at
     * @return Order
     */
    public function setUpdatedAt($updated_at)
    {
        $this->updated_at = $updated_at;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set project
     *
     * @param \PiZone\CatalogBundle\Entity\Project $project
     * @return Order
     */
    public function setProject(\PiZone\CatalogBundle\Entity\Project $project = null)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \PiZone\CatalogBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set stock
     *
     * @param \PiZone\CatalogBundle\Entity\Stock $stock
     * @return Order
     */
    public function setStock(\PiZone\CatalogBundle\Entity\Stock $stock = null)
    {
        $this->stock = $stock;
    
        return $this;
    }

    /**
     * Get stock
     *
     * @return \PiZone\CatalogBundle\Entity\Stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set user
     *
     * @param \PiZone\CatalogBundle\Entity\User $user
     * @return Order
     */
    public function setUser(\PiZone\CatalogBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \PiZone\CatalogBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add content
     *
     * @param \PiZone\CatalogBundle\Entity\ContentOrder $content
     *
     * @return Order
     */
    public function addContent(\PiZone\CatalogBundle\Entity\ContentOrder $content)
    {
        $content->setOrder($this);
        $this->content[] = $content;

        return $this;
    }

    /**
     * Remove content
     *
     * @param \PiZone\CatalogBundle\Entity\ContentOrder $content
     */
    public function removeContent(\PiZone\CatalogBundle\Entity\ContentOrder $content)
    {
        $this->content->removeElement($content);
    }

    /**
     * Get content
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Order
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    public function getItogSumm(){
        $itog = 0;
        foreach ($this->getContent() as $one){
            $itog = $itog + ($one->getSellPrice() * $one->getQuantity());
        }
        if($this->getDiscount()){
            return $itog - ($itog/100 * $this->getDiscount());
        }

        return $itog;
    }

    public function getItogDiscount(){
        $itog = 0;
        foreach ($this->getContent() as $one){
            $itog = $itog + ($one->getPrice() * $one->getQuantity());
        }

        return $itog - $this->getItogSumm();
    }

    /**
     * Set isConfirm
     *
     * @param boolean $isConfirm
     *
     * @return Order
     */
    public function setIsConfirm($isConfirm)
    {
        $this->is_confirm = $isConfirm;

        return $this;
    }

    /**
     * Get isConfirm
     *
     * @return boolean
     */
    public function getIsConfirm()
    {
        return $this->is_confirm;
    }

    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }
}
