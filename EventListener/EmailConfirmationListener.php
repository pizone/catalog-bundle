<?php

namespace PiZone\CatalogBundle\EventListener;

use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use PiZone\CatalogBundle\Entity\Order;
use PiZone\CatalogBundle\Event\PiZoneOrderEvent;
use PiZone\CatalogBundle\Event\PiZoneUserEvents;
use PiZone\CatalogBundle\Mailer\MailerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class EmailConfirmationListener implements EventSubscriberInterface
{
    private $mailer;
    private $tokenGenerator;
    private $router;
    private $session;

    /**
     * EmailConfirmationListener constructor.
     *
     * @param MailerInterface         $mailer
     * @param TokenGeneratorInterface $tokenGenerator
     * @param UrlGeneratorInterface   $router
     * @param SessionInterface        $session
     */
    public function __construct(MailerInterface $mailer, TokenGeneratorInterface $tokenGenerator, UrlGeneratorInterface $router, SessionInterface $session)
    {
        $this->mailer = $mailer;
        $this->tokenGenerator = $tokenGenerator;
        $this->router = $router;
        $this->session = $session;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            PiZoneUserEvents::ORDER_USER_SUCCESS => 'onOrderUserSuccess',
            PiZoneUserEvents::ORDER_SUCCESS => 'onOrderSuccess',
        );
    }

    /**
     * @param OrderEvent $event
     */
    public function onOrderSuccess(PiZoneOrderEvent $event)
    {
        /** @var $user \FOS\UserBundle\Model\UserInterface */
        $user = $event->getUser();
        $order = $event->getOrder();

        $user->setEnabled(false);
        $order->setIsConfirm(false);

        if (null === $order->getConfirmationToken()) {
            $order->setConfirmationToken($this->tokenGenerator->generateToken());
        }

        $this->mailer->sendConfirmationOrderEmailMessage($user, $order);

        $this->session->set('fos_user_send_confirmation_email/email', $user->getEmail());

        $url = $this->router->generate('esser_frontend_catalog_order_check_email');
        $event->setResponse(new RedirectResponse($url));
    }

    /**
     * @param OrderEvent $event
     */
    public function onOrderUserSuccess(PiZoneOrderEvent $event)
    {
        /** @var $user \FOS\UserBundle\Model\UserInterface */
        $user = $event->getUser();
        $order = $event->getOrder();

        $order->setIsConfirm(false);

        if (null === $order->getConfirmationToken()) {
            $order->setConfirmationToken($this->tokenGenerator->generateToken());
        }

        $this->mailer->sendConfirmationOrderEmailMessage($user, $order);

        $this->session->set('fos_user_send_confirmation_email/email', $user->getEmail());

        $url = $this->router->generate('esser_frontend_catalog_order_check_email');
        $event->setResponse(new RedirectResponse($url));
    }
}
