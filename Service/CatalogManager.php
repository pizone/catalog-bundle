<?php

namespace PiZone\CatalogBundle\Service;

class CatalogManager{
    /**
     * @var ContainerInterface
     */
    protected $container;
    protected $doctrine;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct($container) {
        $this->container = $container;
        $this->doctrine = $container->get('doctrine')->getManager('catalog');
    }

    public function getTopCategoriesForProject(){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Catalog')->findTopCategoriesForProject();
    }

    public function getTopCategoriesByAlias($alias){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Catalog')->GetTopCategoriesByAlias($alias);
    }

    public function getTreeCategoriesByAlias($alias, $filter = array()){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Catalog')->GetTreeCategoriesByAlias($alias, $filter);
    }

    public function getTreeCatalog(){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Catalog')->GetTreeCatalog();
    }
//
//    public function getContentByAlias($alias){
//        return $this->doctrine->getRepository('PiZoneCatalogBundle:Catalog')->GetContentByAlias($alias);
//    }

    public function getPathForNode($node){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Catalog')->GetPathForNode($node);
    }

    public function getContentByHahTag($hashTag){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Catalog')->GetContentByHahTag($hashTag);
    }

    public function getContentByCategory($category){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Catalog')->GetContentByCategory($category);
    }

    public function Search($query){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Catalog')->Search($query);
    }
}