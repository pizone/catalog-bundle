<?php

namespace PiZone\CatalogBundle\Service;

class OrderManager{
    /**
     * @var ContainerInterface
     */
    protected $container;
    protected $doctrine;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct($container) {
        $this->container = $container;
        $this->doctrine = $container->get('doctrine')->getManager('catalog');
    }

    public function findOrderByConfirmationToken($token){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Order')->findOrderByConfirmationToken($token);
    }

    public function findById($id){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Order')->findById($id);
    }
}