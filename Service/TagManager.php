<?php

namespace PiZone\CatalogBundle\Service;

class TagManager{
    /**
     * @var ContainerInterface
     */
    protected $container;
    protected $doctrine;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct($container) {
        $this->container = $container;
        $this->doctrine = $container->get('doctrine')->getManager('catalog');
    }

    public function GetList(){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Tag')->getTagList();
    }
}