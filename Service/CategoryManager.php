<?php

namespace PiZone\CatalogBundle\Service;

class CategoryManager{
    /**
     * @var ContainerInterface
     */
    protected $container;
    protected $doctrine;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct($container) {
        $this->container = $container;
        $this->doctrine = $container->get('doctrine')->getManager('catalog');
    }

    public function getTreeCategoriesByAlias($alias = null){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Category')->GetTreeCategoriesByAlias($alias);
    }

    public function getPathForNode($node){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Category')->getPathForNode($node);
    }

    public function findByAlias($alias){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Category')->findByAlias($alias);
    }

    public function getContentByAliasQuery($alias){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Category')->getContentByAliasQuery($alias);
    }

    public function getContentByAlias($alias){
        return $this->getContentByAliasQuery($alias)->getResult();
    }

    public function getFilteringParameters($parent){
        $collection = $this->doctrine->getRepository('PiZoneCatalogBundle:Category')->getCategoryWithChild($parent);

        $filters = array();
        foreach($collection as $category){
            foreach($category->getGroups() as $group){
                if($group->getIsActive()) {
                    $parameters = array();

                    foreach ($group->getParameter() as $one) {
                        if ($one->getIsActive()) {

                            $content = $one->getContent()->filter( function($entry) use ($category) {
                                return $entry->getCategory() == $category;
                            });
                            $parameters[] = array(
                                'id' => $one->getId(),
                                'alias' => $one->getAlias(),
                                'title' => $one->getTitle(),
                                'count' => $content->count()
                            );
                        }
                    }

                    uasort($parameters, array('self', 'cmp'));

                    $filters[$group->getId()] = array(
                        'id' => $group->getId(),
                        'alias' => $group->getAlias(),
                        'title' => $group->getTitle(),
                        'parameters' => $parameters
                    );
                }
            }
        }


        return $filters;
    }

    private function cmp($a, $b) {
        if ($a['count'] == $b['count']) {
            return 0;
        }
        return ($a['count'] > $b['count']) ? -1 : 1;
    }

    public function getChildContentByAliasQuery($alias, $orderBy = null, $filters = null){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Category')->getChildContentByAliasQuery($alias, $orderBy, $filters);
    }

    public function getChildContentByAlias($alias){
        return $this->getChildContentByAliasQuery($alias)->getResult();
    }
}