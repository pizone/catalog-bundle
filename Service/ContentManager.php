<?php

namespace PiZone\CatalogBundle\Service;

class ContentManager{
    /**
     * @var ContainerInterface
     */
    protected $container;
    protected $doctrine;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct($container) {
        $this->container = $container;
        $this->doctrine = $container->get('doctrine')->getManager('catalog');
    }

    public function GetContentByHahTag($hashTag){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Content')->GetContentByHahTag($hashTag);
    }

    public function GetContentByCategory($category){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Content')->GetContentByCategory($category);
    }

    public function GetContentByIds($ids){
        return $this->doctrine->getRepository('PiZoneCatalogBundle:Content')->getContentsByIds($ids);
    }
}