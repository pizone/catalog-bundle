<?php

namespace PiZone\CatalogBundle\Listener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\DependencyInjection\ContainerAware;

class CatalogListener {

    private $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function onKernelController(FilterControllerEvent $event) {
        if (!$this->container->get('session')->get('project_id')){
            $projectId = trim($this->container->getParameter('project_id'));
            $projects = explode(',', $projectId);

            if($projectId && count($projects) > 0) {
                $this->container->get('session')->set('project_id', trim($projects[0]));
            }
            else{
                $projects = $this->container->get('doctrine')->getManager('catalog')->getRepository('PiZoneCatalogBundle:Project')->findBy(array('is_active' => true));
                if($projects)
                    $this->container->get('session')->set('project_id', $projects[0]->getId());
            }
        }
    }

}
