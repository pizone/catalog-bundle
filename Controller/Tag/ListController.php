<?php

namespace PiZone\CatalogBundle\Controller\Tag;

use FOS\RestBundle\View\View;
use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 *
 */
class ListController  extends AListController implements IListController
{
    protected $userId = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\CatalogBundle\TagList';
        $this->routeList = array(
            'list' => array(
                'name' => 'catalog_tag',
                'parameters' => array()
            ),
            'delete' => 'catalog_tag_delete',
            'batch' => array(
                'delete' => 'catalog_tag_batch_delete',
                'active' => 'catalog_tag_batch_active'
            )
        );
        $this->model = 'PiZone\CatalogBundle\Entity\Tag';
        $this->repository = 'PiZone\CatalogBundle\Entity\Tag';
        $this->filtersForm = 'PiZone\CatalogBundle\Form\TagFilterType';
        $this->manager = 'catalog';
    }
    
    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $this->fieldList[] = array(
                'id' => $one->getId(),
                'title' => $one->getTitle(),
                'is_active' => $one->getIsActive(),
                '_token' => $token->getValue()
            );
        }

        return $this->fieldList;
    }

    protected function buildQuery()
    {
        return $this->getDoctrine()
            ->getManager($this->manager)
            ->getRepository($this->repository)
            ->createQueryForProject();
    }

    protected function setFilters($filters)
    {
        $this->get('session')->set($this->prefixSession . '\Filters', $filters);
        $this->get('session')->set('project_id', $filters['project']->getId());
    }

    protected function getFilterForm()
    {
        $filters = $this->getFilters();
        $em = $this->getDoctrine()->getManager($this->manager);

        $project_id = $this->container->get('session')->get('project_id');
        $project = $em->getRepository('PiZoneCatalogBundle:Project')->find($project_id);
        $filters['project'] = $project;
        $em->persist($project);

        return $this->createForm($this->getFiltersType(), $filters);
    }

    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);

        if (isset($filterObject['all']) && null !== $filterObject['all']) {
            $queryFilter->addOrStringFilter('title', $filterObject['all']);
        }
        if (isset($filterObject['title']) && null !== $filterObject['title']) {
            $queryFilter->addStringFilter('title', $filterObject['title']);
        }
        if (isset($filterObject['is_active']) && null !== $filterObject['is_active']) {
            $queryFilter->addBooleanFilter('is_active', $filterObject['is_active']);
        }

    }

    public function findAction($search){
        $em = $this->getDoctrine()->getManager($this->manager);
        $users = $em->getRepository($this->repository)->findTagLike($search);

        $result = array();
        foreach($users as $user){
            $result[] = array(
                'id' => $user->getId(),
                'name' => $user->getTitle()
            );
        }

        $view = $this->view(json_encode(array('data' => $result)))
            ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');

        return $this->handleView($view);
    }
}