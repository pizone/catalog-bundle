<?php

namespace PiZone\CatalogBundle\Controller\Tag;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * Category controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Tag';
        $this->repository = 'PiZone\CatalogBundle\Entity\Tag';
        $this->route['delete'] = 'catalog_tag_delete';
        $this->route['list']['name'] = 'catalog_tag';
        $this->manager = 'catalog';
    }
}