<?php

namespace PiZone\CatalogBundle\Controller\Tag;

use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;

/**
 * Category controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Tag';
        $this->form = 'PiZone\CatalogBundle\Form\TagType';
        $this->routeList['update'] = 'catalog_tag_update';
        $this->routeList['delete'] = 'catalog_tag_delete';
        $this->manager = 'catalog';
    }
}