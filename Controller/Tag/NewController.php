<?php

namespace PiZone\CatalogBundle\Controller\Tag;

use PiZone\AdminBundle\Controller\ANewController;
use PiZone\AdminBundle\Controller\INewController;
use PiZone\CatalogBundle\Entity\Tag;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

/**
 * Category controller.
 *
 */
class NewController extends ANewController implements INewController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Tag';
        $this->form = 'PiZone\CatalogBundle\Form\TagType';
        $this->route['create'] = 'catalog_tag_create';
        $this->manager = 'catalog';
    }

    protected function getObject(){
        $tag = new Tag();
        $project = $this->container->get('session')->get('project_id');
        $tag->setProject($project);

        return $tag;
    }
}
