<?php

namespace PiZone\CatalogBundle\Controller\Order;

use FOS\RestBundle\View\View;
use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use PiZone\CatalogBundle\Entity\OrderStatus;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 *
 */
class ListController  extends AListController implements IListController
{
    protected $userId = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\CatalogBundle\OrderList';
        $this->routeList = array(
            'list' => array(
                'name' => 'catalog_order',
                'parameters' => array()
            ),
            'delete' => 'catalog_order_delete',
            'batch' => array(
                'delete' => 'catalog_order_batch_delete',
                'active' => 'catalog_order_batch_active'
            )
        );
        $this->model = 'PiZone\CatalogBundle\Entity\Order';
        $this->repository = 'PiZone\CatalogBundle\Entity\Order';
        $this->filtersForm = 'PiZone\CatalogBundle\Form\OrderFilterType';
        $this->manager = 'catalog';
    }
    
    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $content = $this->getContentData($one->getContent());
            $user = $this->getUserData($one->getUser());
            $stock = $this->getStockData($one->getStock());
            $this->fieldList[] = array(
                'id' => $one->getId(),
                'content' => $content,
                'user' => $user,
                'itog' => $one->getItog(),
                'discount' => $one->getDiscount(),
                'status' => OrderStatus::getReadableValue($one->getStatus()),
                'stock' => $stock,
                'created_at' => $one->getCreatedAt()->format('d.m.Y H:i'),
                'updated_at' => $one->getUpdatedAt()->format('d.m.Y H:i'),
                'is_active' => $one->getIsActive(),
                'is_confirm' => $one->getIsConfirm(),
                '_token' => $token->getValue()
            );
        }

        return $this->fieldList;
    }

    private function getContentData($list){
        $result= array();
        foreach($list as $one){
            $content = $one->getContent();
            $values = array(
                'id' => $content->getId(),
                'title' => $content->getTitle(),
                'alias' => $content->getAlias(),
                'article' => $content->getArticle(),
                'anons' => $content->getAnons(),
                'image' => $content->getImageWebPath(),
                'is_active' => $content->getIsActive(),
                'category' => array(),
            );
            $values['quantity'] = $one->getQuantity();
            $values['price'] = $one->getPrice();
            $values['discount'] = $one->getDiscount();
            $values['comment'] = $one->getComment();

            if($content->getCategory()){
                $values['category'] = array(
                    'id' => $content->getCategory()->getId(),
                    'title' => $content->getCategory()->getTitle()
                );
            }

            $params = array();
            foreach($content->getParameters() as $parameter){
                $params[] = array(
                    'group' => $parameter->getGroup()->getTitle(),
                    'value' => $parameter->getTitle()
                );
            }
            $values['parameters'] = $params;
            $catalogs = array();
            foreach($content->getCatalogs() as $catalog){
                $catalogs[] = array(
                    'id' => $catalog->getId(),
                    'title' => $catalog->getTitle()
                );
            }
            $values['catalogs'] = $catalogs;

            $result[] = $values;
        }

        return $result;
    }

    private function getUserData($user){
        if($user){
            return array(
                'id' => $user->getId(),
                'username' => $user->getUsername(),
                'full_name' => $user->getFullName(),
                'company' => $user->getCompany(),
                'position_held' => $user->getPositionHeld(),
                'address' => $user->getAddress(),
                'email' => $user->getEmail(),
                'phone' => $user->getPhone(),
                'messenger' => $user->getMessenger(),
                'enabled' => $user->isEnabled(),
                'registration_at' => $user->getRegistrationAt() ? $user->getRegistrationAt()->format('d.m.Y H:i'): '',
                'last_login' => $user->getLastLogin() ? $user->getLastLogin()->format('d.m.Y H:i'): '',
                'roles' => $user->getRoles(),
            );
        }
        return null;
    }

    private function getStockData($stock){
        if($stock)
            return array(
                'id' => $stock->getId(),
                'title' => $stock->getTitle(),
                'address' => $stock->getAddress(),
                'is_active' => $stock->getIsActive()
            );
        return null;
    }

    protected function getFilterForm()
    {
        $filters = $this->getFilters();
        $em = $this->getDoctrine()->getManager($this->manager);

        if(isset($filters['project']) && $filters['project']) {
            $em->persist($filters['project']);
        }

        return $this->createForm($this->getFiltersType(), $filters);
    }

    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);


        if (isset($filterObject['all']) && null !== $filterObject['all']) {

        }
        if (isset($filterObject['project']) && null !== $filterObject['project']) {
            $queryFilter->addDefaultFilter('project', $filterObject['project']);
        }
        if (isset($filterObject['is_active']) && null !== $filterObject['is_active']) {
            $queryFilter->addBooleanFilter('is_active', $filterObject['is_active']);
        }
        if (isset($filterObject['is_confirm']) && null !== $filterObject['is_confirm']) {
            $queryFilter->addBooleanFilter('is_confirm', $filterObject['is_confirm']);
        }
    }
}