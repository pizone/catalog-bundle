<?php

namespace PiZone\CatalogBundle\Controller\Order;

use PiZone\AdminBundle\Controller\ANewController;
use PiZone\AdminBundle\Controller\INewController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

/**
 * Category controller.
 *
 */
class NewController extends ANewController implements INewController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Order';
        $this->form = 'PiZone\CatalogBundle\Form\OrderType';
        $this->route['create'] = 'catalog_order_create';
        $this->manager = 'catalog';
    }

    protected function save($entity, $form){
        $em = $this->getDoctrine()->getManager($this->manager);
        $project = $em->getRepository('PiZoneCatalogBundle:Project')->find($this->getParameter('project_id'));
        $entity->setProject($project);

        foreach($entity->getContent() as $one){
            $one->setOrder($entity);
        }

        $em->persist($entity);
        $em->flush();
    }
}
