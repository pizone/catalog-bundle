<?php

namespace PiZone\CatalogBundle\Controller\Order;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * Category controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Order';
        $this->repository = 'PiZone\CatalogBundle\Entity\Order';
        $this->route['delete'] = 'catalog_order_delete';
        $this->route['list']['name'] = 'catalog_order';
        $this->manager = 'catalog';
    }
}