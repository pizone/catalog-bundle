<?php

namespace PiZone\CatalogBundle\Controller\Order;

use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;

/**
 * Category controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Order';
        $this->form = 'PiZone\CatalogBundle\Form\OrderType';
        $this->routeList['update'] = 'catalog_order_update';
        $this->routeList['delete'] = 'catalog_order_delete';
        $this->manager = 'catalog';
    }

    protected function save($em, $entity, $form){
        foreach($entity->getContent() as $one){
            $one->setOrder($entity);
        }

        $em->persist($entity);
        $em->flush();
    }
}