<?php

namespace PiZone\CatalogBundle\Controller\Stock;

use PiZone\AdminBundle\Controller\ANewController;
use PiZone\AdminBundle\Controller\INewController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

/**
 * Category controller.
 *
 */
class NewController extends ANewController implements INewController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Stock';
        $this->form = 'PiZone\CatalogBundle\Form\StockType';
        $this->route['create'] = 'catalog_stock_create';
        $this->manager = 'catalog';
    }
}
