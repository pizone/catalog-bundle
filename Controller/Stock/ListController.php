<?php

namespace PiZone\CatalogBundle\Controller\Stock;

use FOS\RestBundle\View\View;
use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 *
 */
class ListController  extends AListController implements IListController
{
    protected $userId = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\CatalogBundle\StockList';
        $this->routeList = array(
            'list' => array(
                'name' => 'catalog_stock',
                'parameters' => array()
            ),
            'delete' => 'catalog_stock_delete',
            'batch' => array(
                'delete' => 'catalog_stock_batch_delete',
                'active' => 'catalog_stock_batch_active'
            )
        );
        $this->model = 'PiZone\CatalogBundle\Entity\Stock';
        $this->repository = 'PiZone\CatalogBundle\Entity\Stock';
        $this->filtersForm = 'PiZone\CatalogBundle\Form\StockFilterType';
        $this->manager = 'catalog';
    }
    
    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $this->fieldList[] = array(
                'id' => $one->getId(),
                'title' => $one->getTitle(),
                'is_active' => $one->getIsActive(),
                'address' => $one->getAddress(),
                'lat' => $one->getLat(),
                'lng' => $one->getLng(),
                '_token' => $token->getValue()
            );
        }

        return $this->fieldList;
    }

    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);


        if (isset($filterObject['all']) && null !== $filterObject['all']) {
            $queryFilter->addOrStringFilter('title', $filterObject['all']);
            $queryFilter->addOrStringFilter('address', $filterObject['all']);
            $queryFilter->addOrStringFilter('lat', $filterObject['all']);
            $queryFilter->addOrStringFilter('lng', $filterObject['all']);
        }
        if (isset($filterObject['title']) && null !== $filterObject['title']) {
            $queryFilter->addStringFilter('title', $filterObject['title']);
        }
        if (isset($filterObject['address']) && null !== $filterObject['address']) {
            $queryFilter->addStringFilter('address', $filterObject['address']);
        }
        if (isset($filterObject['lat']) && null !== $filterObject['lat']) {
            $queryFilter->addStringFilter('lat', $filterObject['lat']);
        }
        if (isset($filterObject['lng']) && null !== $filterObject['lng']) {
            $queryFilter->addStringFilter('lng', $filterObject['lng']);
        }
        if (isset($filterObject['is_active']) && null !== $filterObject['is_active']) {
            $queryFilter->addBooleanFilter('is_active', $filterObject['is_active']);
        }
    }
}