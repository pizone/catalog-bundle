<?php

namespace PiZone\CatalogBundle\Controller\Stock;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * Category controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Stock';
        $this->repository = 'PiZone\CatalogBundle\Entity\Stock';
        $this->route['delete'] = 'catalog_stock_delete';
        $this->route['list']['name'] = 'catalog_stock';
        $this->manager = 'catalog';
    }
}