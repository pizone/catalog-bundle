<?php

namespace PiZone\CatalogBundle\Controller\Stock;

use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;

/**
 * Category controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Stock';
        $this->form = 'PiZone\CatalogBundle\Form\StockType';
        $this->routeList['update'] = 'catalog_stock_update';
        $this->routeList['delete'] = 'catalog_stock_delete';
        $this->manager = 'catalog';
    }
}