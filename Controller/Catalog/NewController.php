<?php

namespace PiZone\CatalogBundle\Controller\Catalog;

use PiZone\AdminBundle\Controller\ANewController;
use PiZone\AdminBundle\Controller\INewController;
use PiZone\CatalogBundle\Entity\CatalogProject;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

/**
 * Catalog controller.
 *
 */
class NewController extends ANewController implements INewController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Catalog';
        $this->form = 'PiZone\CatalogBundle\Form\CatalogType';
        $this->route['create'] = 'catalog_create';
        $this->manager = 'catalog';
    }

    public function postSave($entity){
        $projectId = $this->container->get('session')->get('project_id');
        if ($entity->isRoot()) {
            $em = $this->getDoctrine()->getManager($this->manager);
            $catalogProject = new CatalogProject($entity, $em->getRepository('CatalogProjectBundle:Project')->find($projectId));
            $em->persist($catalogProject);

            $em->flush();
        }
    }
}
