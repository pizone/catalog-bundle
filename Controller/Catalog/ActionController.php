<?php

namespace PiZone\CatalogBundle\Controller\Catalog;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * Catalog controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Catalog';
        $this->repository = 'PiZone\CatalogBundle\Entity\Catalog';
        $this->route['delete'] = 'catalog_delete';
        $this->route['list']['name'] = 'catalog';
        $this->manager = 'catalog';
    }

    protected function executeObjectDelete($entity)
    {
        $em = $this->getDoctrine()->getManager($this->manager);
        $em->removeFromTree($entity);
        $em->flush();
        $em->clear();
    }
}