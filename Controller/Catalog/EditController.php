<?php

namespace PiZone\CatalogBundle\Controller\Catalog;

use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;

/**
 * Catalog controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Catalog';
        $this->form = 'PiZone\CatalogBundle\Form\CatalogType';
        $this->routeList['update'] = 'catalog_update';
        $this->routeList['delete'] = 'catalog_delete';
        $this->manager = 'catalog';
    }
}