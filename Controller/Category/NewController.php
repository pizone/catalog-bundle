<?php

namespace PiZone\CatalogBundle\Controller\Category;

use PiZone\AdminBundle\Controller\ANewController;
use PiZone\AdminBundle\Controller\INewController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

/**
 * Category controller.
 *
 */
class NewController extends ANewController implements INewController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Category';
        $this->form = 'PiZone\CatalogBundle\Form\CategoryType';
        $this->route['create'] = 'catalog_category_create';
        $this->manager = 'catalog';
    }
}
