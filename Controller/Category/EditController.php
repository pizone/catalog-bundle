<?php

namespace PiZone\CatalogBundle\Controller\Category;

use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;

/**
 * Category controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Category';
        $this->form = 'PiZone\CatalogBundle\Form\CategoryType';
        $this->routeList['update'] = 'catalog_category_update';
        $this->routeList['delete'] = 'catalog_category_delete';
        $this->manager = 'catalog';
    }

    public function prepareView($id, $editForm){
        $data = $this->get('pz_form')->formDataToArray($editForm->createView());

        if($editForm->getData()->getImageWebPath()){
            $info = pathinfo($editForm->getData()->getImageAbsolutePath());

            $data['image']['info'] = array(
                'path' => $editForm->getData()->getImageWebPath(),
                'size' => file_exists($editForm->getData()->getImageAbsolutePath()) ? filesize($editForm->getData()->getImageAbsolutePath()) : 0,
                'name' => $info ? $info['basename'] : '',
                'mimetype' => $info ? $info['extension'] : ''
            );
        }

        return array(
            'action' => $this->generateUrl($this->routeList['update'], array('id' => $id)),
            'fields' => $data,
            '_delete_token' => $this->getDeleteFormToken($id)
        );
    }
}