<?php

namespace PiZone\CatalogBundle\Controller\Category;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * Category controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Category';
        $this->repository = 'PiZone\CatalogBundle\Entity\Category';
        $this->route['delete'] = 'catalog_category_delete';
        $this->route['list']['name'] = 'catalog_category';
        $this->manager = 'catalog';
    }

    protected function executeObjectDelete($entity)
    {
        $em = $this->getDoctrine()->getManager($this->manager);
        $em->removeFromTree($entity);
        $em->flush();
        $em->clear();
    }
}