<?php

namespace PiZone\CatalogBundle\Controller\Category;

use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 *
 */
class ListController  extends AListController implements IListController
{
    protected $userId = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\CatalogBundle\CategoryList';
        $this->routeList = array(
            'list' => array(
                'name' => 'catalog_category',
                'parameters' => array()
            ),
            'delete' => 'catalog_category_delete',
            'batch' => array(
                'delete' => 'catalog_category_batch_delete',
                'active' => 'catalog_category_batch_active'
            )
        );
        $this->model = 'PiZone\CatalogBundle\Entity\Category';
        $this->repository = 'PiZone\CatalogBundle\Entity\Category';
        $this->filtersForm = 'PiZone\CatalogBundle\Form\CategoryFilterType';
        $this->manager = 'catalog';
    }
    
    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $groups = array();
            foreach($one->getGroups() as $group){
                $groups[] = array(
                    'id' => $group->getId(),
                    'title' => $group->getTitle()
                );
            }

            $this->fieldList[] = array(
                'id' => $one->getId(),
                'title' => $one->getTitle(),
                'alias' => $one->getAlias(),
                'anons' => $one->getAnons(),
                'image' => $one->getImageWebPath(),
                'groups' => $groups,
                'is_active' => $one->getIsActive(),
                'level' => $one->getLevel() +1,
                '_token' => $token->getValue()
            );

        }

        return $this->fieldList;
    }

    protected function getFilterForm()
    {
        $filters = $this->getFilters();
        if(isset($filters['parent']) && $filters['parent']) {
            $em = $this->getDoctrine()->getManager($this->manager);
            $em->persist($filters['parent']);
        }

        return $this->createForm($this->getFiltersType(), $filters);
    }

    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);


        if (isset($filterObject['all']) && null !== $filterObject['all']) {
            $queryFilter->addOrStringFilter('title', $filterObject['all']);
            $queryFilter->addOrStringFilter('alias', $filterObject['all']);
            $queryFilter->addOrStringFilter('anons', $filterObject['all']);
        }
        if (isset($filterObject['title']) && null !== $filterObject['title']) {
            $queryFilter->addStringFilter('title', $filterObject['title']);
        }
        if (isset($filterObject['alias']) && null !== $filterObject['alias']) {
            $queryFilter->addStringFilter('alias', $filterObject['alias']);
        }
        if (isset($filterObject['anons']) && null !== $filterObject['anons']) {
            $queryFilter->addStringFilter('anons', $filterObject['anons']);
        }
        if (isset($filterObject['is_active']) && null !== $filterObject['is_active']) {
            $queryFilter->addBooleanFilter('is_active', $filterObject['is_active']);
        }
        if (isset($filterObject['parent']) && null !== $filterObject['parent']) {
            $queryFilter->addNestedSetParentFilter('parent', $filterObject['parent']);
        }
    }

    protected function processSort($query)
    {
        $query->orderBy('q.lft', $this->getSortOrder());
        if ($this->getSortColumn()) {
            if (!strstr($this->getSortColumn(), '.')) { //direct column
                $query->addOrderBy('q.' . $this->getSortColumn(), $this->getSortOrder());
            }
            else {
                $path = explode('.', $this->getSortColumn());
                $column = array_pop($path);

                $join = null;
                foreach($path as $i => $one){
                    if(!$join)
                        $join = 'q';
                    else
                        $join = $path[$i - 1];
                    if(!$this->checkJoin($join, $one, $query))
                        $this->addJoinFor($join, $one, $query);
                }

                $query->addOrderBy(array_pop($path).'.'.$column, $this->getSortOrder());
            }
        }
    }
}