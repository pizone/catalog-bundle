<?php

namespace PiZone\CatalogBundle\Controller\GroupParameters;

use PiZone\AdminBundle\Controller\AActionController;
use PiZone\CatalogBundle\Entity\Content;
use PiZone\CatalogBundle\Entity\Repository\CategoryRepository;
use PiZone\CatalogBundle\Form\ContentParametersType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\GroupParameters';
        $this->repository = 'PiZone\CatalogBundle\Entity\GroupParameters';
        $this->route['delete'] = 'catalog_group_parameters_delete';
        $this->route['list']['name'] = 'catalog_group_parameters';
        $this->manager = 'catalog';
    }
}