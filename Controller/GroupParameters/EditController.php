<?php

namespace PiZone\CatalogBundle\Controller\GroupParameters;

use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Category controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\GroupParameters';
        $this->form = 'PiZone\CatalogBundle\Form\GroupParametersType';
        $this->routeList['update'] = 'catalog_group_parameters_update';
        $this->routeList['delete'] = 'catalog_group_parameters_delete';
        $this->manager = 'catalog';
    }

    protected $originalTags;

    public function preBindRequest($GroupParameters)
    {
        $this->originalTags = new ArrayCollection();

        foreach ($GroupParameters->getParameter() as $param) {
            $this->originalTags->add($param);
        }
    }

    public function preSave($GroupParameters, $form)
    {
        $em = $this->getDoctrine()->getManager($this->manager);

        foreach ($this->originalTags as $param) {
            if (false === $GroupParameters->getParameter()->contains($param)) {
                $em->remove($param);
            }
        }

        foreach ($GroupParameters->getParameter() as $item) {
            $item->setGroup($GroupParameters);
        }
    }
}