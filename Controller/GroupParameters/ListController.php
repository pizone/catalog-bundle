<?php

namespace PiZone\CatalogBundle\Controller\GroupParameters;

use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 *
 */
class ListController  extends AListController implements IListController
{
    protected $userId = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\CatalogBundle\GroupParametersList';
        $this->routeList = array(
            'list' => array(
                'name' => 'catalog_group_parameters',
                'parameters' => array()
            ),
            'delete' => 'catalog_group_parameters_delete',
            'batch' => array(
                'delete' => 'catalog_group_parameters_batch_delete',
                'active' => 'catalog_group_parameters_batch_active'
            )
        );
        $this->model = 'PiZone\CatalogBundle\Entity\GroupParameters';
        $this->repository = 'PiZone\CatalogBundle\Entity\GroupParameters';
        $this->filtersForm = 'PiZone\CatalogBundle\Form\GroupParametersFilterType';
        $this->manager = 'catalog';
    }
    
    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $params = array();

            foreach($one->getParameter() as $param){
                $params[] = array(
                    'title' => $param->getTitle(),
                    'alias' => $param->getAlias(),
                    'is_active' => $param->getIsActive()
                );
            }

            $values = array(
                'id' => $one->getId(),
                'title' => $one->getTitle(),
                'alias' => $one->getAlias(),
                'description' => $one->getDescription(),
                'parameters' => $params,
                'is_active' => $one->getIsActive(),
                '_token' => $token->getValue()
            );

            $this->fieldList[] = $values;

        }

        return $this->fieldList;
    }


    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);


        if (isset($filterObject['all']) && null !== $filterObject['all']) {
            $queryFilter->addOrStringFilter('title', $filterObject['all']);
            $queryFilter->addOrStringFilter('alias', $filterObject['all']);
            $queryFilter->addOrStringFilter('description', $filterObject['all']);
        }
        if (isset($filterObject['title']) && null !== $filterObject['title']) {
            $queryFilter->addStringFilter('title', $filterObject['title']);
        }
        if (isset($filterObject['alias']) && null !== $filterObject['alias']) {
            $queryFilter->addStringFilter('alias', $filterObject['alias']);
        }
        if (isset($filterObject['description']) && null !== $filterObject['description']) {
            $queryFilter->addStringFilter('description', $filterObject['description']);
        }
        if (isset($filterObject['is_active']) && null !== $filterObject['is_active']) {
            $queryFilter->addBooleanFilter('is_active', $filterObject['is_active']);
        }
    }
}