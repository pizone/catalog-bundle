<?php

namespace PiZone\CatalogBundle\Controller\GroupParameters;

use PiZone\AdminBundle\Controller\ANewController;
use PiZone\AdminBundle\Controller\INewController;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

/**
 * Category controller.
 *
 */
class NewController extends ANewController implements INewController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\GroupParameters';
        $this->form = 'PiZone\CatalogBundle\Form\GroupParametersType';
        $this->route['create'] = 'catalog_group_parameters_create';
        $this->manager = 'catalog';
    }

    public function preSave($GroupParameters, $form)
    {
        foreach ($GroupParameters->getParameter() as $item) {
            $item->setGroup($GroupParameters);
        }
    }
}
