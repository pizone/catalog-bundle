<?php

namespace PiZone\CatalogBundle\Controller\Content;

use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\ANewController;
use PiZone\AdminBundle\Controller\INewController;
use PiZone\CatalogBundle\Entity\ContentSale;
use PiZone\CatalogBundle\Entity\StockContent;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

/**
 * Category controller.
 *
 */
class NewController extends ANewController implements INewController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Content';
        $this->form = 'PiZone\CatalogBundle\Form\ContentType';
        $this->route['create'] = 'catalog_content_create';
        $this->manager = 'catalog';
    }

    protected function getObject(){
        $em = $this->getDoctrine()->getManager('catalog');

        $entity = new $this->model();
        $projectId = $this->container->get('session')->get('project_id');
        $project = $em->getRepository('PiZoneCatalogBundle:Project')->find($projectId);

        $stocks = $em->getRepository('PiZoneCatalogBundle:Stock')->findBy(array('is_active' => true));
        $newSale = new ContentSale();

        $entity->setSale($newSale);
        foreach($stocks as $one){
            $newStock = new StockContent();
            $newStock->setStock($one);
            $newSale->addStock($newStock);
        }

        $entity->setProject($project);

        return $entity;
    }

    protected function save($entity, $form){
        $em = $this->getDoctrine()->getManager($this->manager);

        $stocks = $entity->getSale()->getStocks();

        $temp = new ArrayCollection();
        foreach($stocks as $one){
            $temp->add($one);
        }
        $entity->getSale()->setStocks(null);

        $em->persist($entity);
        $em->flush();

        $entity->getSale()->setStocks($temp);
        $em->persist($entity);
        $em->flush();
    }
}
