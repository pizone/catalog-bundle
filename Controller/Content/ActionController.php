<?php

namespace PiZone\CatalogBundle\Controller\Content;

use PiZone\AdminBundle\Controller\AActionController;
use PiZone\CatalogBundle\Entity\Content;
use PiZone\CatalogBundle\Entity\Repository\CategoryRepository;
use PiZone\CatalogBundle\Form\ContentParametersType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Content';
        $this->repository = 'PiZone\CatalogBundle\Entity\Content';
        $this->route['delete'] = 'catalog_content_delete';
        $this->route['list']['name'] = 'catalog_content';
        $this->manager = 'catalog';
    }

    public function getParametersAction($categoryId){
        $category = $this->getDoctrine()->getManager($this->manager)->getRepository('PiZoneCatalogBundle:Category')->find($categoryId);

        $content = new Content();
        $content->setCategory($category);

        $form = $this->get('form.factory')->createNamedBuilder('content', FormType::class)
            ->add('group_parameters', ContentParametersType::class, array(
                'form_data' => $content
            ))
            ->getForm();

        $result = $this->get('pz_form')->formDataToArray($form->createView());

        $view = $this->view(json_encode(array(
            'fields' => $result
        )))
            ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');
        return $this->handleView($view);
    }

    public function getCategoriesAction(){
        $form = $this->get('form.factory')->createNamedBuilder('content', FormType::class)
            ->add('content', HiddenType::class, array(
                'required' => true
            ))
            ->add('category', EntityType::class, array(
                'label' => 'CCONTENT.FIELD.CATEGORY',
                'class' => 'PiZone\CatalogBundle\Entity\Category',
                'query_builder' => function (CategoryRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.lft', 'ASC');
                }
            ))
            ->getForm();

        $result = $this->get('pz_form')->formDataToArray($form->createView());

        $view = $this->view(json_encode(array(
            'fields' => $result
        )))
            ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');
        return $this->handleView($view);
    }

    public function saveCategoriesAction(Request $request){
        $form = $request->request->get('content');
        if($form['content'] && $form['category']) {
            $em = $this->getDoctrine()->getManager('catalog');
            $contents = $em->getRepository('PiZoneCatalogBundle:Content')->getContentsByIds($form['content']);
            if($contents){
                $category = $em->getRepository('PiZoneCatalogBundle:Category')->find($form['category']);
                foreach ($contents as $entity){
                    foreach($entity->getParameters() as $one){
                        $entity->removeParameter($one);
                    }

                    foreach($form['group_parameters'] as $params){
                        foreach($params as $one) {
                            $parameter = $this->getDoctrine()->getManager('catalog')->getRepository('PiZoneCatalogBundle:Parameter')->find($one);
                            if($parameter&& !$entity->getParameters()->contains($parameter))
                                $entity->addParameter($parameter);
                        }
                    }

                    $entity->setCategory($category);
                }
                try {
                    $em->persist($entity);
                    $em->flush();

                    $result = json_encode(array('result' => 'ok'));
                    $view = $this->view($result)
                        ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');
                    return $this->handleView($view);
                }
                catch(\Exception $error){
                    $result = json_encode(array(
                        'result' => 'error',
                        'mess' => $error->getMessage()
                    ));
                    $view = $this->view($result)
                        ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');
                    return $this->handleView($view);
                }
            }
        }
        else{
            $result = json_encode(array(
                'result' => 'error',
                'mess' => 'CCONTENT.MESSAGE.REQUIRED.CONTENT'
            ));
            $view = $this->view($result)
                ->setTemplate('PiZoneAdminBundle:Admin:_data.yml.twig');
            return $this->handleView($view);
        }
    }
}