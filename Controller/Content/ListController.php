<?php

namespace PiZone\CatalogBundle\Controller\Content;

use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 *
 */
class ListController  extends AListController implements IListController
{
    protected $userId = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\CatalogBundle\ContentList';
        $this->routeList = array(
            'list' => array(
                'name' => 'catalog_content',
                'parameters' => array()
            ),
            'delete' => 'catalog_content_delete',
            'batch' => array(
                'delete' => 'catalog_content_batch_delete',
                'active' => 'catalog_content_batch_active'
            )
        );
        $this->model = 'PiZone\CatalogBundle\Entity\Content';
        $this->repository = 'PiZone\CatalogBundle\Entity\Content';
        $this->filtersForm = 'PiZone\CatalogBundle\Form\ContentFilterType';
        $this->manager = 'catalog';
    }
    
    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $values = array(
                'id' => $one->getId(),
                'title' => $one->getTitle(),
                'alias' => $one->getAlias(),
                'article' => $one->getArticle(),
                'anons' => $one->getAnons(),
                'image' => $one->getImageWebPath(),
                'is_active' => $one->getIsActive(),
                'category' => array(),
                'is_new' => $one->getIsNew() ? true : false,
                'is_hit' => $one->getIsHit() ? true : false,
                'is_old' => $one->getIsOld() ? true : false,
                'created_at' => $one->getCreatedAt() ? $one->getCreatedAt()->format('d.m.Y H:i:s') : '-',
                'updated_at' => $one->getUpdatedAt() ? $one->getUpdatedAt()->format('d.m.Y H:i:s') : '-',
                '_token' => $token->getValue()
            );
            if($one->getCategory()){
                $values['category'] = array(
                    'id' => $one->getCategory()->getId(),
                    'title' => $one->getCategory()->getTitle()
                );
            }

            $params = array();
            foreach($one->getParameters() as $parameter){
                $params[] = array(
                    'group' => $parameter->getGroup()->getTitle(),
                    'value' => $parameter->getTitle()
                );
            }
            $values['parameters'] = $params;

            $catalogs = array();
            foreach($one->getCatalogs() as $catalog){
                $catalogs[] = array(
                    'id' => $catalog->getId(),
                    'title' => $catalog->getTitle()
                );
            }
            $values['catalogs'] = $catalogs;

            $this->fieldList[] = $values;

        }

        return $this->fieldList;
    }

    protected function buildQuery()
    {
        return $this->getDoctrine()
            ->getManager($this->manager)
            ->getRepository($this->repository)
            ->createQueryForProject();
    }

    protected function getFilterForm()
    {
        $filters = $this->getFilters();
        $em = $this->getDoctrine()->getManager($this->manager);
        if(isset($filters['category']) && $filters['category']) {
            $em->persist($filters['category']);
        }
        if(isset($filters['catalog']) && $filters['catalog']) {
            $em->persist($filters['catalog']);
        }

        $project_id = $this->container->get('session')->get('project_id');

        $project = $em->getRepository('PiZoneCatalogBundle:Project')->find($project_id);
        $filters['project'] = $project;
        $em->persist($project);

        return $this->createForm($this->getFiltersType(), $filters);
    }

    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);


        if (isset($filterObject['all']) && null !== $filterObject['all']) {
            $queryFilter->addOrStringFilter('title', $filterObject['all']);
            $queryFilter->addOrStringFilter('alias', $filterObject['all']);
            $queryFilter->addOrStringFilter('anons', $filterObject['all']);
        }
        if (isset($filterObject['title']) && null !== $filterObject['title']) {
            $queryFilter->addStringFilter('title', $filterObject['title']);
        }
        if (isset($filterObject['alias']) && null !== $filterObject['alias']) {
            $queryFilter->addStringFilter('alias', $filterObject['alias']);
        }
        if (isset($filterObject['anons']) && null !== $filterObject['anons']) {
            $queryFilter->addStringFilter('anons', $filterObject['anons']);
        }
        if (isset($filterObject['is_active']) && null !== $filterObject['is_active']) {
            $queryFilter->addBooleanFilter('is_active', $filterObject['is_active']);
        }
        if (isset($filterObject['is_new']) && null !== $filterObject['is_new']) {
            $queryFilter->addBooleanFilter('is_new', $filterObject['is_new']);
        }
        if (isset($filterObject['is_hit']) && null !== $filterObject['is_hit']) {
            $queryFilter->addBooleanFilter('is_hit', $filterObject['is_hit']);
        }
        if (isset($filterObject['is_old']) && null !== $filterObject['is_old']) {
            $queryFilter->addBooleanFilter('is_old', $filterObject['is_old']);
        }
        if (isset($filterObject['category']) && null !== $filterObject['category']) {
            $node = $this->getDoctrine()->getManager($this->manager)
                ->getRepository('PiZoneCatalogBundle:Category')->find($filterObject['category']);
            $queryFilter->leftJoin('category');
            $queryFilter->addLeftJoinNestedSetFilter('category', $node);
        }
        if (isset($filterObject['catalog']) && null !== $filterObject['catalog']) {
            $node = $this->getDoctrine()->getManager($this->manager)
                ->getRepository('PiZoneCatalogBundle:Catalog')->find($filterObject['catalog']);
            $queryFilter->leftJoin('catalogs');
            $queryFilter->addLeftJoinNestedSetFilter('catalogs', $node);
        }
        if (isset($filterObject['article_field']) && null !== $filterObject['article_field']) {
            $queryFilter->addStringFilter('more_field', $filterObject['article_field']);
        }
        if (isset($filterObject['created_at']) && null !== $filterObject['created_at']) {
            $queryFilter->addDateFilter('created_at', $filterObject['created_at']);
        }
        if (isset($filterObject['updated_at']) && null !== $filterObject['updated_at']) {
            $queryFilter->addDateFilter('updated_at', $filterObject['updated_at']);
        }
    }

    protected function setFilters($filters)
    {
        $this->get('session')->set($this->prefixSession . '\Filters', $filters);
        $this->get('session')->set('project_id', $filters['project']->getId());
    }
}