<?php

namespace PiZone\CatalogBundle\Controller\Content;

use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;
use PiZone\CatalogBundle\Entity\ContentSale;
use PiZone\CatalogBundle\Entity\StockContent;
use PiZone\CatalogBundle\Entity\Tag;

/**
 * Category controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Content';
        $this->form = 'PiZone\CatalogBundle\Form\ContentType';
        $this->routeList['update'] = 'catalog_content_update';
        $this->routeList['delete'] = 'catalog_content_delete';
        $this->manager = 'catalog';
    }

    protected $parameters = array();

    public function preBindRequest($entity){
        $data = $this->get('request_stack')->getCurrentRequest()->request->get('content');
        if(isset($data['group_parameters']))
            $this->parameters = $data['group_parameters'];
    }

    public function preSave($entity, $form){
        foreach($entity->getParameters() as $one){
            $entity->removeParameter($one);
        }

        foreach($this->parameters as $params){
            foreach($params as $one) {
                $parameter = $this->getDoctrine()->getManager('catalog')->getRepository('PiZoneCatalogBundle:Parameter')->find($one);
                if($parameter&& !$entity->getParameters()->contains($parameter))
                    $entity->addParameter($parameter);
            }
        }

        foreach($entity->getTags() as $one){
            $entity->removeTag($one);
        }
        $tags = json_decode($entity->getStringTags());
        foreach($tags as $tag){
            if(isset($tag->id)){
                $tagEntity = $this->getDoctrine()->getManager('catalog')->getRepository('PiZoneCatalogBundle:Tag')->find($tag->id);
            }
            else{
                $tagEntity = $this->getDoctrine()->getManager('catalog')->getRepository('PiZoneCatalogBundle:Tag')->findOneBy(array('title' => $tag->name));
                if(!$tagEntity){
                    $tagEntity = new Tag();
                    $tagEntity->setTitle($tag->name);
                }
            }

            $entity->addTag($tagEntity);
        }
    }

    public function prepareView($id, $editForm){
        $data = $this->get('pz_form')->formDataToArray($editForm->createView());

        if($editForm->getData()->getImageWebPath()){
            $info = pathinfo($editForm->getData()->getImageAbsolutePath());

            $data['image']['info'] = array(
                'path' => $editForm->getData()->getImageWebPath(),
                'size' => file_exists($editForm->getData()->getImageAbsolutePath()) ? filesize($editForm->getData()->getImageAbsolutePath()) : 0,
                'name' => $info ? $info['basename'] : '',
                'mimetype' => $info ? $info['extension'] : ''
            );
        }

        if($editForm->getData()->getBigImageWebPath()){
            $info = pathinfo($editForm->getData()->getBigImageAbsolutePath());

            $data['big_image']['info'] = array(
                'path' => $editForm->getData()->getBigImageWebPath(),
                'size' => file_exists($editForm->getData()->getBigImageAbsolutePath()) ? filesize($editForm->getData()->getBigImageAbsolutePath()) : 0,
                'name' => $info ? $info['basename'] : '',
                'mimetype' => $info ? $info['extension'] : ''
            );
        }

        return array(
            'action' => $this->generateUrl($this->routeList['update'], array('id' => $id)),
            'fields' => $data,
            '_delete_token' => $this->getDeleteFormToken($id)
        );
    }

    protected function getObject($em, $id){
        $content =  $em->getRepository($this->model)->find($id);

        $stocks = $em->getRepository('PiZoneCatalogBundle:Stock')->findBy(array('is_active' => true));
        $newSale = $content->getSale();
        if(!$newSale) {
            $newSale = new ContentSale();
            $content->setSale($newSale);
        }
        foreach($stocks as $one){
            $id = $one->getId();
            $test = $newSale->getStocks()->filter(function($entity) use ($id){
                return $entity->getStock()->getId() == $id;
            });
            if($test->count() == 0){
                $newStock = new StockContent();
                $newStock->setStock($one);
                $newSale->addStock($newStock);
            }
        }

        return $content;
    }

    protected function save($em, $entity, $form){
        $stocks = $entity->getSale()->getStocks();

        $temp = new ArrayCollection();
        foreach($stocks as $one){
            $temp->add($one);
        }
        $entity->getSale()->setStocks(null);

        $em->persist($entity);
        $em->flush();

        $entity->getSale()->setStocks($temp);
        $em->persist($entity);
        $em->flush();
    }
}