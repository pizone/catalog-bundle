<?php

namespace PiZone\CatalogBundle\Controller\User;

use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;
use Symfony\Component\HttpFoundation\Request;

/**
 * WebItem controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\User';
        $this->form = 'PiZone\CatalogBundle\Form\UserType';
        $this->routeList['update'] = 'catalog_user_update';
        $this->routeList['delete'] = 'catalog_user_delete';
        $this->manager = 'catalog';
    }

    protected function save($em, $user, $form){
        $userManager = $this->get('pi_zone.catalog_user_manager');
        $userManager->updateUser($user);
    }
}