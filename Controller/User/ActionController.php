<?php

namespace PiZone\CatalogBundle\Controller\User;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * WebItem controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\User';
        $this->repository = 'PiZone\CatalogBundle\Entity\User';
        $this->route['delete'] = 'catalog_user_delete';
        $this->route['list']['name'] = 'catalog_user';
        $this->manager = 'catalog';
    }
}