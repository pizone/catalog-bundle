<?php

namespace PiZone\CatalogBundle\Controller\User;

use PiZone\AdminBundle\Controller\ANewController;
use PiZone\AdminBundle\Controller\INewController;

/**
 * WebItem controller.
 *
 */
class NewController extends ANewController implements INewController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\User';
        $this->form = 'PiZone\CatalogBundle\Form\UserType';
        $this->route['create'] = 'catalog_user_create';
        $this->manager = 'catalog';
    }

    protected function getObject(){
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $user->setEnabled(true);
        return $user;
    }

    protected function save($user, $form){
        $userManager = $this->get('fos_user.user_manager');
        $user->addRole('ROLE_USER');
        $userManager->updateUser($user);
    }
}
