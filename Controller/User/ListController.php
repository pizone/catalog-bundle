<?php

namespace PiZone\CatalogBundle\Controller\User;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\View\View;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use PiZone\CatalogBundle\PiZoneCatalogBundle;
use Symfony\Component\HttpFoundation\Request;

/**
 * User list controller.
 *
 */
class ListController  extends AListController implements IListController
{
    protected $userId = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\CatalogBundle\UserList';
        $this->routeList = array(
            'list' => array(
                'name' => 'catalog_user',
                'parameters' => array()
            ),
            'delete' => 'catalog_user_delete',
            'batch' => array(
                'delete' => 'catalog_user_batch_delete',
                'active' => 'catalog_user_batch_active'
            )
        );
        $this->model = 'PiZone\CatalogBundle\Entity\User';
        $this->repository = 'PiZone\CatalogBundle\Entity\User';
        $this->filtersForm = 'PiZone\CatalogBundle\Form\UserFilterType';
        $this->manager = 'catalog';
    }
    
    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $this->fieldList[] = array(
                'id' => $one->getId(),
                'username' => $one->getUsername(),
                'full_name' => $one->getFullName(),
                'company' => $one->getCompany(),
                'position_held' => $one->getPositionHeld(),
                'address' => $one->getAddress(),
                'email' => $one->getEmail(),
                'phone' => $one->getPhone(),
                'messenger' => $one->getMessenger(),
                'enabled' => $one->isEnabled(),
                'registration_at' => $one->getRegistrationAt() ? $one->getRegistrationAt()->format('d.m.Y H:i'): '',
                'last_login' => $one->getLastLogin() ? $one->getLastLogin()->format('d.m.Y H:i'): '',
                'roles' => $one->getRoles(),
                '_token' => $token->getValue()
            );
        }

        return $this->fieldList;
    }

    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);

        if (isset($filterObject['all']) && null !== $filterObject['all']) {
            $queryFilter->addOrStringFilter('username', $filterObject['all']);
            $queryFilter->addOrStringFilter('email', $filterObject['all']);
        }
        if (isset($filterObject['username']) && null !== $filterObject['username']) {
            $queryFilter->addStringFilter('username', $filterObject['username']);
        }
        if (isset($filterObject['email']) && null !== $filterObject['email']) {
            $queryFilter->addStringFilter('email', $filterObject['email']);
        }
        if (isset($filterObject['is_enabled']) && null !== $filterObject['is_enabled']) {
            $queryFilter->addBooleanFilter('is_enabled', $filterObject['is_enabled']);
        }
        if (isset($filterObject['registration_at']) && null !== $filterObject['registration_at']) {
            $this->addPublishFilter($queryFilter, $filterObject['registration_at']);
        }
        if (isset($filterObject['last_login']) && null !== $filterObject['last_login']) {
            $this->addPublishFilter($queryFilter, $filterObject['last_login']);
        }
    }

    public function findAction(Request $request){
        $search = $request->query->get('str');
        $em = $this->getDoctrine()->getManager($this->manager);
        $users = $em->getRepository('PiZoneCatalogBundle:User')->findUserLike($search);

        $result = array();
        foreach($users as $user){
            $result[] = array(
                'id' => $user->getId(),
                'name' => $user->getUsername(),
                'description' => $user->getFullName()
            );
        }

        $view = new View(
            array(
                'items' => $result
            )
        );
        return $this->handleView($view);
    }
}