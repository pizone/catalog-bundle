<?php

namespace PiZone\CatalogBundle\Controller\Project;

use PiZone\AdminBundle\Controller\AEditController;
use PiZone\AdminBundle\Controller\IEditController;

/**
 * Category controller.
 *
 */
class EditController extends AEditController implements IEditController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Project';
        $this->form = 'PiZone\CatalogBundle\Form\ProjectType';
        $this->routeList['update'] = 'catalog_project_update';
        $this->routeList['delete'] = 'catalog_project_delete';
        $this->manager = 'catalog';
    }

    protected $originalFields = array();

    public function prepareView($id, $editForm){
        $data = $this->get('pz_form')->formDataToArray($editForm->createView());

        if($editForm->getData()->getWebPath()){
            $info = pathinfo($editForm->getData()->getAbsolutePath());

            $data['image']['info'] = array(
                'path' => $editForm->getData()->getWebPath(),
                'size' => file_exists($editForm->getData()->getAbsolutePath()) ? filesize($editForm->getData()->getAbsolutePath()) : 0,
                'name' => $info ? $info['basename'] : '',
                'mimetype' => $info ? $info['extension'] : ''
            );
        }

        return array(
            'action' => $this->generateUrl($this->routeList['update'], array('id' => $id)),
            'fields' => $data,
            '_delete_token' => $this->getDeleteFormToken($id)
        );
    }

    public function preBindRequest($Project) {
        foreach ($Project->getFields() as $field) {
            $this->originalFields->add($field);
        }
    }

    public function preSave($Project, $form) {
        $em = $this->getDoctrine()->getManager($this->manager);

        foreach ($this->originalFields as $param) {
            if (false === $Project->getFields()->contains($param)) {
                $em->remove($param);
            }
        }

        foreach ($Project->getFields() as $item) {
            $item->setProject($Project);
        }
    }
}