<?php

namespace PiZone\CatalogBundle\Controller\Project;

use PiZone\AdminBundle\Controller\AActionController;

/**
 * Category controller.
 *
 */
class ActionController extends AActionController
{
    public function __construct(){
        $this->model = 'PiZone\CatalogBundle\Entity\Project';
        $this->repository = 'PiZone\CatalogBundle\Entity\Stock';
        $this->route['delete'] = 'catalog_project_delete';
        $this->route['list']['name'] = 'catalog_project';
        $this->manager = 'catalog';
    }
}