<?php

namespace PiZone\CatalogBundle\Controller\Project;

use FOS\RestBundle\View\View;
use Doctrine\Common\Collections\ArrayCollection;
use PiZone\AdminBundle\Controller\AListController;
use PiZone\AdminBundle\Controller\IListController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 *
 */
class ListController  extends AListController implements IListController
{
    protected $userId = null;

    public function __construct(){
        $this->prefixSession = 'PiZone\CatalogBundle\ProjectList';
        $this->routeList = array(
            'list' => array(
                'name' => 'catalog_project',
                'parameters' => array()
            ),
            'delete' => 'catalog_project_delete',
            'batch' => array(
                'delete' => 'catalog_project_batch_delete',
                'active' => 'catalog_project_batch_active'
            )
        );
        $this->model = 'PiZone\CatalogBundle\Entity\Project';
        $this->repository = 'PiZone\CatalogBundle\Entity\Project';
        $this->filtersForm = 'PiZone\CatalogBundle\Form\ProjectFilterType';
        $this->manager = 'catalog';
    }
    
    protected function getlist($entities){
        $tokenManager = $this->get('security.csrf.token_manager');

        foreach ($entities as $one) {
            $tokenId = $this->generateUrl($this->routeList['delete'], array('id' => $one->getId()));
            $token = $tokenManager->getToken($tokenId);

            $this->fieldList[] = array(
                'id' => $one->getId(),
                'title' => $one->getTitle(),
                'alias' => $one->getAlias(),
                'description' => $one->getDescription(),
                'image' => $one->getWebPath(),
                'is_active' => $one->getIsActive(),
                '_token' => $token->getValue()
            );
        }

        return $this->fieldList;
    }

    protected function processFilters($query)
    {
        $filterObject = $this->getFilters();

        $queryFilter = $this->getQueryFilter();
        $queryFilter->setQuery($query);


        if (isset($filterObject['all']) && null !== $filterObject['all']) {
            $queryFilter->addOrStringFilter('title', $filterObject['all']);
            $queryFilter->addOrStringFilter('alias', $filterObject['all']);
            $queryFilter->addOrStringFilter('description', $filterObject['all']);
        }
        if (isset($filterObject['title']) && null !== $filterObject['title']) {
            $queryFilter->addStringFilter('title', $filterObject['title']);
        }
        if (isset($filterObject['alias']) && null !== $filterObject['alias']) {
            $queryFilter->addStringFilter('alias', $filterObject['alias']);
        }
        if (isset($filterObject['description']) && null !== $filterObject['description']) {
            $queryFilter->addStringFilter('description', $filterObject['description']);
        }
        if (isset($filterObject['is_active']) && null !== $filterObject['is_active']) {
            $queryFilter->addBooleanFilter('is_active', $filterObject['is_active']);
        }
    }

    protected function buildQuery()
    {
        return $this->getDoctrine()
            ->getManager($this->manager)
            ->getRepository($this->repository)
            ->getAvailableList();
    }
}