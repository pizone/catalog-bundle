<?php

namespace PiZone\CatalogBundle\Form;


use PiZone\AdminBundle\Form\Type\EntityHiddenType;
use PiZone\CatalogBundle\Entity\ContentOrder;
use PiZone\CatalogBundle\Entity\OrderStatus;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_active', CheckboxType::class, array(
                'label' => 'ORDER.FIELD.IS_ACTIVE.LABEL'
            ))
            ->add('is_confirm', CheckboxType::class, array(
                'label' => 'ORDER.FIELD.IS_CONFIRM.LABEL'
            ))
            ->add('itog', TextType::class, array(
                'label' => 'ORDER.FIELD.ITOG'
            ))
            ->add('discount', TextType::class, array(
                'label' => 'ORDER.FIELD.DISCOUNT'
            ))
            ->add('status', ChoiceType::class, array(
                'label' => 'ORDER.FIELD.STATUS.LABEL',
                'choices' => OrderStatus::getChoices()
            ))
            ->add('stock', EntityType::class, array(
                'label' => 'ORDER.FIELD.STOCK',
                'class' => 'PiZone\CatalogBundle\Entity\Stock'
            ))
            ->add('content', CollectionType::class, array(
                'entry_type' => ContentOrderType::class,
                'allow_add' => true,
                'allow_delete' => true
            ))
            ->add('comment', TextareaType::class, array(
                'label' => 'ORDER.FIELD.COMMENT'
            ))
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();

            $userOptions = array(
                'label' => 'ORDER.FIELD.USER.TITLE',
                'class' => 'PiZOne\CatalogBundle\Entity\User',
                'em' => 'catalog'
            );
            if ($user = $data->getUser()) {
                $userOptions['attr'] = array(
                    'model' => array(
                        'id' => $user->getId(),
                        'name' => $user->getUsername(),
                        'description' => $user->getFullName()
                    ));
            }
            $form->add('user', EntityHiddenType::class, $userOptions);
        });

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZOrder', 'PZContentOrder'),
            'cascade_validation' => true,
            'data_class' => 'PiZone\CatalogBundle\Entity\Order'
        ));
    }
}
