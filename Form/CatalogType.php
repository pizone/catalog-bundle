<?php

namespace PiZone\CatalogBundle\Form;


use PiZone\CatalogBundle\Entity\Repository\CatalogRepository;
use PiZone\CatalogBundle\Entity\Repository\ContentRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CatalogType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $builder->getData();

        $builder
            ->add('is_active', CheckboxType::class, array(
                'label' => 'CATALOG.FIELD.IS_ACTIVE.LABEL'
            ))
            ->add('title', TextType::class, array(
                'label' => 'CATALOG.FIELD.TITLE'
            ))
            ->add('alias', TextType::class, array(
                'label' => 'CATALOG.FIELD.ALIAS'
            ))
            ->add('parent', EntityType::class, array(
                'label' => 'CATALOG.FIELD.PARENT',
                'class' => 'PiZone\CatalogBundle\Entity\Catalog',
                'query_builder' => function (CatalogRepository $er) use ($data) {
                    return $er->createQueryForProjectWithoutChild($data);
                }
            ))
            ->add('content', EntityType::class, array(
                'class' => 'PiZoneCatalogBundle:Content',
                'required' => false,
                'query_builder' => function(ContentRepository $er) {
                    return $er->getContentForSelectList();
                },
            ))
            ->add('meta', CatalogMetaType::class);
        ;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZCatalog'),
            'data_class' => 'PiZone\CatalogBundle\Entity\Catalog'
        ));
    }
}
