<?php

namespace PiZone\CatalogBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContentParametersType extends AbstractType
{

    protected $category;
    protected $selected;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $options['form_data'];
        $this->category = $data->getCategory();
        $this->selected = $data->getParameters();

        if($this->category) {
            foreach ($this->category->getGroups() as $group) {
                $options = array(
                    'choices' => $group->getParameter(),
                    'class' => 'PiZone\CatalogBundle\Entity\Parameter',
                    'label' => $group->getTitle(),
                    'multiple' => true,
                    'em' => 'catalog',
                    'required' => false,
                    'attr' => array('class' => 'multiselect'),

                );
                $p = array();
                if ($this->selected->count() > 0) {
                    foreach ($group->getParameter() as $one) {
                        if ($this->selected->contains($one))
                            $p[] = $one;
                    }

                    $options['data'] = $p;
                }
                $builder->add('group_' . $group->getId(), EntityType::class, $options);
            }
        }
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'allow_extra_fields' => true,
            'form_data' => ''
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'group_parameters';
    }
}

