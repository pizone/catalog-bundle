<?php

namespace PiZone\CatalogBundle\Form;

use PiZone\CatalogBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array(
                'label' => 'CUSER.FIELD.USERNAME',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('fullName', TextType::class, array(
                'label' => 'CUSER.FIELD.FULL_NAME',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('email', EmailType::class, array(
                'label' => 'CUSER.FIELD.EMAIL',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('phone', TextType::class, array(
                'label' => 'CUSER.FIELD.PHONE',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Телефон'
                )
            ))
            ->add('company', TextType::class, array(
                'label' => 'CUSER.FIELD.COMPANY',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('positionHeld', TextType::class, array(
                'label' => 'CUSER.FIELD.POSITION_HELD',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Должность'
                )
            ))
            ->add('address', TextType::class, array(
                'label' => 'CUSER.FIELD.ADDRESS',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('messenger', TextType::class, array(
                'label' => 'CUSER.FIELD.MESSENGER',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('enabled', CheckboxType::class, array(
                'label' => 'CUSER.FIELD.ENABLED.LABEL',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'required' => false,
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'USER.FIELD.PASSWORD', 'required' => false),
                'second_options' => array('label' => 'USER.FIELD.PASSWORD_CONFIRMATION', 'required' => false),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('User', 'Profile'),
            'data_class' => 'PiZone\CatalogBundle\Entity\User'
        ));
    }
}
