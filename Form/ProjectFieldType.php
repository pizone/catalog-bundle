<?php

namespace PiZone\CatalogBundle\Form;


use PiZone\CatalogBundle\Entity\Repository\FieldRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectFieldType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_active', CheckboxType::class, array(
                'label' => 'PROJECT.FIELD.IS_ACTIVE.LABEL'
            ))
            ->add('field', EntityType::class, array(
                'required' => true,
                'label' => 'PROJECT.FIELD.FIELD',
                'class' => 'PiZone\CatalogBundle\Entity\Field',
                'placeholder' => 'PROJECT.FIELD.FIELD_PLACEHOLDER',
                'query_builder' => function(FieldRepository $er) {
                    return $er->getActiveFields();
                }
            ))
            ->add('title', TextType::class, array(
                'label' => 'PROJECT.FIELD.TITLE'
            ))
            ->add('alias', TextType::class, array(
                'label' => 'PROJECT.FIELD.ALIAS'
            ))
            ->add('default_value', TextareaType::class, array(
                'label' => 'PROJECT.FIELD.DEFAULT_VALUE'
            ))
            ->add('sort', TextType::class, array(
                'label' => 'PROJECT.FIELD.SORT'
            ))
        ;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZProjectField'),
            'data_class' => 'PiZone\CatalogBundle\Entity\ProjectField'
        ));
    }
}
