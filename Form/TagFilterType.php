<?php

namespace PiZone\CatalogBundle\Form;

use PiZone\CatalogBundle\Entity\Repository\ProjectRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TagFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'TAG.FIELD.ALL',
                'required' => false
            ))
            ->add('title', TextType::class, array(
                'label' => 'TAG.FIELD.TITLE',
                'required' => false
            ))
            ->add('project', EntityType::class, array(
                'label' => 'TAG.FIELD.PROJECT',
                'class' => 'PiZone\CatalogBundle\Entity\PROJECT',
                'required' => false,
                'query_builder' => function (ProjectRepository $er) {
                    return $er->getAvailableList();
                }
            ))
            ->add('is_active',  ChoiceType::class, array(
                'choices' =>   array('TAG.FIELD.IS_ACTIVE.NO' => 0,    'TAG.FIELD.IS_ACTIVE.YES' => 1,),
                'label' => 'TAG.FIELD.IS_ACTIVE.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'TAG.FIELD.IS_ACTIVE.ANY',
                'empty_data'  => null,
                'required' => false
            ))
        ;
    }
}
