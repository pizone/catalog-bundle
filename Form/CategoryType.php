<?php

namespace PiZone\CatalogBundle\Form;


use PiZone\CatalogBundle\Entity\Repository\CategoryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $builder->getData();

        $builder
            ->add('is_active', CheckboxType::class, array(
                'label' => 'CATEGORY.FIELD.IS_ACTIVE.LABEL'
            ))
            ->add('title', TextType::class, array(
                'label' => 'CATEGORY.FIELD.TITLE'
            ))
            ->add('alias', TextType::class, array(
                'label' => 'CATEGORY.FIELD.ALIAS'
            ))
            ->add('anons', TextareaType::class, array(
                'label' => 'CATEGORY.FIELD.ANONS'
            ))
            ->add('show_editor_anons', CheckboxType::class, array(
                'label' => 'CATEGORY.FIELD.EDITOR_CONTENT'
            ))
            ->add('image', FileType::class, array(
                'label' => 'CATEGORY.FIELD.IMAGE'
            ))
            ->add('delete_image', CheckboxType::class, array(
                'label' => 'CATEGORY.FIELD.DELETE_IMAGE'
            ))
            ->add('parent', EntityType::class, array(
                'label' => 'CATEGORY.FIELD.PARENT',
                'class' => 'PiZone\CatalogBundle\Entity\Category',
                'query_builder' => function (CategoryRepository $er) use ($data) {
                    $q = $er->createQueryBuilder('u')
                        ->orderBy('u.lft', 'ASC');
                    if($data->getId())
                        $q = $q->andWhere('u.id != :id')
                            ->andWhere('u.lft < :lft or u.rgt > :rgt')
                            ->setParameter('lft', $data->getLft())
                            ->setParameter('rgt', $data->getRgt())
                            ->setParameter('id', $data->getId());
                    return $q;
                }
            ))
            ->add('groups', EntityType::class, array(
                'label' => 'CATEGORY.FIELD.GROUPS',
                'required' => false,
                'multiple' => true,
                'class' => 'PiZone\CatalogBundle\Entity\GroupParameters'
            ))
           ;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZCatalogCategory'),
            'data_class' => 'PiZone\CatalogBundle\Entity\Category'
        ));
    }
}
