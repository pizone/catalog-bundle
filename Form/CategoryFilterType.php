<?php

namespace PiZone\CatalogBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CategoryFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'CATEGORY.FIELD.ALL',
                'required' => false
            ))
            ->add('title', TextType::class, array(
                'label' => 'CATEGORY.FIELD.TITLE',
                'required' => false
            ))
            ->add('alias', TextType::class, array(
                'label' => 'CATEGORY.FIELD.ALIAS',
                'required' => false
            ))
            ->add('anons', TextType::class, array(
                'label' => 'CATEGORY.FIELD.ANONS',
                'required' => false
            ))
            ->add('is_active',  ChoiceType::class, array(
                'choices' =>   array('CATEGORY.FIELD.IS_ACTIVE.NO' => 0,    'CATEGORY.FIELD.IS_ACTIVE.YES' => 1,),
                'label' => 'CATEGORY.FIELD.IS_ACTIVE.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'CATEGORY.FIELD.IS_ACTIVE.ANY',
                'empty_data'  => null,
                'required' => false
            ))
            ->add('parent', EntityType::class, array(
                'label' => 'CATEGORY.FIELD.PARENT',
                'class' => 'PiZone\CatalogBundle\Entity\Category',
                'required' => false
            ))
        ;
    }
}
