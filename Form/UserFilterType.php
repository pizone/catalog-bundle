<?php

namespace PiZone\CatalogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'CUSER.FIELD.ALL',
                'required' => false
            ))
            ->add('username', TextType::class, array(
                'label' => 'CUSER.FIELD.USERNAME',
                'required' => false
            ))
            ->add('full_name', TextType::class, array(
                'label' => 'CUSER.FIELD.FULL_NAME',
                'required' => false
            ))
            ->add('email', TextType::class, array(
                'label' => 'CUSER.FIELD.EMAIL',
                'required' => false
            ))
            ->add('phone', TextType::class, array(
                'label' => 'CUSER.FIELD.PHONE',
                'required' => false
            ))
            ->add('company', TextType::class, array(
                'label' => 'CUSER.FIELD.COMPANY',
                'required' => false
            ))
            ->add('position_held', TextType::class, array(
                'label' => 'CUSER.FIELD.POSITION_HELD',
                'required' => false
            ))
            ->add('address', TextType::class, array(
                'label' => 'CUSER.FIELD.ADDRESS',
                'required' => false
            ))
            ->add('messenger', TextType::class, array(
                'label' => 'CUSER.FIELD.MESSENGER',
                'required' => false
            ))
            ->add('enabled',  ChoiceType::class, array(
                'choices' =>   array('CUSER.FIELD.ENABLED.NO' => 0,    'CUSER.FIELD.ENABLED.YES' => 1,),
                'label' => 'CUSER.FIELD.ENABLED.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'CUSER.FIELD.ENABLED.ANY',
                'empty_data'  => null,
                'required' => false
            ))
            ->add(
                $builder->create('registration_at', FormType::class, array(
                    'by_reference' => true,
                    'label' => 'CUSER.FIELD.REGISTRATION_AT'
                ))
                    ->add('from', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
                    ->add('to', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
            )
            ->add(
                $builder->create('last_login', FormType::class, array(
                    'by_reference' => true,
                    'label' => 'CUSER.FIELD.LAST_LOGIN'
                ))
                    ->add('from', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
                    ->add('to', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
            )
        ;
    }
}
