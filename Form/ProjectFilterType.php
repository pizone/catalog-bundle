<?php

namespace PiZone\CatalogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProjectFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'PROJECT.FIELD.ALL',
                'required' => false
            ))
            ->add('title', TextType::class, array(
                'label' => 'PROJECT.FIELD.TITLE',
                'required' => false
            ))
            ->add('alias', TextType::class, array(
                'label' => 'PROJECT.FIELD.ALIAS',
                'required' => false
            ))
            ->add('description', TextType::class, array(
                'label' => 'PROJECT.FIELD.DESCRIPTION',
                'required' => false
            ))
            ->add('is_active',  ChoiceType::class, array(
                'choices' =>   array('PROJECT.FIELD.IS_ACTIVE.NO' => 0,    'PROJECT.FIELD.IS_ACTIVE.YES' => 1,),
                'label' => 'PROJECT.FIELD.IS_ACTIVE.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'PROJECT.FIELD.IS_ACTIVE.ANY',
                'empty_data'  => null,
                'required' => false
            ))
        ;
    }
}
