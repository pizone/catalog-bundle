<?php

namespace PiZone\CatalogBundle\Form;

use PiZone\CatalogBundle\Entity\OrderStatus;
use PiZone\CatalogBundle\Entity\Repository\ProjectRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class OrderFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'ORDER.FIELD.ALL',
                'required' => false
            ))
            ->add('project', EntityType::class, array(
                'label' => 'CATALOG.FIELD.PROJECT',
                'class' => 'PiZone\CatalogBundle\Entity\PROJECT',
                'required' => false,
                'query_builder' => function (ProjectRepository $er) {
                    return $er->getAvailableList();
                }
            ))
            ->add('status', ChoiceType::class, array(
                'label' => 'ORDER.FIELD.STATUS.LABEL',
                'choices' => OrderStatus::getChoices()
            ))
            ->add('is_active',  ChoiceType::class, array(
                'choices' =>   array('ORDER.FIELD.IS_ACTIVE.NO' => 0,    'ORDER.FIELD.IS_ACTIVE.YES' => 1,),
                'label' => 'ORDER.FIELD.IS_ACTIVE.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'ORDER.FIELD.IS_ACTIVE.ANY',
                'empty_data'  => null,
                'required' => false
            ))
            ->add('is_confirm',  ChoiceType::class, array(
                'choices' =>   array('ORDER.FIELD.IS_CONFIRM.NO' => 0,    'ORDER.FIELD.IS_CONFIRM.YES' => 1,),
                'label' => 'ORDER.FIELD.IS_CONFIRM.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'ORDER.FIELD.IS_CONFIRM.ANY',
                'empty_data'  => null,
                'required' => false
            ))
        ;
    }
}
