<?php

namespace PiZone\CatalogBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class GroupParametersFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'GPARAM.FIELD.ALL',
                'required' => false
            ))
            ->add('title', TextType::class, array(
                'label' => 'GPARAM.FIELD.TITLE',
                'required' => false
            ))
            ->add('alias', TextType::class, array(
                'label' => 'GPARAM.FIELD.ALIAS',
                'required' => false
            ))
            ->add('description', TextType::class, array(
                'label' => 'GPARAM.FIELD.DESCRIPTION'
            ))
            ->add('is_active',  ChoiceType::class, array(
                'choices' =>   array('GPARAM.FIELD.IS_ACTIVE.NO' => 0,    'GPARAM.FIELD.IS_ACTIVE.YES' => 1,),
                'label' => 'GPARAM.FIELD.IS_ACTIVE.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'GPARAM.FIELD.IS_ACTIVE.ANY',
                'empty_data'  => null,
                'required' => false
            ))
        ;
    }
}
