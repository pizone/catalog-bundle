<?php

namespace PiZone\CatalogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class ParameterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', HiddenType::class, array(
                'mapped' => false
            ))
            ->add('title', TextType::class, array(
                'label' => 'GPARAM.FIELD.PARAMS.TITLE',
                'required' => false
            ))
            ->add('alias', TextType::class, array(
                'label' => 'GPARAM.FIELD.PARAMS.ALIAS',
                'required' => false
            ))
            ->add('is_active', CheckboxType::class, array(
                'label' => 'GPARAM.FIELD.PARAMS.IS_ACTIVE'
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZParameter'),
            'data_class' => 'PiZone\CatalogBundle\Entity\Parameter'
        ));
    }
}
