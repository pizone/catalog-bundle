<?php

namespace PiZone\CatalogBundle\Form;

use PiZone\CatalogBundle\Entity\Repository\CategoryRepository;
use PiZone\CatalogBundle\Entity\Repository\ProjectRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ContentFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'CCONTENT.FIELD.ALL',
                'required' => false
            ))
            ->add('project', EntityType::class, array(
                'label' => 'CATALOG.FIELD.PROJECT',
                'class' => 'PiZone\CatalogBundle\Entity\PROJECT',
                'required' => false,
                'query_builder' => function (ProjectRepository $er) {
                    return $er->getAvailableList();
                }
            ))
            ->add('title', TextType::class, array(
                'label' => 'CCONTENT.FIELD.TITLE',
                'required' => false
            ))
            ->add('article_field', TextType::class, array(
                'label' => 'CCONTENT.FIELD.ARTICLE'
            ))
            ->add('alias', TextType::class, array(
                'label' => 'CCONTENT.FIELD.ALIAS',
                'required' => false
            ))
            ->add('catalog', EntityType::class, array(
                'label' => 'CCONTENT.FIELD.CATALOG',
                'class' => 'PiZone\CatalogBundle\Entity\Catalog',
                'required' => false
            ))
            ->add('category', EntityType::class, array(
                'label' => 'CCONTENT.FIELD.CATEGORY',
                'class' => 'PiZone\CatalogBundle\Entity\Category',
                'required' => false,
                'query_builder' => function (CategoryRepository $er) {
                    return $er->getListCategories();
                }
            ))
            ->add('anons', TextType::class, array(
                'label' => 'CCONTENT.FIELD.ANONS',
                'required' => false
            ))
            ->add('is_active',  ChoiceType::class, array(
                'choices' =>   array('CCONTENT.FIELD.IS_ACTIVE.NO' => 0,    'CCONTENT.FIELD.IS_ACTIVE.YES' => 1,),
                'label' => 'CCONTENT.FIELD.IS_ACTIVE.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'CCONTENT.FIELD.IS_ACTIVE.ANY',
                'empty_data'  => null,
                'required' => false
            ))
            ->add(
                $builder->create('created_at', FormType::class, array(
                    'by_reference' => true,
                    'label' => 'CCONTENT.FIELD.CREATED_AT'
                ))
                    ->add('from', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
                    ->add('to', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
            )
            ->add(
                $builder->create('updated_at', FormType::class, array(
                    'by_reference' => true,
                    'label' => 'CCONTENT.FIELD.UPDATED_AT'
                ))
                    ->add('from', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
                    ->add('to', DateType::class, array(
                        'widget' => 'single_text',
                        'format' => 'dd.MM.yyyy',
                        'required' => false
                    ))
            )
            ->add('is_new',  ChoiceType::class, array(
                'choices' =>   array('CCONTENT.FIELD.IS_NEW.NO' => 0,    'CCONTENT.FIELD.IS_NEW.YES' => 1,),
                'label' => 'CCONTENT.FIELD.IS_NEW.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'CCONTENT.FIELD.IS_NEW.ANY',
                'empty_data'  => null,
                'required' => false
            ))
            ->add('is_hit',  ChoiceType::class, array(
                'choices' =>   array('CCONTENT.FIELD.IS_HIT.NO' => 0,    'CCONTENT.FIELD.IS_HIT.YES' => 1,),
                'label' => 'CCONTENT.FIELD.IS_HIT.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'CCONTENT.FIELD.IS_HIT.ANY',
                'empty_data'  => null,
                'required' => false
            ))
            ->add('is_old',  ChoiceType::class, array(
                'choices' =>   array('CCONTENT.FIELD.IS_OLD.NO' => 0,    'CCONTENT.FIELD.IS_OLD.YES' => 1,),
                'label' => 'CCONTENT.FIELD.IS_OLD.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'CCONTENT.FIELD.IS_OLD.ANY',
                'empty_data'  => null,
                'required' => false
            ))
        ;
    }
}
