<?php

namespace PiZone\CatalogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MoreFieldType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    protected $data;
    protected $fields;
    protected $formId;

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $data = $options['form_data'];
        $this->formId = $data->getId();
        $this->fields = $data->getProject()->getFields();
        $this->data = $data->getMoreField() ? $data->getMoreField() : array();

        foreach($this->fields as $one){
            $type = $one->getField()->getAlias();
            $activeField = $one->getField()->getIsActive();
            
            if($type == 'input' && $one->getIsActive() && $activeField){
                $builder->add($one->getAlias(), TextType::class, array(
                    'required' => false,
                    'label' => $one->getTitle(),
                    'data' => $this->isNew() ? $one->getDefaultValue() : (array_key_exists($one->getAlias(), $this->data) ? $this->data[$one->getAlias()] : '')
                ));
            }
            if($type == 'textarea' && $one->getIsActive() && $activeField){
                $builder->add($one->getAlias(), TextareaType::class, array(
                    'required' => false,
                    'label' => $one->getTitle(),
                    'data' => $this->isNew() ? $one->getDefaultValue() : (array_key_exists($one->getAlias(), $this->data) ? $this->data[$one->getAlias()] : '')
                ));
            }
            if($type == 'ckeditor' && $one->getIsActive() && $activeField){
                $builder->add($one->getAlias(), TextareaType::class, array(
                    'required' => false,
                    'label' => $one->getTitle(),
                    'data' => $this->isNew() ? $one->getDefaultValue() : (array_key_exists($one->getAlias(), $this->data) ? $this->data[$one->getAlias()] : '')
                ));
            }
            if($type == 'checkbox' && $one->getIsActive() && $activeField){
                $builder->add($one->getAlias(), CheckboxType::class, array(
                    'required' => false,
                    'label' => $one->getTitle(),
                    'data' => $this->isNew() ? ($one->getDefaultValue() == 'true' ? true : false) : (array_key_exists($one->getAlias(), $this->data) ? ($this->data[$one->getAlias()] == 'true' ? true : false) : false)
                ));
            }
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'form_data' => ''
        ));
    }
    
    private function isNew(){
        if(!$this->formId)
            return true;
        return false;
    }

    /**
     * @return string
     */
    public function getName() {
        return 'catalog_contentbundle_morefield';
    }

}
