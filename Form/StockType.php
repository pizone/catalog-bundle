<?php

namespace PiZone\CatalogBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StockType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_active', CheckboxType::class, array(
                'label' => 'STOCK.FIELD.IS_ACTIVE.LABEL'
            ))
            ->add('title', TextType::class, array(
                'label' => 'STOCK.FIELD.TITLE'
            ))
            ->add('address', TextType::class, array(
                'label' => 'STOCK.FIELD.ADDRESS'
            ))
            ->add('lat', TextType::class, array(
                'label' => 'STOCK.FIELD.LAT'
            ))
            ->add('lng', TextType::class, array(
                'label' => 'STOCK.FIELD.LNG'
            ))
        ;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZStock'),
            'data_class' => 'PiZone\CatalogBundle\Entity\Stock'
        ));
    }
}
