<?php

namespace PiZone\CatalogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CatalogMetaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('meta_title', TextType::class, array(
                'label' => 'CATALOG.FIELD.META_TITLE',
                'required' => false
            ))
            ->add('meta_keywords', TextType::class, array(
                'label' => 'CATALOG.FIELD.META_KEYWORDS',
                'required' => false
            ))
            ->add('meta_description', TextareaType::class, array(
                'label' => 'CATALOG.FIELD.META_DESCRIPTION',
                'required' => false
            ))
            ->add('more_scripts', TextareaType::class, array(
                'label' => 'CATALOG.FIELD.META_SCRIPTS',
                'required' => false
            ))
            ->add('in_site_map', CheckboxType::class, array(
                'required' => false,
                'label' => 'CATALOG.FIELD.IN_SITE_MAP'
            ))
            ->add('in_robots', CheckboxType::class, array(
                'required' => false,
                'label' => 'CATALOG.FIELD.IN_ROBOTS'
            ))
            ->add('in_breadcrumbs', CheckboxType::class, array(
                'required' => false,
                'label' => 'CATALOG.FIELD.IN_BREADCRUMBS'
            ))
            ->add('in_menu', CheckboxType::class, array(
                'required' => false,
                'label' => 'CATALOG.FIELD.IN_MENU'
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZCatalog'),
            'data_class' => 'PiZone\CatalogBundle\Entity\CatalogMeta'
        ));
    }
}