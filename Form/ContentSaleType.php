<?php

namespace PiZone\CatalogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContentSaleType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('purchase_price', TextType::class, array(
                'label' => 'CCONTENT.FIELD.PURCHASE_PRICE',
                'required' => false
            ))
            ->add('retail_price', TextType::class, array(
                'label' => 'CCONTENT.FIELD.RETAIL_PRICE',
                'required' => false
            ))
            ->add('VAT', CheckboxType::class, array(
                'label' => 'CCONTENT.FIELD.VAT',
                'required' => false
            ))
            ->add('weight', TextType::class, array(
                'label' => 'CCONTENT.FIELD.WEIGHT',
                'required' => false
            ))
            ->add('length', TextType::class, array(
                'label' => 'CCONTENT.FIELD.LENGTH',
                'required' => false
            ))
            ->add('width', TextType::class, array(
                'label' => 'CCONTENT.FIELD.WIDTH',
                'required' => false
            ))
            ->add('height', TextType::class, array(
                'label' => 'CCONTENT.FIELD.HEIGHT',
                'required' => false
            ))
            ->add('discount', TextType::class, array(
                'label' => 'CCONTENT.FIELD.DISCOUNT',
                'required' => false
            ))
            ->add('barcode', TextType::class, array(
                'label' => 'CCONTENT.FIELD.BARCODE',
                'required' => false
            ))
            ->add('stocks', CollectionType::class, array(
                'entry_type' => StockContentType::class,
                'label' => 'CCONTENT.FIELD.STOCKS',
            ));
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZCatalogContent'),
            'data_class' => 'PiZone\CatalogBundle\Entity\ContentSale'
        ));
    }
}
