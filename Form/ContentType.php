<?php

namespace PiZone\CatalogBundle\Form;


use PiZone\CatalogBundle\Entity\Repository\CategoryRepository;
use PiZone\CatalogBundle\Entity\Repository\ContentRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ContentType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $data = $builder->getData();
        $id = $data->getId();

        $builder
            ->add('is_active', CheckboxType::class, array(
                'label' => 'CCONTENT.FIELD.IS_ACTIVE.LABEL'
            ))
            ->add('title', TextType::class, array(
                'label' => 'CCONTENT.FIELD.TITLE'
            ))
            ->add('alias', TextType::class, array(
                'label' => 'CCONTENT.FIELD.ALIAS'
            ))
            ->add('anons', TextareaType::class, array(
                'label' => 'CCONTENT.FIELD.ANONS'
            ))
            ->add('show_editor_anons', CheckboxType::class, array(
                'label' => 'CCONTENT.FIELD.EDITOR_CONTENT'
            ))
            ->add('content', TextareaType::class, array(
                'label' => 'CCONTENT.FIELD.CONTENT'
            ))
            ->add('show_editor_content', CheckboxType::class, array(
                'label' => 'CCONTENT.FIELD.EDITOR_CONTENT'
            ))
            ->add('image', FileType::class, array(
                'label' => 'CCONTENT.FIELD.IMAGE'
            ))
            ->add('delete_image', CheckboxType::class, array(
                'label' => 'CCONTENT.FIELD.DELETE_IMAGE'
            ))
            ->add('title_image', TextType::class, array(
                'label' => 'CCONTENT.FIELD.TITLE_IMAGE'
            ))
            ->add('alt_image', TextType::class, array(
                'label' => 'CCONTENT.FIELD.ALT_IMAGE'
            ))
            ->add('big_image', FileType::class, array(
                'label' => 'CCONTENT.FIELD.BIG_IMAGE'
            ))
            ->add('delete_big_image', CheckboxType::class, array(
                'label' => 'CCONTENT.FIELD.DELETE_BIG_IMAGE'
            ))
            ->add('title_big_image', TextType::class, array(
                'label' => 'CCONTENT.FIELD.TITLE_BIG_IMAGE'
            ))
            ->add('alt_big_image', TextType::class, array(
                'label' => 'CCONTENT.FIELD.ALT_BIG_IMAGE'
            ))
            ->add('is_new', CheckboxType::class, array(
                'label' => 'CCONTENT.FIELD.IS_NEW.LABEL'
            ))
            ->add('is_hit', CheckboxType::class, array(
                'label' => 'CCONTENT.FIELD.IS_HIT.LABEL'
            ))
            ->add('is_old', CheckboxType::class, array(
                'label' => 'CCONTENT.FIELD.IS_OLD.LABEL'
            ))
            ->add('sale', ContentSaleType::class)
            ->add('more', MoreFieldType::class, array(
                'form_data' => $data
            ))
            ->add('meta', ContentMetaType::class)
            ->add('category', EntityType::class, array(
                'class' => 'PiZone\CatalogBundle\Entity\Category',
                'required' => false,
                'query_builder' => function (CategoryRepository $er) {
                    return $er->getListCategories();
                }
            ))
            ->add('related', EntityType::class, array(
                'required' => false,
                'multiple' => true,
                'class' => 'PiZone\CatalogBundle\Entity\Content',
                'choice_name' => 'TitleWithArticle',
                'query_builder' => function (ContentRepository $er) use ($id) {
                    return $er->getContentWithoutSelf($id);
                }
            ))
            ->add('string_tags', TextType::class, array(
                'label' => 'CCONTENT.FIELD.HASH_TAG',
                'required' => false,
                'data' => $this->getStringTags($data)
            ));

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();
            $data = $event->getData();
            $form->add('group_parameters', ContentParametersType::class, array(
                'form_data' => $data
            ));
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZCatalogContent'),
            'data_class' => 'PiZone\CatalogBundle\Entity\Content'
        ));
    }

    private function getStringTags($data){
        $stringTags = array();
        if($tags = $data->getTags()){
            foreach($tags as $tag){
                $stringTags[] = array(
                    'id' => $tag->getId(),
                    'name' => $tag->getTitle()
                );
            }
        }

        return $stringTags;
    }
}
