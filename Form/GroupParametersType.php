<?php

namespace PiZone\CatalogBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupParametersType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_active', CheckboxType::class, array(
                'label' => 'GPARAM.FIELD.IS_ACTIVE.LABEL'
            ))
            ->add('title', TextType::class, array(
                'label' => 'GPARAM.FIELD.TITLE'
            ))
            ->add('alias', TextType::class, array(
                'label' => 'GPARAM.FIELD.ALIAS'
            ))
            ->add('description', TextType::class, array(
                'label' => 'GPARAM.FIELD.DESCRIPTION'
            ))
            ->add('parameter', CollectionType::class, array(
                'entry_type' => ParameterType::class,
                'allow_add' => true,
                'allow_delete' => true
            ))

        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZGroupParameters', 'PZParameter'),
            'data_class' => 'PiZone\CatalogBundle\Entity\GroupParameters',
            'cascade_validation' => true
        ));
    }
}
