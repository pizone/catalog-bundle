<?php

namespace PiZone\CatalogBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('is_active', CheckboxType::class, array(
                'label' => 'PROJECT.FIELD.IS_ACTIVE.LABEL'
            ))
            ->add('title', TextType::class, array(
                'label' => 'PROJECT.FIELD.TITLE'
            ))
            ->add('alias', TextType::class, array(
                'label' => 'PROJECT.FIELD.ALIAS'
            ))
            ->add('description', TextType::class, array(
                'label' => 'PROJECT.FIELD.DESCRIPTION'
            ))
            ->add('image', FileType::class, array(
                'label' => 'PROJECT.FIELD.IMAGE'
            ))
            ->add('delete_image', CheckboxType::class, array(
                'label' => 'PROJECT.FIELD.DELETE_IMAGE'
            ))
            ->add('fields', CollectionType::class, array(
                'entry_type' => ProjectFieldType::class,
                'allow_add' => true,
                'allow_delete' => true
            ))
        ;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZProject'),
            'data_class' => 'PiZone\CatalogBundle\Entity\Project'
        ));
    }
}
