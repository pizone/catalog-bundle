<?php

namespace PiZone\CatalogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class StockFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'STOCK.FIELD.ALL',
                'required' => false
            ))
            ->add('title', TextType::class, array(
                'label' => 'STOCK.FIELD.TITLE',
                'required' => false
            ))
            ->add('address', TextType::class, array(
                'label' => 'STOCK.FIELD.ADDRESS',
                'required' => false
            ))
            ->add('lat', TextType::class, array(
                'label' => 'STOCK.FIELD.LAT',
                'required' => false
            ))
            ->add('lng', TextType::class, array(
                'label' => 'STOCK.FIELD.LNG',
                'required' => false
            ))
            ->add('is_active',  ChoiceType::class, array(
                'choices' =>   array('STOCK.FIELD.IS_ACTIVE.NO' => 0,    'STOCK.FIELD.IS_ACTIVE.YES' => 1,),
                'label' => 'STOCK.FIELD.IS_ACTIVE.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'STOCK.FIELD.IS_ACTIVE.ANY',
                'empty_data'  => null,
                'required' => false
            ))
        ;
    }
}
