<?php

namespace PiZone\CatalogBundle\Form;

use PiZone\CatalogBundle\Entity\Repository\CatalogRepository;
use PiZone\CatalogBundle\Entity\Repository\ProjectRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CatalogFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('all', TextType::class, array(
                'label' => 'CATALOG.FIELD.ALL',
                'required' => false
            ))
            ->add('title', TextType::class, array(
                'label' => 'CATALOG.FIELD.TITLE',
                'required' => false
            ))
            ->add('alias', TextType::class, array(
                'label' => 'CATALOG.FIELD.ALIAS',
                'required' => false
            ))
            ->add('parent', EntityType::class, array(
                'label' => 'CATALOG.FIELD.PARENT',
                'class' => 'PiZone\CatalogBundle\Entity\Catalog',
                'required' => false,
                'query_builder' => function (CatalogRepository $er) {
                    return $er->createQueryForProject();
                }
            ))
            ->add('project', EntityType::class, array(
                'label' => 'CATALOG.FIELD.PROJECT',
                'class' => 'PiZone\CatalogBundle\Entity\PROJECT',
                'required' => false,
                'query_builder' => function (ProjectRepository $er) {
                    return $er->getAvailableList();
                }
            ))
            ->add('level', IntegerType::class, array(
                'label' => 'CATALOG.FIELD.LEVEL',
                'required' => false
            ))
            ->add('is_active',  ChoiceType::class, array(
                'choices' =>   array('CATALOG.FIELD.IS_ACTIVE.NO' => 0,    'CATALOG.FIELD.IS_ACTIVE.YES' => 1,),
                'label' => 'CATALOG.FIELD.IS_ACTIVE.LABEL',
                'choices_as_values' => true,
                'placeholder' => 'CATALOG.FIELD.IS_ACTIVE.ANY',
                'empty_data'  => null,
                'required' => false
            ))
        ;
    }
}
