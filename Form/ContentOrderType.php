<?php

namespace PiZone\CatalogBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class ContentOrderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', EntityType::class, array(
                'label' => 'ORDER.FIELD.GOODS',
                'class' => 'PiZone\CatalogBundle\Entity\Content',
            ))
            ->add('quantity', TextType::class, array(
                'label' => 'ORDER.FIELD.QUANTITY'
            ))
            ->add('price', TextType::class, array(
                'label' => 'ORDER.FIELD.CONTENT.PRICE'
            ))
            ->add('discount', TextType::class, array(
                'label' => 'ORDER.FIELD.CONTENT.DISCOUNT'
            ))
            ->add('comment', TextType::class, array(
                'label' => 'ORDER.FIELD.COMMENT'
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'validation_groups' => array('PZContentOrder'),
            'cascade_validation' => true,
            'data_class' => 'PiZone\CatalogBundle\Entity\ContentOrder'
        ));
    }
}
